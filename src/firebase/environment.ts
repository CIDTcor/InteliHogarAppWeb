export const snapshotToArray = (snapshot) => {
    let returnArray = [];
    snapshot.forEach(element => {
        let item = element.val();
        //item.key = element.key;
        returnArray.push(item);
    });
    return returnArray;
}

export const snapshotToArrayWihtKey = (snapshot) => {
    let returnArray = [];
    snapshot.forEach(element => {
        let item = element.val();
        item.key = element.key;
        returnArray.push(item);
    });
    return returnArray;
}

export var snapshotToJSONArray = (snapshot) => {
    let returnJSONArray = {};
    snapshot.forEach(element => {
        let item = element.val();
        item.key = element.key;
        returnJSONArray[item.key] = item;
    });
    return returnJSONArray;
}