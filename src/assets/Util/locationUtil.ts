import { strings } from '../Util/constants';
import * as firebase from 'firebase';

export var getLocationKey = () => {
    return JSON.parse(localStorage.getItem(strings.session.selectedLocation)).locationKey;
}

export var getSelectedLocation = () => {
    return JSON.parse(localStorage.getItem(strings.session.selectedLocation));
}

export var savePreferenceToFirebase = (data: Array<any>) => {
    let userId = JSON.parse(localStorage.getItem("sessionUser")).userMail;
    let locationId = JSON.parse(
        localStorage.getItem(strings.session.selectedLocation)
    ).locationId;

    let ref = firebase.database().ref("locationsUsers/" + locationId + "_" + userId.replace(/\./g, "-") + "/preferences");
    ref.set(data.toString());
}

export var getUserId = () => {
    return JSON.parse(localStorage.getItem('sessionUser')).userMail;
}