import { getSelectedLocation } from './locationUtil';
import { dataBase } from '../Util//FirebaseCommunication';
import { getRoomAlias } from '../Util//roomUtils';
import { strings } from './constants';
import { Device } from './Device';


declare function getMeterUnit(type);
var deviceList = Device.getInstance();

//#region Const
export const sheetVibrateTime = 100;
export const sheetActiveTime = 200;
//#endregion

//#region Utils
export var getDevices = (locationKey, locationId) => {
    dataBase.getDevices(locationKey, devices => {
        let newDevices = devices;
        if (existDevicesAliasDB(locationId)) {
            newDevices = updateDevicesDB(locationId, devices);
        }
        saveLocationDevices(locationId, newDevices);
    });
}

var existDevicesAliasDB = (locationId) => {
    return localStorage.getItem('deviceAliasList_' + locationId) ? true : false;
}

var updateDevicesDB = (locationId, newDB) => {
    let devicesAlias = JSON.parse(localStorage.getItem('deviceAliasList_' + locationId));
    newDB.forEach(device => {
        if (devicesAlias[device.id]) {
            device.alias = devicesAlias[device.id];
        }
    });
    return newDB;
}

export var updateDevicesAliasDB = (locationId, device) => {
    let devicesAlias = JSON.parse(localStorage.getItem('deviceAliasList_' + locationId));
    if (!devicesAlias) {
        devicesAlias = {};
    }
    devicesAlias[device.id] = device.alias;
    localStorage.setItem('deviceAliasList_' + locationId, JSON.stringify(devicesAlias));
    saveLocationDevices(locationId, updateDevicesDB(locationId, getLocationDevices(locationId)));
}

export var translateDeviceType = (deviceType) => {
    switch (deviceType) {
        case strings.types.locks:
            return " - Cerradura";
        case strings.types.doorContacts:
            return " - Contacto Puerta";
        case strings.types.garages:
            return " - Garaje";
        case strings.types.lockDoors:
            return " - Cerradura Puerta";
        case strings.types.switches:
            return " - Interruptor";
        case strings.types.switchLights:
            return " - Interruptor Luz";
        case strings.types.sirenas:
            return " - Sirena";
        case strings.types.alarmas:
            return " - Alarma";
        case strings.types.alertas:
            return " - Alertas";
        case strings.types.inteliAppsGeneral:
            return " - IA General";
        case strings.types.inteliAppsMedical:
            return " - IA Médica";
        case strings.types.inteliAppsSecurity:
            return " - IA Seguridad";
        case strings.types.inteliAppsEnergy:
            return " - IA Energía";
        case strings.types.dimmers:
            return " - Variador";
        case strings.types.curtains:
            return " - Cortina";
        case strings.types.windows:
            return " - Ventana";
        case strings.types.windowContacts:
            return " - Contacto Ventana";
        case strings.types.adjustableLights:
            return " - Luz Ajustable";
        case strings.types.colorLights:
            return " - Luz Color";
        case strings.types.showerOld:
            return " - Ducha";
        case strings.types.shower:
            return " - Ducha";
        case strings.types.thermostats:
            return " - Termostato";
        case strings.types.momentary:
            return " - Boton";
        case strings.types.contacts:
            return " - Contacto";
        case strings.types.presence:
            return " - Presencia";
        case strings.types.motion:
            return " - Movimiento";
        case strings.types.temperature:
            return " - Temperatura";
        case strings.types.humidity:
            return " - Humedad";
        case strings.types.valves:
            return " - Válvula";
        case strings.types.smokes:
            return " - Humo";
        case strings.types.cameras:
            return " - Cámara";
        case strings.types.projectors:
            return " - Proyector";
        case strings.types.battery:
            return " - Batería";
        case strings.types.energy:
            return " - Energía";
        case strings.types.power:
            return " - Potencia";
        case strings.types.monoxide:
            return " - Monoxido";
        case strings.types.water:
            return " - Agua";
        case strings.types.modo:
            return " - Modo";
        case strings.types.helloHome:
            return " - Rutina";
        case strings.types.outerDoor:
            return " - Portón Exterior";
        default:
            return "";
    }
}

export var translateThermostatModes = (mode) => {
    switch (mode) {
        case 'off':
            return 'Apagado';
        case 'cool':
            return 'Enfriando';
        case 'heat':
            return 'Calentando';
        default:
            return 'Auto';
    }
}
//#endregion

//#region Setter
export var saveLocationDevices = (locationId, devices) => {
    localStorage.setItem('devices_' + locationId, JSON.stringify(devices));
}

export var getLocationDevices = (locationId) => {
    return JSON.parse(localStorage.getItem('devices_' + locationId));
}

export var devicesToArray = (data) => {
    let array = [];
    for (let i in data) {
        for (let j in data[i]) {
            array.push(data[i][j]);
        }
    }
    return array;
}

export var setDeviceTypeList = (listAction, listState, listColorLight, listMultiWhiteLight, listThermostat, modes, allDevices) => {
    allDevices.forEach(device => {
        switch (device.type) {
            case "colorLight":
                listColorLight.push(device);
                break;
            case "multiWhiteLight":
                listMultiWhiteLight.push(device);
                break;
            case "mode":
                break;
            case "helloHome":
                modes.push(device);
                break;
            case "thermostat":
            case "shower":
                listThermostat.push(device);
                break;
            case "temperature":
            case "humidity":
            case "battery":
            case "energy":
            case "power":
            case "monoxide":
            //with icon
            case "doorContact":
            case "windowContact":
            case "motion":
            case "smoke":
            case "water":
            case "valve":
            case "lightSensor":
                listState.push(device);
                break;
            default:
                listAction.push(device);
        }
    });
}

export var setSecurityDeviceTypeList = (devicesSecurityAction, modes, allDevices) => {
    allDevices.forEach(device => {
        let alias = (device.roomId > 1) ? device.alias + " " + getRoomAlias(device.roomId) : device.alias;
        if (device.type == "alarmaIntrusos") {
            device.alias = alias;
            modes.push(device);
        } else if (device.type == "alarmaIncendio" || device.type == "alarmaGas" || device.type == "alarmaAgua" || device.type == "simulacionPresencia") {
            device.alias = alias;
            devicesSecurityAction.push(device);
        }

    });
}

export var setEnergyDeviceTypeList = (energyMeterDevices: any[], energyGeneratorDevices: any[], batteries: any[], allDevices: any[]) => {
    allDevices.forEach(device => {
        switch (device.type) {
            case "energy":
                //energyMeterDevices.push(device);
                break;
            case "generator":
                //energyGeneratorDevices.push(device);
                break;
            case "battery":
                batteries.push(device);
                break;
        }
        //TODO: temporary fix for energy samples, fix later on
    });
}

//#endregion

//#region Update
export var updateDevicesByType = (listAction, listState, listColorLight, listMultiWhiteLight, listThermostat, listModes, devices, context) => {
    devices.forEach(device => {
        switch (device.type) {
            case "colorLight":
                context.icon = `icon-${device.type}-${device.status}`;
                context.image = null;
                context.tile = `${context.page}_${device.id}`;
                updateDevicesColorLight(listColorLight, device, context);
                break;
            case "multiWhiteLight":
                context.icon = `icon-${device.type}-${device.status}`;
                context.image = null;
                context.tile = `${context.page}_${device.id}`;
                updateDevicesMultiWhiteLight(listMultiWhiteLight, device, context);
                break;
            case "thermostat":
            case "shower":
                context.icon = `icon-${device.type}-${device.status}`;
                context.image = null;
                context.tile = `${context.page}_${device.id}`;
                updateDevicesThermostat(listThermostat, device, context);
                break;
            case "mode":
                break;
            case "helloHome":
                // updateTile(device, "cft", page);
                // updateTile(device, "sc", page);
                context.icon = null;
                context.image = `${device.type}.png`;
                context.tile = `${context.page}_${device.id}`;

                updateModesState(listModes, device, context);
            case "temperature":
            case "humidity":
            case "battery":
            case "energy":
            case "power":
            case "monoxide":
                context.icon = null;
                context.image = null;
                context.tile = `${context.page}_${device.id}`;
                updateDevicesState(listState, device, context);
                break;
            //with icon
            case "doorContact":
            case "windowContact":
            case "motion":
            case "smoke":
            case "water":
            case "valve":
                // updateTile(device, "cft", page);
                // updateTile(device, "sc", page);
                context.icon = `icon-${device.type}-${device.status}`;
                context.image = null;
                context.tile = `${context.page}_${device.id}`;
                updateDevicesState(listState, device, context);
                break;
            default:
                // updateTile(device, "cft", page);
                // updateTile(device, "sc", page);
                context.icon = `icon-${device.type}-${device.status}`;
                context.image = `${device.type}-${device.status}.png`;
                context.tile = `${context.page}_${device.id}`;
                updateDevicesAction(listAction, device, context);
        }
    });
}

export var updateSecurityDevicesByType = (devicesSecurityAction, devicesSecurityState, modes, devices, context) => {
    devices.forEach(device => {
        switch (device.type) {
            case "alarma":
            case "alerta":
            case "energy":
            case "power":
            case "monoxide":
            case "smoke":
                context.icon = null;
                context.image = null;
                context.tile = `${context.page}_${device.id}`;
                updateDevicesState(devicesSecurityState, device, context);
                break;
            case "alarmaIntrusos":
                context.icon = (device.name.toLowerCase().includes('noche')) ? 'icon-night' : (device.name.toLowerCase().includes('completa')) ? 'icon-away' : 'icon-home';
                context.image = `${device.type}-${device.status}.png`;
                context.tile = `${context.page}_${device.id}`;
                context.style = 'border: 2px solid green';
                context.circle = `${context.page}_${device.id}`;
                updateModesState(modes, device, context);
                break;
            default:
                context.icon = `icon-${device.type}-${device.status}`;
                context.image = `${device.type}-${device.status}.png`;
                context.tile = `${context.page}_${device.id}`;
                updateDevicesAction(devicesSecurityAction, device, context);
        }
    });
}

export var updateEnergyDevicesByType = (energyMeterDevice: any[], energyGeneratorDevice: any[], energyDifferenceDevice: any, batteries: any[], devicesState: any[]) => {
    devicesState.forEach(device => {
        switch (device.type) {
            case "battery":
                updateBatteryDevice(batteries, device);
                break;
            case "generator":
                updateEnergyDevice(energyGeneratorDevice, device);
                break;
            case "energy":
                updateEnergyDevice(energyMeterDevice, device);
                break;
            case "energyDifference":
                updateDifferenceDevice(energyDifferenceDevice, device);
                break;

        }
    });
}

export var updateDevicesAction = (list, device, context) => {
    list.forEach(item => {
        if (item.id == device.id) {
            item.status = device.status;
            item.level = device.value.level;
            context.level = item.level;
            updateTile(context);
        }
    });
}

export var updateDevicesState = (list, device, context) => {
    list.forEach(item => {
        if (item.id == device.id) {
            switch (item.type) {
                case "energy":
                    if (device.value.power) {
                        item.status = Math.round(device.value.power) + getMeterUnit(item.type);
                        context.value = item.status;
                        updateTile(context);
                    }
                    break;
                case "humidity":
                case "monoxide":
                case "power":
                case "temperature":
                case "battery":
                    if (device.value != "n/a" && device.value != "NaN" && device.value != "-1000") {
                        item.status = Math.round(device.value) + getMeterUnit(item.type);
                        context.value = item.status;
                        updateTile(context);
                    } else {
                        item.status = "!";
                        context.value = item.status;
                        updateTile(context);
                    }
                    break;
                default:
                    item.status = device.status;
                    item.level = device.value.level;
                    updateTile(context);
            }
        }
    });
}

export var updateModesState = (list, device, context) => {
    list.forEach(item => {
        if (item.id == device.id) {
            item.status = device.status;
            context.status = item.status;
            // item.level = device.value.level;
            updateTile(context);
        }
    });
}

export var updateDevicesColorLight = (list, device, context) => {
    list.forEach(item => {
        if (item.id == device.id) {
            item.status = device.status;
            item.value = device.value;
            if (item.value.hue == 52.5 || item.value.hue == 14.16) {
                item.color = 'hsl(' + (item.value.hue * 3.6) + ',' + item.value.saturation + '%' + ',' + item.value.level + '%)';
            } else {
                item.color = 'hsl(' + (item.value.hue * 3.6) + ',' + item.value.saturation + '%' + ',' + '50%' + ')';
            }
            item.level = device.value.level;
            context.level = item.level;
            updateTile(context);
        }
    });
}

export var updateDevicesMultiWhiteLight = (list, device, context) => {
    list.forEach(item => {
        if (item.id == device.id) {
            item.status = device.status;
            item.value = device.value;
            if (item.value.hue == 52.5 || item.value.hue == 14.16) {
                item.color = 'hsl(' + (item.value.hue * 3.6) + ',' + item.value.saturation + '%' + ',' + item.value.level + '%)';
            } else {
                item.color = 'hsl(' + (item.value.hue * 3.6) + ',' + item.value.saturation + '%' + ',' + '50%' + ')';
            }
            item.level = device.value.level;
            context.level = item.level;
            updateTile(context);
        }
    });
}

export var updateDevicesThermostat = (list, device, context) => {
    list.forEach(item => {
        if (item.id == device.id) {
            item.status = device.status;
            item.temperature = device.value.temperature + "°C";
            item.value = device.value;
            context.temperature = item.temperature;
            updateTile(context);
        }
    });
}

var updateEnergyDevice = (energyDevices: any[], deviceState: any) => {
    energyDevices.forEach(device => {
        if (device.id == deviceState.id) {
            device.value = deviceState.value;
            device.energy = device.value.energy.toFixed(1) + getMeterUnit(device.type);
        }
    });
}

var updateDifferenceDevice = (energyDifferenceDevice, deviceState: any) => {
    energyDifferenceDevice.energy = deviceState.value.energy;
}

var updateBatteryDevice = (batteries: any[], deviceState: any) => {
    batteries.forEach(battery => {
        if (battery.id == deviceState.id) {
            battery.value = Number.parseInt(deviceState.value);
            battery.percent = deviceState.value + getMeterUnit(battery.type);
        }
    });
}

var updateTile = (context) => {
    let tile = document.getElementById(context.tile);
    if (tile) {
        let icon = (tile.getElementsByClassName("st-icon")[0]) ? tile.getElementsByClassName("st-icon")[0].getElementsByTagName("i")[0] : null;
        let image = (tile.getElementsByClassName("st-icon")[0]) ? tile.getElementsByClassName("st-icon")[0].getElementsByTagName("img")[0] : null;
        let input = tile.getElementsByTagName("input")[0];
        let span = tile.getElementsByTagName("span")[0];
        let p = tile.getElementsByTagName("p")[0];

        if (icon) {
            icon.setAttribute("class", `intelihogarIcon ${context.icon}`);
        }
        if (image) {
            image.src = `../../assets/imgs/inteliApps/${context.image}`;
        }
        if (input) {
            input.value = context.level;
            tile.getElementsByTagName("label")[0].innerHTML = context.level;
        }
        if (span) {
            span.innerHTML = context.temperature;
        }
        if (p) {
            p.innerHTML = context.value;
        }

    }
    if (document.getElementById(context.circle)) {
        let innerCircle = document.getElementById(context.circle);

        if (innerCircle) {
            if (context.status == "on") {
                innerCircle.getElementsByClassName("st-innerMode")[0].setAttribute("style", "border: 2px solid green");
            } else {
                innerCircle.getElementsByClassName("st-innerMode")[0].setAttribute("style", "border: 2px solid #000d37");

            }
        }
    }
}

// var updateTile = (device, page, from) => {
// let deviceId = (from == 'fb') ? device.deviceType + "_" + device.deviceId : device.type + "_" + device.id;
// let deviceValue = (from == 'fb') ? device.value : device.status;
// let deviceType = (from == 'fb') ? device.deviceType : device.type;
// let elementHTML = document.getElementById(page + "_" + deviceId);
// if (elementHTML) {
//     let stIcon = elementHTML.getElementsByClassName("st-icon")[0];
//     let icon = stIcon.getElementsByTagName("i")[0];
//     if (icon) {
//         //icon.setAttribute("class", "icon-" + device.type + "-" + deviceValue)
//         icon.classList.add("icon-" + device.type + "-" + deviceValue);
//         let input = stIcon.getElementsByTagName("input")[0];
//         if (input) {
//             input.value = device.level;
//             stIcon.getElementsByTagName("label")[0].innerHTML = device.level;
//         }
//     } else if (stIcon.getElementsByTagName("img")[0]) {
//         if (deviceType != "helloHome") {
//             stIcon.getElementsByTagName("img")[0].src = '../../assets/imgs/inteliApps/' + deviceType + '-' + deviceValue + '.png';
//         }
//     } else if (stIcon.getElementsByTagName("p")[0]) {
//         stIcon.getElementsByTagName("p")[0].innerHTML = deviceValue;
//     }
// }
// }
//#endregion

//#region Preferences
//Shortuts
export var addDevicesListToShortcuts = (locationId, devices) => {
    localStorage.setItem('shortcuts' + "_" + locationId, JSON.stringify(devices));
}

//Notifications
export var addDevicesListToNotifications = (locationId, devices) => {
    localStorage.setItem('notifications' + "_" + locationId, JSON.stringify(devices));
}
//#endregion

//#region -----Action Sheet
export var isItInShortcuts = (device) => {
    let shortcuts = JSON.parse(localStorage.getItem('shortcuts_' + getSelectedLocation().locationId));
    if (shortcuts == null) {
        shortcuts = [];
        localStorage.setItem('shortcuts_' + getSelectedLocation().locationId, JSON.stringify(shortcuts));
    }
    if (shortcuts.includes(device.id))
        return true;
    else
        return false;
}

export var addDeviceToShortcuts = (locationId, device) => {
    let shortcuts = JSON.parse(localStorage.getItem('shortcuts_' + locationId));
    shortcuts.push(device.id);
    localStorage.setItem('shortcuts_' + locationId, JSON.stringify(shortcuts));
}

export var removeDeviceFromShortcuts = (locationId, device) => {
    let shortcuts = JSON.parse(localStorage.getItem('shortcuts_' + locationId));
    for (let i in shortcuts) {
        if (shortcuts[i] == device.id) {
            shortcuts.splice(i, 1);
            break;
        }
    }
    localStorage.setItem('shortcuts_' + locationId, JSON.stringify(shortcuts));
    // this.showToast(`Dispositivo <<${device.name}>> eliminado de accesos directos.`);
}

export var isItInNotifications = (device) => {
    let notifications = JSON.parse(localStorage.getItem('notifications_' + getSelectedLocation().locationId));
    if (notifications == null) {
        notifications = [];
        localStorage.setItem('notifications_' + getSelectedLocation().locationId, JSON.stringify(notifications));
    }
    if (notifications.includes(device.id))
        return true;
    else
        return false;
}

export var addDeviceToNotifications = (locationId, device, devicesList) => {
    let notifications = JSON.parse(localStorage.getItem('notifications_' + locationId));
    notifications.push(device.id);
    localStorage.setItem('notifications_' + locationId, JSON.stringify(notifications));
    devicesList(notifications);
}

export var removeDeviceFromNotifications = (locationId, device, devicesList) => {
    let notifications = JSON.parse(localStorage.getItem('notifications_' + locationId));
    for (let i in notifications) {
        if (notifications[i] == device.id) {
            notifications.splice(i, 1);
            break;
        }
    }
    localStorage.setItem('notifications_' + locationId, JSON.stringify(notifications));
    devicesList(notifications);
}
//#endregion

//#region Sample Alarms
export let sampleAlarms = [
    { id: "sampleIntrude", alias: "Alarma de Intrusos", type: "alarmaIntrusos", status: "sample", },
    { id: "sampleGas", alias: "Alarma de Gas", type: "alarmaGas", status: "sample", },
    { id: "sampleFire", alias: "Alarma de Incendio", type: "alarmaIncendio", status: "sample", },
    { id: "sampleWater", alias: "Alarma fuga de agua", type: "alarmaAgua", status: "sample", },
    { id: "samplePresence", alias: "Simulación de presencia", type: "simulacionPresencia", status: "sample", }
]

export let sampleEnergyDevices = [
    { id: "sampleConsumed", alias: "Medición de energía consumida", type: "energy", status: "sample", },
    { id: "sampleGeneration", alias: "Medición de energía generada", type: "generator", status: "sample", }

]
//#endregion

//FIXME: Algunas temperaturas siguen mostrando -1000 porque la condicion es que sea diferente de esa cantinadad, pero hay variaciones decimales.