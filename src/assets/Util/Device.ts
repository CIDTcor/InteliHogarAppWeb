export class Device {
    private static instance: Device;
    public allDevices = [];
    public modes = [];
    public devicesAction = [];
    public devicesState = [];
    public devicesColorLight = [];
    public devicesMultiWhiteLight = [];
    public devicesThermostat = [];

    private constructor() {}

    static getInstance(): Device {
        if (!Device.instance) {
            Device.instance = new Device();
        }
        return Device.instance;
    }
}
