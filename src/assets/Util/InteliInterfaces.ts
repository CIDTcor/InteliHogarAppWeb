export interface InteliUser {
    provider: string;
    sInit: boolean;
    userImage: string;
    userMail: string;
    userName: string;
    userPassword: string;
    userPhone: string;
}

export interface UserProfile {
    displayName: string;
    photoURL: string;
}

export interface InteliTab {
    page: any;
    title: string;
    id: string;
    icon: string;
}

interface LocationsUsers {
    accessLevel: string;
    isActive: boolean;
    locationId: string;
    owner: boolean;
    userId: string;
    preferences?: string;
}

export interface InteliDevice {
    accessLevel: string;
    alias: string;
    help: string;
    id: string;
    name: string;
    roomId: string;
    type: string;
    visible: boolean;
}

export interface EnergyLog {
    date: string;
    id: string;
    name: string;
    status: string;
    type: string;
    value: EnergyValue;
}

interface EnergyValue {
    current: number;
    energy: number;
    power: number;
    voltage: number;
}