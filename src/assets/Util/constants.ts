export const strings = {
    session: {
        sessionUser: "sessionUser",
        defaultLocation: "defaultLocation",
        selectedLocation: "selectedLocation",
        sessionLocations: "sessionLocations",
        allLocations: "allLocations"
    },
    types: {
        locks: "lock",
        doorContacts: "doorContact",
        garages: "garage",
        lockDoors: "lockDoor",
        switches: "switch",
        switchLights: "switchLight",
        sirenas: "sirena",
        alarmas: "alarma",
        alertas: "alerta",
        inteliAppsGeneral: "inteliAppsGeneral",
        inteliAppsMedical: "inteliAppsMedical",
        inteliAppsSecurity: "inteliAppsSecurity",
        inteliAppsEnergy: "inteliAppsEnergy",
        dimmers: "dimmer",
        curtains: "curtain",
        windows: "window",
        windowContacts: "windowContact",
        adjustableLights: "adjustableLight",
        colorLights: "colorLight",
        showerOld: "showerOld",
        shower: "shower",
        thermostats: "thermostat",
        momentary: "momentary",
        contacts: "contacts",
        presence: "presence",
        motion: "motion",
        temperature: "temperature",
        humidity: "humidity",
        valves: "valve",
        smokes: "smoke",
        cameras: "camera",
        projectors: "projector",
        battery: "battery",
        energy: "energy",
        power: "power",
        monoxide: "monoxide",
        water: "water",
        modo: "modo",
        helloHome: "helloHome",
        outerDoor: "outerDoor"
    },
    actions: {
        ok: "Aceptar",
        cancel: "Cancelar",
        add: "Agregar",
        shortcut: "shortcuts",
        notification: "notifications",
        hiddenDevices: "hiddenDevices"
        
    },
    messages: {
        notificationConfig: "Notificaciones activas",
        shortcutConfig: "Favoritos",
        widgetConfig: "Widgets activos"
    },
    widgets: {
        clock: "Reloj",
        backButton: "Botón de regresar",
        weather: "Clima",
        widgets: "Widgets"
    },
    devices: {
        db: "devicesDB",
        types: {
            doorContacts: "Puertas",
            windows: "Ventanas"
        },
        shortcuts: "shortcuts"
    },
    modules: {
        shortcuts: "Favoritos",
        comfort: "Confort",
        security: "Seguridad",
        energy: "Ahorro de Energía"
    },
    references: {
        users: 'users',
        tokens: 'deviceTokens',
        locationUsers: 'locationsUsers',
        locations: 'locations'
    },
    keys: {
        password: 'userPassword',
        mail: 'userMail',
        userId: 'userId',
        locationId: 'locationId',
        deviceTokenUser: 'user'
    }
}

export const alerts = {
    newRoom: {
        nameTitle: 'Adicionar habitación',
        nameMessage: 'Ingrese el nombre de su nueva habitación.',
        typeTitle: 'Tipo de habitación',
        typeMessage: 'Seleccione el tipo de habitación.'
    }
};

export var roomTypes = [
    { type:'radio', value: 'config', label: 'Panel de control' },
    { type:'radio', value: 'inteliGadget', label: 'InteliApps' },
    { type:'radio', value: 'living', label: 'Sala' },
    { type:'radio', value: 'dinning', label: 'Comedor' },
    { type:'radio', value: 'bed-kid', label: 'Recámara de los niños' },
    { type:'radio', value: 'bed-young', label: 'Recámara de los jóvenes' },
    { type:'radio', value: 'bed-guest', label: 'Recámara de los huéspedes' },
    { type:'radio', value: 'bed-owner', label: 'Recámara de los dueños' },
    { type:'radio', value: 'bath-kid', label: 'Baño de los niños' },
    { type:'radio', value: 'bath-young', label: 'Baño de los jóvenes' },
    { type:'radio', value: 'bath-guest', label: 'Baño de los huéspedes' },
    { type:'radio', value: 'bath-owner', label: 'Baño de los dueños' },
    { type:'radio', value: 'kitchen', label: 'Cocina' },
    { type:'radio', value: 'stay', label: 'Estancia' },
    { type:'radio', value: 'laundry', label: 'Lavandería' },
    { type:'radio', value: 'office', label: 'Oficina' },
    { type:'radio', value: 'garage', label: 'Garaje' },
    { type:'radio', value: 'yard', label: 'Patio' },
    { type:'radio', value: 'hall', label: 'Pasillo' },
    { type:'radio', value: 'battery', label: 'Baterías' },
    { type:'radio', value: 'intake', label: 'Consumos' },
    { type:'radio', value: 'pool', label: 'Piscina' },
];

export var iconSet = [
    { iconName: 'icon-bed-owner'},
    { iconName: 'icon-bed-young'},
    { iconName: 'icon-bed-kid'},
    { iconName: 'icon-bed-guest'},
    { iconName: 'icon-bath-owner'},
    { iconName: 'icon-bath-young'},
    { iconName: 'icon-bath-kid'},
    { iconName: 'icon-bath-guest'},
    { iconName: 'icon-kitchen'},
    { iconName: 'icon-dinning'},
    { iconName: 'icon-bar'},
    { iconName: 'icon-living'},
    { iconName: 'icon-stay'},
    { iconName: 'icon-office'},
    { iconName: 'icon-laundry'},
    { iconName: 'icon-hall'},
    { thisIsABlankSpace: 'ignore'},
    { iconName: 'icon-outdoor'},
    { iconName: 'icon-rocket'}
];