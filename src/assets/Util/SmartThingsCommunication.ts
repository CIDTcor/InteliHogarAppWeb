import * as $ from 'jquery';

class SmartThingsCommunication {
    
    public getDevicesStateFromST(locationServer, locationIdentifier, locationAccessToken, callback) {
        let url = "https://" + locationServer + "/api/smartapps/installations/" + locationIdentifier + "/data/?access_token=" + locationAccessToken + "&callback=?";
        $.getJSON(url)
          .done(data => {
            callback(data);
          })
          .fail((jqxhr, textStatus, error) => {
            console.log("Response: " + error + " Estado: " + textStatus);
          });
      }
}

export var smartThings = new SmartThingsCommunication();