export interface Device {
    selectedLocation : any;
    devices: Array<any>;
    devicesAction: Array<any>;
    devicesState: Array<any>;
    devicesColorLight: Array<any>;
    devicesThermostat: Array<any>;
}