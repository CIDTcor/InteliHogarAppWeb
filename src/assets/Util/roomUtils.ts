import { dataBase } from './../../assets/Util/FirebaseCommunication';
import { getSelectedLocation } from './locationUtil';

export var getRooms = (locationKey, locationId) => {
  dataBase.getRooms(locationKey, rooms => {
    let newRooms = rooms;
    if (existRoomsAliasDB(locationId)) {
      newRooms = updateRoomsDB(locationId, rooms);
    }
    saveLocationRooms(locationId, newRooms);
  });
}

export var getRoomAlias = (roomId) => {
  let locationId = JSON.parse(localStorage.getItem("selectedLocation")).locationId;
  let roomList = JSON.parse(localStorage.getItem("roomList_" + locationId));
  let roomAlias = "";
  roomList.forEach(element => {
    if (element.roomId == roomId) {
      roomAlias = element.roomAlias;
    }
  });
  return roomAlias;
}

var existRoomsAliasDB = (locationId) => {
  return localStorage.getItem('roomAliasList_' + locationId) ? true : false;
}

var updateRoomsDB = (locationId, newDB) => {
  let roomsAlias = JSON.parse(localStorage.getItem('roomAliasList_' + locationId));
  newDB.forEach(room => {
    if (roomsAlias[room.roomId]) {
      room.roomAlias = roomsAlias[room.roomId];
    }
  });
  return newDB;
}

export var updateRoomsAliasDB = (locationId, room) => {
  let roomsAlias = JSON.parse(localStorage.getItem('roomAliasList_' + locationId));
  if (!roomsAlias) {
    roomsAlias = {};
  }
  roomsAlias[room.roomId] = room.roomAlias;
  localStorage.setItem('roomAliasList_' + locationId, JSON.stringify(roomsAlias));
  saveLocationRooms(locationId, updateRoomsDB(locationId, getLocationRooms(locationId)));
}

export var saveLocationRooms = (locationId, rooms) => {
  localStorage.setItem('roomList_' + locationId, JSON.stringify(rooms));
}

export var getLocationRooms = (locationId) => {
  return JSON.parse(localStorage.getItem('roomList_' + locationId));
}

export var generateNewRoomId = (rooms) => {
  //let roomList = JSON.parse(localStorage.getItem("roomList_" + locationId));
  if (rooms.length < 1) {
    return "room_0";
  }
  rooms = sortById(rooms);
  let newIdNumber = parseInt(rooms[rooms.length - 1].roomId.split('_')[1]) + 1;
  let newId = "room_" + newIdNumber;
  return newId;
}

export var generateNewRoomOrder = (rooms) => {
  if (rooms.length < 1) {
    return 0;
  }
  rooms = sortByOrder(rooms);
  return rooms[rooms.length - 1].roomOrder + 1;
}

var sortByOrder = (rooms) => {
  rooms.sort((a, b) => {
    return a.roomOrder - b.roomOrder;
  });
  return rooms;
}

var sortById = (roomList) => {
  roomList.sort((a, b) => {
    return a.roomId - b.roomId;
  });
  return roomList;
}