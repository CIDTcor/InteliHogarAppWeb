import * as CryptoJS from 'crypto-js';
import * as $ from 'jquery';

class TuyaCommunication {
  public clientId = '7a9hmj7taotw7qite0qe';
  public secret = '3fedc14ee40a430d93b0b7c98afc932c';
  private map = {};
  private arr = [];
  private accessToken = undefined;
  private accessTokenExpiration = 0;
  private ticket;
  public deviceId; //TODO: cambiar el deviceId por uno que agarre automaticamente
  public device = {};
  public interval;

  //#region Tuya
  public sendTuyaCommand(type, id, value) {
    return new Promise((resolve, reject) => {
      this.deviceId = id;
      switch (type) {
        case 'lockDoor':
          console.log(this.device[id], "this.device[id]");
          this.openSmartLock(this.deviceId).then(doorOpened => {
            resolve(true);
          });
          break;

        case 'switchLight':
          this.switchLight(value).then(response => {
            resolve(true);
          });
          break;


        default:
          // this.sendGeneralCommand();
          resolve(true);
          break;
      }
    });
  }

  public getAccessToken() {
    return new Promise((resolve, reject) => {
      let url = 'https://openapi.tuyaus.com/v1.0/token?grant_type=1';
      let timestamp = new Date().getTime();
      console.log(timestamp, "timestamp");
      console.log(this.accessTokenExpiration, "this.accessTokenExpiration");
      // let timestamp = '1626282139';
      let requestType = 'GET';
      let query = [{ 'key': 'grant_type', 'value': '1' }];
      let mode = 'raw';
      let urlElements = ['v1.0', 'token'];
      let body = undefined;
      let nonce = undefined;
      let signStr = this.getStringToSign(query, mode, requestType, urlElements, body);
      let sign = this.getSign(timestamp, nonce, signStr);
      if (timestamp > this.accessTokenExpiration) {
        this.accessToken = undefined;
        this.sendTuyaRequest(url, requestType, sign, timestamp, body).then(response => {
          let result = JSON.parse(response.toString());
          console.log(result, "ACCESS TOKEN");
          if (result.success) {
            this.accessToken = result.result.access_token;
            this.accessTokenExpiration = result.t + (result.result.expire_time * 1000);
            resolve(true);
          }
        })
      } else {
        resolve(true);
      }
    });
  }

  public getTicket(deviceId): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = `https://openapi.tuyaus.com/v1.0/devices/${deviceId}/door-lock/password-ticket`
      let timestamp = new Date().getTime();
      let query = [];
      let mode = 'raw';
      let body = undefined;
      let nonce = undefined;
      let requestType = 'POST';
      let urlElements = ['v1.0', 'devices', deviceId, 'door-lock', 'password-ticket'];
      let signStr = this.getStringToSign(query, mode, requestType, urlElements, body);
      let sign = this.getSign(timestamp, nonce, signStr);
      if (!this.ticket || ((deviceId != this.ticket.deviceId) || (deviceId == this.ticket.deviceId && timestamp > this.ticket.ticketExpiration))) {
        this.sendTuyaRequest(url, requestType, sign, timestamp, body).then(response => {
          let result = JSON.parse(response.toString());
          if (result.success) {
            this.ticket = {
              id: result.result.ticket_id,
              key: result.result.ticket_key,
              timestamp: result.t,
              ticketExpiration: result.t + result.result.expire_time,
              deviceId: deviceId
            };
            this.device[deviceId]['ticket'] = this.ticket;
            console.log(this.ticket, "this.ticket");
            resolve(true);
          }
        });
      } else {
        resolve(true);
      }
    });
  }


  public openSmartLock(deviceId) {
    return new Promise((resolve, reject) => {
      this.device[deviceId] = { 'deviceId': deviceId };
      this.getAccessToken().then(() => {
        return this.getTicket(deviceId);
      }).then(ticket => {
        return this.createTempPassword(deviceId);
      }).then(door => {
        return this.doorReadyToSync(deviceId, door);
      }).then(door => {
        return this.synchronizePassword(deviceId, door);
      }).then(door => {
        this.openDoorWithPassword(door.password).then(clearTempPass => {
          let url = `https://openapi.tuyaus.com/v1.0/devices/${this.deviceId}/door-lock/temp-passwords/rest-password`;
          let timestamp = new Date().getTime();
          let query = [];
          let mode = 'raw';
          let body = undefined;
          let nonce = undefined;
          let requestType = 'POST';
          let urlElements = ['v1.0', 'devices', this.deviceId, 'door-lock', 'temp-passwords', 'rest-password'];
          let signStr = this.getStringToSign(query, mode, requestType, urlElements, body);
          let sign = this.getSign(timestamp, nonce, signStr);
          this.sendTuyaRequest(url, requestType, sign, timestamp, body).then(response => {
            console.log("Temp passwords RESETED.");
            resolve(true);
          });
        });
      });
    });
  }

  public switchLight(value) {
    return new Promise((resolve, reject) => {
      this.getAccessToken().then(() => {
        let url = `https://openapi.tuyaus.com/v1.0/devices/${this.deviceId}/commands`;
        let timestamp = new Date().getTime();
        let query = [];
        let mode = 'raw';
        let commandValue;
        if (value == 'on') {
          commandValue = false;
        } else {
          commandValue = true;
        }
        let body = `{"commands":[{"code": "switch_led","value":${commandValue}}]}`;
        let nonce = undefined;
        let requestType = 'POST';
        let urlElements = ['v1.0', 'devices', this.deviceId, 'commands'];
        let signStr = this.getStringToSign(query, mode, requestType, urlElements, body);
        let sign = this.getSign(timestamp, nonce, signStr);
        this.sendTuyaRequest(url, requestType, sign, timestamp, body).then(response => {
          console.log(response, "response");
          resolve(true);
        });
      });
    });
  }

  public createTempPassword(deviceId): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = `https://openapi.tuyaus.com/v2.0/devices/${deviceId}/door-lock/temp-password`;
      let timestamp = new Date().getTime();
      let query = [];
      let mode = 'raw';
      let password = this.getEncryptedPassword(this.ticket.key);
      let body = `{"password":"${password}","password_type":"ticket","effective_time":${String(this.ticket.timestamp).slice(0, -3)},"invalid_time":${String(this.ticket.timestamp + 120000).slice(0, -3)},"ticket_id":"${this.ticket.id}","type":"1"}`;
      let nonce = undefined;
      let requestType = 'POST';
      let urlElements = ['v2.0', 'devices', deviceId, 'door-lock', 'temp-password'];
      let signStr = this.getStringToSign(query, mode, requestType, urlElements, body);
      let sign = this.getSign(timestamp, nonce, signStr);
      this.sendTuyaRequest(url, requestType, sign, timestamp, body).then(response => {
        let result = JSON.parse(response.toString());
        if (result.success) {
          resolve({
            password,
            ticketId: this.ticket.id,
            passwordId: result.result.id
          });
          console.log("password ID:", result.result.id);
        }
      });
    })
  }

  public synchronizePassword(deviceId, door): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = `https://openapi.tuyaus.com/v1.0/devices/${deviceId}/door-lock/issue-password`;
      let timestamp = new Date().getTime();
      let query = [];
      let mode = 'raw';
      let body = undefined;
      let nonce = undefined;
      let requestType = 'POST';
      let urlElements = ['v1.0', 'devices', deviceId, 'door-lock', 'issue-password'];
      let signStr = this.getStringToSign(query, mode, requestType, urlElements, body);
      let sign = this.getSign(timestamp, nonce, signStr);
      this.sendTuyaRequest(url, requestType, sign, timestamp, body).then(response => {
        let result = JSON.parse(response.toString());
        console.log(result, "sync pass result")
        if (result.success) {
          resolve(door);
        }
      });
    })
  }

  public openDoorWithPassword(password): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = `https://openapi.tuyaus.com/v1.0/devices/${this.deviceId}/door-lock/open-door`;
      let timestamp = new Date().getTime();
      let query = [];
      let mode = 'raw';
      // let ticketId = door.ticketId;
      console.log(this.device, "this.device");
      console.log(this.deviceId, "this.deviceId");
      let ticketId = this.device[this.deviceId].ticket.id;
      let body = `{"password":"${password}","password_type":"ticket","ticket_id":"${ticketId}"}`;
      console.log(password, ticketId, "PASS Y TICKET");
      let nonce = undefined;
      let requestType = 'POST';
      let urlElements = ['v1.0', 'devices', this.deviceId, 'door-lock', 'open-door'];
      let signStr = this.getStringToSign(query, mode, requestType, urlElements, body);
      let sign = this.getSign(timestamp, nonce, signStr);
      // let _this = this;
      //   let interval = setTimeout(function () {
      //   _this.sendTuyaRequest(url, requestType, sign, timestamp, body).then(response => {
      //     console.log("DOOR OPENED");
      //     resolve(true);
      //   });
      // },10000);

      this.sendTuyaRequest(url, requestType, sign, timestamp, body).then(response => {
        console.log("DOOR OPENED");
        resolve(true);
      });
    })
  }

  public doorReadyToSync(deviceId, door): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = `https://openapi.tuyaus.com/v1.0/devices/${deviceId}/door-lock/temp-password/${door.passwordId}`;
      let timestamp = new Date().getTime();
      let query = [];
      let mode = 'raw';
      let body = undefined;
      let nonce = undefined;
      let requestType = 'GET';
      let urlElements = ['v1.0', 'devices', deviceId, 'door-lock', 'temp-password', door.passwordId];
      let signStr = this.getStringToSign(query, mode, requestType, urlElements, body);
      let sign = this.getSign(timestamp, nonce, signStr);
      let i = 1;
      let _this = this;
      let interval = setInterval(function () {
        if (i <= 20) {
          _this.sendTuyaRequest(url, requestType, sign, timestamp, body).then(response => {
            let result = JSON.parse(response.toString());
            if (result.success) {
              if (result.result.delivery_status == '2') {
                resolve(door);
                console.log("PASSWORD SYNCHED.");
                console.log(result, "delivery status")
                clearInterval(interval);
              }
            }
          });
        } else {
          reject(true);
          console.log("ERROR PASSWORD NOT SYNCHED.");
          clearInterval(interval);
        }
        i++;
      }, 1000);

    });
  }

  private getEncryptedPassword(ticketKey) {
    let keyToEncrypt = this.decryptTicketKey(ticketKey);
    let password = this.encryptTicketKey(keyToEncrypt);
    return password;
  }

  public decryptTicketKey(ticketKey) {
    var cipherText = CryptoJS.enc.Hex.parse(ticketKey);
    var key = CryptoJS.enc.Utf8.parse(this.secret);
    var result = CryptoJS.AES.decrypt({ ciphertext: cipherText }, key, {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
    });
    var resultBase64 = CryptoJS.enc.Base64.stringify(result);
    return resultBase64.toString();
  }

  public encryptTicketKey(keyToEncrypt) {
    var cipherText = Math.floor(100000 + Math.random() * 900000).toString();
    var key = CryptoJS.enc.Base64.parse(keyToEncrypt);
    var result = CryptoJS.AES.encrypt(cipherText, key, {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
    });
    var resultBase64 = CryptoJS.enc.Base64.parse(result.toString());
    return resultBase64.toString();
  }


  private sendTuyaRequest(url, requestType, sign, t, body) {
    return new Promise((resolve, reject) => {
      let posturl = 'https://www.intelihogar.com/wp-includes/PostsPHP/tuya/tuyaCommunication.php';
      let params = `&clientId=${this.clientId}&url=${url}&requestType=${requestType}&sign=${sign}&t=${t}`;
      if (body) {
        params += `&accessToken=${this.accessToken}&body=${body}`;
      } else if (this.accessToken) {
        params += `&accessToken=${this.accessToken}`;
      }
      $.post(posturl, params, function (data) {
      })
        .done(function (data) {
          resolve(data);
        })
        .fail(function (err) {
          reject(err);
        });
    });
  }



  private getSign(timestamp, nonce = '', signStr) {
    let accessToken = this.accessToken ? this.accessToken : '';
    var str = this.clientId + accessToken + timestamp + nonce + signStr;
    var hash = CryptoJS.HmacSHA256(str, this.secret);
    var hashInBase64 = hash.toString();
    var signUp = hashInBase64.toUpperCase();
    return signUp;
  }

  private getStringToSign(query, mode, method, urlElements, body = '') {
    var sha256 = "";
    var url = "";
    var headersStr = "";
    var bodyStr = "";
    this.arr = [];
    this.map = {};
    if (query) {
      this.toJsonObj(query)
    }
    if (mode != "formdata" && mode != "urlencoded") {
      bodyStr = body;
    } else if (mode == "formdata") {
      this.toJsonObj(body);
    } else if (mode == "urlencoded") {
    }
    sha256 = CryptoJS.SHA256(bodyStr)
    this.arr = this.arr.sort()
    this.arr.forEach(item => {
      url += item + "=" + this.map[item] + "&"
    })
    if (url.length > 0) {
      url = url.substring(0, url.length - 1)
      url = "/" + urlElements.join("/") + "?" + url
    } else {
      url = "/" + urlElements.join("/")
    }

    var map = {}
    map["signUrl"] = `${method}\n${sha256}\n${headersStr}\n${url}`
    map["url"] = url
    return map["signUrl"];
  }

  private toJsonObj(params) {
    var jsonBodyStr = JSON.stringify(params);
    var jsonBody = JSON.parse(jsonBodyStr);
    jsonBody.forEach(item => {
      this.arr.push(item.key)
      this.map[item.key] = item.value
    })
  }



  //#endregion

}

export var tuya = new TuyaCommunication();