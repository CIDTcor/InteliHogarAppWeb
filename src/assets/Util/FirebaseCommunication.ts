import { getLocationKey } from './../../assets/Util/locationUtil';
import * as firebase from 'firebase';
import { Md5 } from 'md5-typescript';
import { strings } from '../../assets/Util/constants';
import { snapshotToArray, snapshotToArrayWihtKey, snapshotToJSONArray } from '../../firebase/environment';
import { InteliDevice, EnergyLog } from '../Util/InteliInterfaces';
import { FavoritesPage } from 'src/app/pages/tabs/favorites/favorites.page';
import { callbackify } from 'util';
import { promise } from 'protractor';
import { PhoneNumber } from 'src/app/modals/passwordless-auth/passwordless-auth.page';
import { rejects } from 'assert';
// import { AngularFireAuth } from '@angular/fire/auth'; 

let FIREBASE_CONFIG = {
    apiKey: "AIzaSyBtdVjV-sORR4YUSOYaadW-96iyIpdrCjI",
    authDomain: "intelihogardb.firebaseapp.com",
    databaseURL: "https://intelihogardb.firebaseio.com",
    projectId: "intelihogardb",
    storageBucket: "intelihogardb.appspot.com",
    messagingSenderId: "709660632353"
};

class FirebaseCommunication {
    // private auth: firebase.auth.Auth;
    private usersStorageRef = 'users';
    // private auth: AngularFireAuth;
    public storeDB;
    constructor() {
        firebase.initializeApp(FIREBASE_CONFIG);
        // this.auth = firebase.auth();
        let subscriptionPlans = localStorage.getItem("subscriptionPlans");
        if (subscriptionPlans) {
            this.initSubscriptionPlanListener(plans => {
                localStorage.setItem('subscriptionPlan', JSON.stringify(plans));
            });
        }
    }

    //#region Login | Auth
    // public setAuthStateListener(credential) {
    //     // this.auth.onAuthStateChanged(user => {
    //     //     credential(user);
    //     // });
    //     console.log(this.auth);
    //     this.auth.authState.subscribe(res => {
    //         if (res && res.uid) {
    //             credential(res);
    //             console.log('user is logged in');
    //         } else {
    //             credential(res);
    //             console.log('user not logged in');
    //         }
    //     });
    // }

    // public initOnGetRedirectResult(user) {
    //     this.auth.auth.getRedirectResult().then(result => {
    //         if (result) {
    //             user(result.additionalUserInfo.profile, result.credential);
    //         }
    //     }).catch(function (error) {
    //         // Handle Errors here.
    //     });
    // }

    //Email


    //Google 
    // public signInWithGoogle() {
    //     let googleProvider = new firebase.auth.GoogleAuthProvider();
    //     googleProvider.setCustomParameters({
    //         prompt: 'select_account',
    //     });
    //     this.auth.auth.signInWithRedirect(googleProvider)
    //         .then(() => {
    //             return this.auth.auth.getRedirectResult();
    //         })
    //         .catch();
    // }

    //Facebook
    // public signInWithFacebook() {
    //     var facebookProvider = new firebase.auth.FacebookAuthProvider();
    //     firebase.auth().signInWithRedirect(facebookProvider);
    // }



    //Auth

    // public checkTermsAndPrivacyStatus(callback) {
    //     let user = firebase.auth().currentUser;
    //     let userMail = user.email;
    //     firebase.database().ref(`locationsUsers/`).orderByChild('userId').equalTo(userMail).limitToFirst(1).once('value', users => {
    //         users.forEach(user => {
    //             if (user.val().statusTermsAndConditions) {
    //                 callback(true);
    //             } else {
    //                 callback(false);
    //             }
    //         })
    //     })
    // }

    public updateUserTermsAndConds(callback) {
        // let user = firebase.auth().currentUser;
        let user = JSON.parse(localStorage.getItem('sessionUser'));
        user ? user : user = JSON.parse(localStorage.getItem('savedData'));
        let userMail = this.convertDotToDash(user.userMail);
        firebase.database().ref(`users/${userMail}/userTermsAndConds/`).set(true);
        callback(true);
        // let user = JSON.parse(localStorage.getItem('sessionUser'));
        // user ? user : user = JSON.parse(localStorage.getItem('savedData'));
        // let userMail = user.userMail;
        // firebase.database().ref(`locationsUsers/`).orderByChild('userId').equalTo(userMail).once('value', users => {
        //     if (users.val()) {
        //         users.forEach(user => {
        //             firebase.database().ref(`locationsUsers/${user.key}/statusTermsAndConditions`).set(true);
        //             callback(true);
        //         })
        //     } else {
        //         callback(true)
        //     }
        // })
    }

    public getStatusTermsAndConds(userMail, callback) {
        userMail = this.convertDotToDash(userMail);
        firebase.database().ref(`users/${userMail}`).once('value', user => {
            localStorage.removeItem("userMailTmp");
            if (user.val()) {
                if (user.val().userTermsAndConds) {
                    callback(true);
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        })
    }
    //#endregion

    //#region Account
    // public updateAccount(data, callback) {
    //     let currentUser = this.auth.auth.currentUser;
    //     currentUser.updateProfile({
    //         displayName: (data.displayName) ? data.displayName : currentUser.displayName,
    //         photoURL: (data.photoURL) ? data.photoURL : currentUser.photoURL,
    //     })
    //         .then(result => {
    //             callback(result, null);
    //         })
    //         .catch(error => {
    //             callback(null, error);
    //         });
    // }


    //#endregion

    //#region User

    public checkUser(userMail, userPsw, callback) {
        const ref = firebase.database().ref(strings.references.users);
        ref.orderByChild('userMail').equalTo(userMail).once('value', snapshot => {
            let user = snapshotToArray(snapshot)[0];
            if (user && user.userPassword == Md5.init(userPsw)) {
                callback(user);
            } else {
                callback(null);
            }
        });
    }

    public createNewUser(newUserData, isDone) {
        let userMail = newUserData.userMail.toLowerCase();
        let user = {
            userName: newUserData.userName,
            userImage: (newUserData.userImage) ? newUserData.userImage : "../../assets/imgs/avatar_default.jpg",
            userMail: userMail
        }
        firebase.database().ref(strings.references.users + "/" + userMail.replace(/\./g, "-")).set(user, error => {
            if (error) {
                isDone(false);
            } else {
                isDone(true);
            }
        });
    }

    public getUserDevices(locationId, callback) {
        const ref = firebase.database().ref(`locations/${locationId}/deviceTokens`);
        ref.once("value", snapshot => {
            let devices = snapshotToArray(snapshot);
            callback(devices);
        });
    }
    //#endregion

    //#region Token
    public saveToken(userMail, locationKey, device, isDone) {
        let token = localStorage.getItem("token");
        token = (!token) ? "develop" : token;
        firebase.database().ref("locations/" + locationKey + "/deviceTokens/" + token /*userMail.replace(/\./g, "-")*/)
            .set({ pushToken: token, user: userMail, device: device }, error => {
                if (error) {
                    isDone(false);
                }
                isDone(true);
            });
    }

    public checkIfUserTokenIsAlreadyRegistered(locationId, uuid, userMail, userDevice, response) {
        let token = localStorage.getItem("token");
        token = token ? token : "develop";
        let ref = firebase.database().ref(`locations/${locationId}/deviceTokens`);
        ref.once("value", snap => {
            snap.forEach(device => {
                if (device.val().device.uuid == uuid) {
                    this.deleteRepeatedDevice(locationId, device.val().pushToken);
                } else {
                    this.saveToken(userMail, locationId, userDevice, isDone => { })
                }
            });
            this.saveToken(userMail, locationId, userDevice, isDone => { })
        });
        response(true);
    }

    public deleteRepeatedDevice(locationId, token) {
        let ref = firebase.database().ref(`locations/${locationId}/deviceTokens/${token}`);
        ref.remove();
    }

    public deleteToken(locationId, isDone) {
        let token = localStorage.getItem('token');
        if (!token) token = "develop";
        let ref = firebase.database().ref("locations/" + locationId + "/deviceTokens/" + token);
        ref.remove(error => {
            if (error) {
                isDone(false);
            }
            isDone(true);
        });
    }

    public deleteOldToken(locationId, oldToken, isDone) {
        let ref = firebase.database().ref(`locations/${locationId}/deviceTokens/${oldToken}`);
        ref.remove(error => {
            if (error) {
                isDone(false);
            }
            isDone(true);
        });
    }

    //#endregion

    //#region Location

    public getUserLocations(userMail, callback) {
        const ref = firebase.database().ref(strings.references.locationUsers)
        ref.orderByChild(strings.keys.userId)
            .equalTo(userMail)
            .on("value", snapshot => {
                let users = snapshotToArray(snapshot);
                if (users.length > 0) {
                    this.getLocationsInfo(users, locationsInfo => {
                        callback(locationsInfo);
                    });
                } else {
                    callback(false);
                }
            });
    }

    public getAllLocationUsers(locationId, callback) {
        const ref = firebase.database().ref(strings.references.locationUsers).orderByChild("locationId").equalTo(locationId)
        ref.once("value", snapshot => {
            let locations = snapshotToArray(snapshot);
            callback(locations);
        });
    }

    private getLocationsInfo(users, callback) {
        let locationsInfo = [];
        for (let i in users) {
            const ref = firebase.database().ref(`locations/${users[i].locationId}`);
            ref.on("value", response => {
                users.forEach(user => {
                    let location = response.val().locationInfo;
                    location.owner = users[i].owner;
                    location.accessLevel = users[i].accessLevel;
                    locationsInfo[i] = location;
                    callback(locationsInfo);
                });
            });
        }

        // let userLocations = [];
        // const ref = firebase.database().ref(strings.references.locations);
        // ref.on("value", snapshot => {
        //     snapshot.forEach(locationInfo => {
        //         for (let i in locationsUsers) {
        //             if (locationsUsers[i].locationId == locationInfo.val().locationId) {
        //                 let item = locationInfo.val();
        //                 item.locationInfo.owner = locationsUsers[i].owner;
        //                 item.locationInfo.accessLevel = locationsUsers[i].accessLevel;
        //                 userLocations.push(item.locationInfo);
        //             }
        //         }
        //     });
        //     callback(userLocations);
        //     userLocations = [];
        // });
    }

    public getCurrentLocationUsers(location) {
        return new Promise((resolve, reject) => {
            let locationKey = location ? location.locationKey : JSON.parse(localStorage.getItem("selectedLocation")).locationKey;
            const ref = firebase.database().ref(strings.references.locationUsers);
            ref.on("value", snapshot => {
                let counter = 0;
                snapshot.forEach(locationUser => {
                    if (locationUser.key.includes(locationKey)) {
                        counter++;
                    }
                });
                resolve(counter);
            });
        });
    }

    public getUserCurrentToken() {
        // (<any>window).FirebasePlugin.getToken(token => {
        //     return token;
        //   });
    }

    public userExistsInLocation(locationId, platforms) {
        let token = localStorage.getItem('token');
        let userToken = token ? token : 'develop';
        return new Promise((resolve, reject) => {
            const ref = firebase.database().ref(`locations/${locationId}/deviceTokens/${userToken}`);
            ref.once('value', snapshot => {
                resolve(snapshot.exists());
            });
        });
    }

    public getCurrentLocationUserCount(locationId) {
        return new Promise((resolve, reject) => {
            const ref = firebase.database().ref(`locationsUsers/`).orderByChild('locationId').equalTo(locationId);
            ref.once('value', resp => {
                resolve(Object.keys(resp.val()).length);
            })
        });
    }

    public getCurrentLocationPhones(locationId) {
        return new Promise((resolve, reject) => {
            const ref = firebase.database().ref("locations/" + locationId + "/deviceTokens");
            ref.on("value", snapshot => {
                let counter = 0;
                snapshot.forEach(token => {
                    counter++;
                });
                resolve(counter);
            });
        });
    }

    public checkDeviceStatusChange(locationId,deviceId) {
        return new Promise((resolve, reject) => {
            const ref = firebase.database().ref("locations/" + locationId + "/logs/" + deviceId + "/date");
            ref.on("value", snapshot => {   
                resolve(true);
                ref.off();            
                console.log("status changed:::::");
            });
        });
      }

    public deleteLocation(locationKey) {
        let ref = firebase.database().ref(strings.references.locationUsers + "/" + locationKey)
        ref.remove();
        this.deleteToken(locationKey.split('_')[0], isDone => {

        });
    }

    public isItAnExistingLocation(locationId, callback) {
        firebase.database().ref(strings.references.locations)
            .orderByChild(strings.keys.locationId)
            .equalTo(locationId)
            .once('value', snapshot => {
                if (snapshot.exists())
                    callback(true);
                else
                    callback(false);
            });
    }

    public isFirstUser(locationId, isFirst) {
        firebase.database().ref('locationsUsers').orderByKey().startAt(locationId).limitToFirst(1)
            .once('value', snap => {
                let result = JSON.stringify(snap.val());
                if (result && result.includes(locationId)) {
                    isFirst(false);
                } else {
                    isFirst(true);
                }
            });
    }

    public createNewLocationUser(locationId, userId) {
        firebase.database().ref(strings.references.locationUsers)
            .orderByChild(strings.keys.locationId)
            .equalTo(locationId)
            .once('value', snapshot => {
                if (snapshot.exists()) {
                    this.createNewLocationUserInFirebase(locationId, userId, false, response => { });
                    // this.createNewLocationUserInFirebase(locationId, userId, false, false, response => { });
                } else {
                    this.createNewLocationUserInFirebase(locationId, userId, true, response => { });
                    // this.createNewLocationUserInFirebase(locationId, userId, true, false, response => { });
                }
            });
    }

    public createNewLocationUserInFirebase(locationId, userId, owner, response) {
        // public createNewLocationUserInFirebase(locationId, userId, owner, statusTermsAndConditions , response) {
        this.getUserImageURL(`${userId}/`, photoURL => {
            console.log(photoURL);
            let myData = {
                accessLevel: 2,
                isActive: true,
                owner: owner,
                preferences: "",
                locationId: locationId,
                photo: photoURL ? photoURL : "",
                userId: userId,
                // statusTermsAndConditions: statusTermsAndConditions,
                superUser: false
            };
            let userIdTxt = userId.replace(/\./g, "-");
            const ref = firebase.database().ref("locationsUsers/" + locationId + "_" + userIdTxt);
            ref.set(myData);
            response(true);
        });
    }

    public changeLocationName(locationId, name) {
        const ref = firebase.database().ref(`locations/${locationId}/locationInfo/locationName`).set(name);
    }
    //#endregion

    //#region Device

    public getHubState(hub, locationKey, data) {
        let hubRef = firebase.database().ref('locations/' + locationKey + '/logs/' + hub.id);
        hubRef.off();
        hubRef.on('value', (snapshot) => {
            data(snapshot.val());
        });
    }

    public getDevices(locationKey, devicesList) {
        let deviceRef = firebase.database().ref("locations/" + locationKey + "/devices");
        deviceRef.once("value", resp => {
            let devices = snapshotToArrayWihtKey(resp);
            devicesList(devices);
        });
    }

    public getDeviceState(locationKey, callback) {
        let list;
        let devicesState = firebase.database().ref("locations/" + locationKey + "/logs");
        devicesState.on('child_added', snapshot => {
            list = [];
            list.push(snapshot.val());
            callback(list);
        });
        devicesState.on('child_changed', snap => {
            list = [];
            list.push(snap.val());
            callback(list);
        });
    }

    public getDeviceDate(locationId, deviceId, callback) {
        let ref = firebase.database().ref(`locations/${locationId}/logs/${deviceId}/date`);
        ref.once('value', date => {
            callback(date.val());
        });
    }

    public getDeviceStatus(locationId, deviceId, callback) {
        let ref = firebase.database().ref(`locations/${locationId}/notifications/${deviceId}/status`);
        ref.once('value', status => {
            callback(status.val());
        });
    }

    public saveDeviceColor(locationId, deviceId, value) {
        let ref = firebase.database().ref(`locations/${locationId}/logs/${deviceId}/value`);
        ref.set(value);
    }

    public getSpecificDeviceState(device, state) {
        let deviceRef = firebase.database().ref("locations/" + getLocationKey() + "/logs/" + device.id);
        deviceRef.on('value', snapshot => {
            state(snapshot.val());
        });
    }

    public updateDeviceRoomId(locationKey, device) {
        let ref = firebase.database().ref(`locations/${locationKey}/devices/${device.key}/roomId`);
        ref.set(device.roomId);
    }

    public updateDeviceVisibility(locationKey, device) {
        let ref = firebase.database().ref(`locations/${locationKey}/devices/${device.key}/visible`);
        ref.set(device.visible);
    }

    public updateDeviceAlias(locationKey, device) {
        firebase.database().ref(`locations/${locationKey}/devices/${device.key}/alias`).set(device.alias);
    }

    public updateDevice(locationId, device): void {
        let newDevice: InteliDevice = {
            accessLevel: device.accessLevel,
            alias: device.alias,
            help: device.help,
            id: device.id,
            name: device.name,
            roomId: device.roomId,
            type: device.type,
            visible: device.visible
        };
        let ref = firebase.database().ref(`locations/${locationId}/devices/${device.id}`);
        ref.set(newDevice);
    }

    //#endregion

    //#region Rooms
    public getRooms(locationKey, roomList) {
        const roomsRef = firebase.database().ref("locations/" + locationKey + "/rooms");
        roomsRef.on("value", resp => {
            let rooms = snapshotToArray(resp);
            rooms.sort((a, b) => {
                return a.roomOrder - b.roomOrder;
            });
            roomList(rooms);
        });
    }

    public deleteRoom(roomId, locationKey, isDone) {
        let deleteRoomRef = firebase.database().ref('locations/' + locationKey + '/rooms/' + roomId);
        deleteRoomRef.remove(error => {
            if (!error) {
                isDone(true);
            }
            isDone(false);
        });
    }

    public deleteRooms(locationKey, roomsIdList, isDone) {
        let deleteRoomRef = firebase.database().ref('locations/' + locationKey + "/rooms");
        for (let i in roomsIdList) {
            deleteRoomRef.child(roomsIdList[i]).remove(error => {
                if (error) {
                    isDone(false);
                }
                isDone(true);
            });
        }
    }

    public addNewRoom(newRoom, locationKey, isDone) {
        let newRoomRef = firebase.database().ref("locations/" + locationKey + "/rooms/" + newRoom.roomId);
        newRoomRef.set(newRoom, error => {
            if (!error) {
                isDone(true);
            }
            isDone(false);
        });
    }

    public updateRoomOrder(locationId, room, roomOrder) {
        let ref = firebase.database().ref('locations/' + locationId + "/rooms");
        ref.orderByChild('roomAlias').equalTo(room.roomAlias).limitToFirst(1).once('value', snap => {
            let keyName = Object.keys(snap.val())[0];
            firebase.database().ref(`locations/${locationId}/rooms/${keyName}/roomOrder`).set(roomOrder);
        });
    }

    public updateRoom() {

    }

    public updateRoomAliasAndIconDB(locationId, room) {
        let ref = firebase.database().ref(`locations/${locationId}/rooms/${room.roomId}`);
        ref.set(room);
    }
    //#endregion

    //#region Preferences
    public savePreferences(userId, locationId, list, isDone) {
        let preferencesRef = firebase.database().ref("locationsUsers/" + locationId + "_" + userId.replace(/\./g, "-") + "/preferences");
        preferencesRef.set(list.toString(), error => {
            if (error) {
                isDone(false);
            }
            isDone(true);
        });
    }

    public getCurrentUserFavorites(locationId, userMail, callback) {
        let ref = firebase.database().ref(`locationsUsers/${locationId}_${userMail}/favorites`);
        ref.once("value", snap => {
            if (snap.exists()) {
                let favorites = [];
                snap.forEach(device => {
                    favorites.push(device.val());
                });
                callback(favorites)
            } else {
                callback(null);
            }
        });
    }

    public getCurrentUserNotifications(locationId, userMail, callback) {
        let ref = firebase.database().ref(`locationsUsers/${locationId}_${userMail}/preferences`)
        ref.once("value", snap => {
            if (snap.exists()) {
                let notifications = [];
                if (snap.val().includes(',')) {
                    notifications = snap.val().split(',');
                } else {
                    notifications.push(snap.val());
                }
                callback(notifications);
            } else {
                callback(null);
            }
        });
    }

    public getCurrentUserAccessLevel(userMail, locationId, callback) {
        let mail = this.convertDotToDash(userMail);
        let ref = firebase.database().ref(`locationsUsers/${locationId}_${mail}/accessLevel`);
        ref.once("value", resp => {
            callback(resp.val());
        });
    }

    public addDevicesToFavorites(locationId, userMail, devices) {
        let ref = firebase.database().ref(`locationsUsers/${locationId}_${userMail}/favorites`);
        ref.set(devices);
    }

    public addSingleDeviceToFavorites(locationId, userMail, device) {
        let mail = this.convertDotToDash(userMail);
        let ref = firebase.database().ref(`locationsUsers/${locationId}_${mail}/favorites`);
        ref.push(device);
    }

    public removeSingleDeviceFromFavorites(locationId, userMail, deviceId) {
        let mail = this.convertDotToDash(userMail);
        let ref = firebase.database().ref(`locationsUsers/${locationId}_${mail}/favorites`);
        ref.once("value", snap => {
            snap.forEach(device => {
                if (device.val() == deviceId) {
                    firebase.database().ref(`locationsUsers/${locationId}_${mail}/favorites/${device.key}`).remove();
                }
            });
        })
    }

    public updateEmergencyContacts(locationId, info) {
        let ref = firebase.database().ref(`locations/${locationId}/locationInfo/locationContacts`);
        ref.set(info);
    }
    //#endregion

    //#region Routines
    public saveRoutines(locationKey, routine, isDone) {
        firebase.database().ref(`locations/${locationKey}/devices/routine_${routine.name}`).set(routine, error => {
            if (error) {
                isDone(false);
            }
            isDone(true);
        });
    }

    public deleteRoutines(locationKey, routines, isDone) {
        routines.forEach(routine => {
            firebase.database().ref(`locations/${locationKey}/devices/${routine}`).remove(error => {
                if (error) {
                    isDone(false);
                }
                isDone(true);
            });
        });
    }
    //#endregion

    //#region SECURITY
    public initAlarmListener(locationKey, alarmActive) {
        let alarmQuery = firebase.database().ref(`locations/${locationKey}/logs`).orderByChild("status").equalTo("active");
        alarmQuery.on("value", snap => {
            if (snap.exists()) {
                let device = snap.val();
                for (let key in device) {
                    device = device[key];
                    break;
                }
                alarmActive(device);
            }
        });
    }
    //#endregion

    //#region Planes
    public initSubscriptionPlanListener(callback) {
        let plansListener = firebase.database().ref('plans');
        plansListener.on("value", snap => {
            if (snap.exists()) {
                let plans = snapshotToJSONArray(snap);
                callback(plans);
            }
        });
    }

    public initLocationPlanUpdatedListener(locationId, location) {
        firebase.database().ref(`locations/${locationId}/locationInfo/locationPlan`)
            .on('value', snap => {
                // if (snap.exists()) {
                location(snap.val());
                // }
            });
    }

    public deleteAllUsersExceptSuperUser(locationId) {
        this.getLocationSuperUser(locationId, superUser => {
            let ref = firebase.database().ref(`locations/${locationId}/deviceTokens`);
            ref.orderByChild('user')
                .equalTo(superUser.userId)
                .once('value', firstUser => {
                    let owner = firstUser.val();
                    ref.remove();
                    ref.set(owner);
                })
        });
    }

    public getLocationSuperUser(locationId, callback) {
        firebase.database().ref('locationsUsers')
            .orderByChild('superUser')
            .equalTo(true)
            .once('value', snapshot => {
                snapshot.forEach(superUser => {
                    if (superUser.key.includes(locationId)) {
                        callback(superUser.val());
                    }
                });
            })
    }
    //#endregion 

    //#region Actives
    public getLocationStatus(locationId, status) {
        let ref = firebase.database().ref(`locations/${locationId}/locationInfo`);
        ref.on('value', snap => {
            status(snap.val());
        });
    }

    public getLocationInfo(locationId, info) {
        console.log(locationId);
        let ref = firebase.database().ref(`locations/${locationId}/locationInfo`);
        ref.once('value', snap => {
            info(snap.val());
        });
    }

    public userStatusInLocation(locationId, userId, status) {
        let ref = firebase.database().ref(`locationsUsers/${locationId}_${userId.replace(/\./g, "-")}`);
        ref.on('value', snap => {
            status(snap.val())
        });
    }
    //#endregion

    //#region Storage
    public uploadUserImage(userEmail: string, imgFile: string, callback): void {
        let photoStorageRef = firebase.storage().ref(`${this.usersStorageRef}/${userEmail}/avatar/photo.jpg`);
        let uploadTask = photoStorageRef.putString(imgFile, 'data_url');

        uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, snapshot => {
            callback(snapshot, null, null);
        }, error => {
            callback(null, error, null);
        }, () => {
            uploadTask.snapshot.ref.getDownloadURL()
                .then(downloadURL => {
                    callback(null, null, downloadURL);
                })
                .catch(error => {
                    callback(null, error, null);
                });
        });
    }
   public uploadUserPhoto(userEmail:string, imgFile):Promise<any>{
    return new Promise(result =>{
   const dbRef =  firebase.storage().ref(`${this.usersStorageRef}/${userEmail}/avatar/photo.jpg`);
   const dbRef2 =  firebase.storage().ref(`locationUsers/${userEmail}/photo`);
     dbRef.put(imgFile).then(snap => {
         snap.ref.getDownloadURL().then(downloadURL => {
             result(downloadURL);
             console.log(downloadURL);
         })
     })
    }) 

   }
  
    public deleteUserImage(userEmail: string, callback): void {
        let userRef = firebase.storage().ref(`${this.usersStorageRef}/${userEmail}`);
        let avatarRef = userRef.child('avatar');
        avatarRef.delete()
            .then(() => {
                callback(true, null);
            })
            .catch(error => {
                callback(null, error);
            });
    }

    public getUserImageURL(userEmail, callback) {
        // Create a reference to the file we want to download
        var ref = firebase.storage().ref(`${this.usersStorageRef}/${userEmail}`).child('avatar/photo.jpg');

        // Get the download URL
        ref.getDownloadURL().then(function (url) {
            callback(url);
        }).catch(function (error) {
            switch (error.code) {
                case 'storage/object-not-found':
                    callback("");
                    // File doesn't exist
                    break;
                case 'storage/unauthorized':
                    // User doesn't have permission to access the object
                    break;
                case 'storage/canceled':
                    // User canceled the upload
                    break;
                case 'storage/unknown':
                    // Unknown error occurred, inspect the server response
                    break;
            }
        });
    }
    //#endregion

    //#region Energy
    public getEnergyLogs(locationId: string, deviceId: string, frequency: number, log) {
        let ref = firebase.database().ref(`locations/${locationId}/modules/energy${deviceId ? '/' + deviceId : ""}`);
        let query = ref.orderByChild("date");//.limitToLast(10);
        /*switch (frequency) {
            case 0:
                query = ref.orderByKey().startAt("1565288");
                break;
            case 1:
                query = ref.orderByKey().startAt("145288");
                break;
            case 2:
                query = ref.orderByKey().startAt("135288");
                break;
        }*/

        query.on('child_added', snapshot => {
            if (snapshot) {
                let energyLog = snapshot.val();
                energyLog["key"] = snapshot.key;
                log(energyLog);
            }
        });
    }

    public getEnergyModulesCount(locationId: string) {
        let ref = firebase.database().ref(`locations/${locationId}/modules/energy/`);
    }
    //#endregion

    //#region userDevices
    public deleteUserDevice(locationId, deviceToken) {
        firebase.database().ref(`locations/${locationId}/deviceTokens/${deviceToken}`).remove();
    }

    public deleteUserLocation(locationId, userMail) {
        let mail = this.convertDotToDash(userMail);
        firebase.database().ref(`locationsUsers/${locationId}_${mail}`).remove();
    }

    public deleteAllUserDevices(locationId, userMail) {
        firebase.database().ref(`locations/${locationId}/deviceTokens/`).orderByChild('user').equalTo(userMail).once('value', snapshot => {
            snapshot.forEach(element => {
                firebase.database().ref(`locations/${locationId}/deviceTokens/${element.val().pushToken}`).remove();
            });
        });
    }

    public changeUserAccessLevel(locationUserId, user) {
        firebase.database().ref(`locationsUsers/${locationUserId}`).set(user);
    }

    private convertDotToDash(input) {
        return input.replace(/\./g, "-");
    }
    //#endregion userDevices

    //#region userImage
    public setUserImageInFirebase(locationId, userImage, userMail) {
        let locationUserId = this.convertDotToDash(`${locationId}_${userMail}`)
        
        firebase.database().ref(`locationsUsers/${locationUserId}/photo`).set(userImage);
    }
    //#endregion userImage
    public updateUserMainPhone(userEmail,phoneNumber){
        firebase.database().ref(`users/${userEmail}/phoneNumber`).set(phoneNumber);
        
    }
    public getUserMainPhone(userEmail){
          
        let phoneNumber = "";
       let ref = firebase.database().ref(`users/${userEmail}/phoneNumber`);
        ref.once('value', snap => {
            console.log(snap.val(),'snap')
             phoneNumber = snap.val();
          // resolve(true)
        })
        .catch(er => {
         // reject(false);
        });
        return(phoneNumber);
    }
}

export var dataBase = new FirebaseCommunication();
