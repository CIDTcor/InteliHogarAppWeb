var roomList;
var deviceList;
var selectTab = 0;
var selectedRoom = 0;
var da_RoomTilesList;
var da_DeviceTilesList;
var ctf_RoomTilesList;
var ctf_DeviceTilesList;
var is_DeviceTilesList;
var ie_DeviceTilesList;
var locationServer;
var locationIdentifier;
var locationToken;
var toolbarColor;
var isConnected = true;
var isHubOnline = true;

/*********************** ROOMS ***********************/

function createRoomTiles(_rooms) {
    // let rooms = sortRooms(_rooms);
    // ctf_RoomTilesList = createRoomsButtons(rooms);
    // ctf_RoomTilesList += createBackButton();
    // ctf_RoomTilesList += createModeButtons("home");
    // ctf_RoomTilesList += createModeButtons("night");
    // ctf_RoomTilesList += createModeButtons("away");
}

function sortRooms(rooms) {
    var length = rooms.length;
    for (let i = 0; i < length - 1; i++) {
        for (let j = i + 1; j < length; j++) {
            if (rooms[i].roomId > rooms[j].roomId) {
                var aux = rooms[j];
                rooms[j] = rooms[i];
                rooms[i] = aux;
            }
        }
    }
    return rooms;
}

function createRoomsButtons(rooms) {
    var elements = "";
    for (let i = 0; i < rooms.length; i++) {
        let element = "<div class='fijo st-tile' id='" + rooms[i].roomId + "' onclick='selectRoom(\"" + rooms[i].roomId + "\")'>";
        element += "  <div class='st-title'>" + rooms[i].roomAlias + "</div>";
        element += "  <div class='st-icon'>";
        if (rooms[i].roomType == "inteliGadget") {
            element += "    <img src='../assets/imgs/inteliApps/inteliApps.png'></img>";
        } else {
            element += "    <i class='intelihogarIcon icon-" + rooms[i].roomType + "'></i>";
        }
        element += "  </div>";
        element += "</div>";
        elements += element;
    }
    return elements;
}

function createBackButton() {
    let element = "<div id='backButton' class='backButton st-tile' onclick='hideButtons()' style='display: none;'>";
    element += "  <div class='st-title'>Regresar</div>";
    element += "  <div class='st-icon'>";
    element += "    <i class='intelihogarIcon icon-back'></i>";
    element += "  </div>";
    element += "</div>";
    return element;
}

function createModeButtons(mode) {
    let element = "<div id='cft_" + mode + "_" + mode + "' class='variable st-tile' style='display: none;' room='-1' onclick='sendCommand(\"mode\", \"mode_mode\", \"" + mode + "\")'>";
    element += "  <div class='st-title'>" + mode + "</div>";
    element += "  <div class='st-icon'>";
    element += "    <i class='intelihogarIcon icon-" + mode + "'></i>";
    element += "  </div>";
    element += "</div>";
    return element;
}

function selectRoom(roomId) {
    if (roomId != 0) {
        selectedRoom = roomId;
        hideRoomTiles();
        showRoomDevices(roomId);
        setTabTitle(roomId);
    }
}

function hideRoomTiles() {
    let tiles = document.getElementsByClassName('fijo');
    for (let i = 0; i <= tiles.length - 1; i++) {
        tiles[i].style.display = 'none';
    }
}

function showRoomDevices(roomId) {
    let tiles = document.getElementsByClassName('variable');
    document.getElementById('backButton').style.display = 'block';
    for (let i = 0; i <= tiles.length - 1; i++) {
        let element = tiles[i];
        if (element.getAttribute('id').includes('cft')) {
            element.style.display = 'none';
            let id = element.getAttribute('room');
            if (roomId == '0' || id == roomId) {
                element.style.display = 'block';
            }
            if (roomId == '1' && id <= 1) {
                element.style.display = 'block';
            }
        }
    }
}

function setTabTitle(roomId) {
    let location = JSON.parse(localStorage.getItem("selectedLocation")).locationName;
    let toolbar = document.getElementsByTagName("ion-header")[0].getElementsByClassName("toolbar-title")[0];
    let roomName = "";
    roomList.forEach(room => {
        if (room.roomId == roomId) {
            roomName = room.roomAlias;
        }
    });
    let title = (roomName == "") ? location : roomName;
    toolbar.innerHTML = "<img height='20px' src='../../assets/imgs/inteliApps/inteliAppsGeneral-on.png' width='20px'>&nbsp;&nbsp;<span>" + title + "</span>";
}

function setHeaderTitle(roomAlias, room, roomType = null) {
    if (isConnected && isHubOnline) {
        let toolbar = document.getElementsByTagName('ion-toolbar')[0];
        let headerImage = toolbar.getElementsByClassName('headerImage')[0];
        let headerTitle = toolbar.getElementsByClassName('locationCaption')[0];
        let headerTitleDesktop = document.getElementsByClassName('locationNameTitle')[0];
        let headerIcon = toolbar.getElementsByClassName('headerIcon')[0];
        let headerIconDesktop = document.getElementById('roomIcon');
        let result;
        headerTitle.innerHTML = roomAlias;
        headerTitleDesktop.innerHTML = roomAlias;
        console.log("headerTitleDesktop", headerTitleDesktop);
        headerImage.style = "display:initial"
        headerIcon.style = "display:none"
        switch(room) { 
            case "confort":
                result = "../../assets/imgs/inteliApps/helloHome-on.png";
                headerTitle.style = "color: #5C8FF0; font-weight:bold;";
                break;
            case "favorites":
                result = "../../assets/imgs/inteliApps/inteliAppsGeneral-on.png";
                headerTitle.style = "color: #5C8FF0; font-weight:bold;";
                break;
            case "security":
                result = "../../assets/imgs/inteliApps/inteliAppsSecurity-on.png";
                headerTitle.style = "color: red; font-weight:bold;";
                break;
            case "energy":
                result = "../../assets/imgs/inteliApps/inteliAppsEnergy-on.png";
                headerTitle.style = "color: green; font-weight:bold;";
                break;
            case "room":
                headerImage.style = "display:none"
                headerIcon.style = "display:initial"
                headerIcon.className = `headerIcon icon-${roomType}`;
                headerIconDesktop.className = `headerIcon icon-${roomType}`;
                console.log("headerIconDesktop", headerIconDesktop);
                break;
        }
        headerImage.src = result;
        // let toolbar = document.getElementsByTagName("ion-toolbar")[0].getElementsByClassName("asddsad")[0];
        // toolbar.innerHTML = "<img height='20px' src='../../assets/imgs/inteliApps/inteliAppsGeneral-on.png' width='20px'>&nbsp;&nbsp;<span>" + roomAlias + "</span>";
    }

    //TODO: manage this method with a new way
}

function setHeaderColorTitle(backgroundColor, textTitle, colorTitle) {
    // let toolbar = document.getElementsByTagName("ion-header")[0];
    // let background = toolbar.getElementsByTagName("ion-toolbar")[0].getElementsByClassName("toolbar-background")[0];
    // let contentTitle = toolbar.getElementsByClassName("toolbar-title")[0];
    // background.style.backgroundColor = backgroundColor;
    // contentTitle.innerHTML = "<img height='20px' src='../../assets/imgs/inteliApps/inteliAppsGeneral-on.png' width='20px'>&nbsp;&nbsp;<span>" + textTitle + "</span>";
    // contentTitle.getElementsByTagName('span')[0].style.color = colorTitle;
    //TODO: manage this method with a new way --same
}

/*********************** WIDGETS ***********************/

function createWidgets() {
    let elements = "";
    elements += createWeatherButton();
    da_RoomTilesList = elements;
}

function createWeatherButton() {
    let element = "<div id='weather2' class='fijo st-tile2'>";
    element += "  <div class='st-title'></div>";
    element += "  <div style='position: relative; top: 50%; margin-top: -60px; left: 50%,'>";
    element += "      <iframe async src='https://www.meteored.mx/getwid/8af614279497f61c92ac4d91f849dbf1'></iframe>";
    element += "  </div>";
    element += "</div>";
    return element;
}

/*********************** DIRECT ACCESS ***********************/

function showDirectAccess() {
    let tiles = document.getElementsByClassName('variable');
    for (let i = 0; i <= tiles.length - 1; i++) {
        tiles[i].style.display = 'none';
    }
    hideWidgets();
    let shortcutList = JSON.parse(localStorage.getItem("shortcuts_" + locationIdentifier));
    if (shortcutList) {
        shortcutList.forEach(shortcut => {
            if (shortcut == "Widgets")
                showWidgets();
            else {
                if (shortcut != "home_home" && shortcut != "away_away" && shortcut != "night_night")
                    try {
                        document.getElementById("da_" + shortcut).style.display = 'block';
                    } catch (e) { }
            }
        });
    }
}

function hideWidgets() {
    document.getElementById('weather2').style.display = 'none';
}

function showWidgets() {
    document.getElementById('weather2').style.display = 'block';
}

function setBackground(id) {
    var resolution = (screen.width <= 360) ? "360x598" : (screen.width <= 1024) ? "1024x768" : (screen.width <= 1280) ? "1280x800" : "1920x1080";
    var bgImage = "url('https://s3.amazonaws.com/intelihogar/00001/Default" + resolution + ".jpg')";
    if (id != -1) {
        for (let i = 0; i < this.rooms.length; i++) {
            if (this.rooms[i].roomId == id) {
                var extension = rooms[i].roomImage.indexOf(".jpg");
                bgImage = "url('" + rooms[i].roomImage.substring(0, extension) + resolution + ".jpg')";
            }
        }
    }
    document.getElementById("fondo").style.backgroundImage = bgImage;
}

/*********************** DEVICES ***********************/

function createDeviceTiles(devices) {
    //saveLocationPreferences(null, devices);
    da_DeviceTilesList = createDevice("da", devices);
    //da_DeviceTilesList += addShortCutButton("da");
    ctf_DeviceTilesList = createDevice("cft", devices);
    is_DeviceTilesList = createDevice("is", devices);
    ie_DeviceTilesList = createDevice("ie", devices);
}

function createDevice(page, devices) {
    /*this.devices = devices;
    var elements = "";
    for (let i = 0; i < devices.length; i++) {
        if (devices[i].type != 'mode') {
            let element = "<div id='" + page + "_" + devices[i].id + "' class='variable st-tile " + devices[i].type + "' type='" + devices[i].type + "' room='" + devices[i].roomId + "' onclick='sendCommand(\"" + devices[i].type + "\", \"" + devices[i].id + "\", \"toggle\")' (press)='showDeviceOptions(" + devices[i] + ")' style='display:none;'>";
            let alias = ((page == "da") ? getRoomName(devices[i].roomId) : "") + devices[i].alias;
            element += "  <div class='st-title'>" + alias + "</div>";
            element += "  <div class='st-icon'>";
            if (devices[i].type == "temperature" || devices[i].type == "humidity" || devices[i].type == "battery" || devices[i].type == "energy" || devices[i].type == "power" || devices[i].type == "monoxide") {
                element += "    <p class='textStatus'>!</p>";
            } else if (devices[i].type == "inteliAppsSecurity" || devices[i].type == "inteliAppsGeneral" || devices[i].type == "inteliAppsMedical" || devices[i].type == "inteliAppsEnergy" || devices[i].type == "routine") {
                element += "    <img src='../assets/imgs/inteliApps/" + devices[i].type + ".png'></img>";
            } else {
                element += "    <i class='intelihogarIcon icon-" + devices[i].type + "'></i>";
            }
            element += "  </div>";
            element += "  <div class='full-width-slider' style='display:none'>";
            element += "    <input name='slider_" + devices[i].id + "' id='" + page + "_slider_" + devices[i].id + "' type='range' min='0' max='100' value='50' class='slider' oninput='document.getElementById(\"" + page + "_label_dimmer_" + devices[i].id + "\").innerHTML = this.value;' onchange='sendCommand(\"" + devices[i].type + "\", \"" + devices[i].id + "\", this.value)'>";
            element += "    <label id='" + page + "_label_dimmer_" + devices[i].id + "' class='ui-hidden-accessible'></label>";
            element += "  </div>";
            element += "  <div class='footer' style='display:none;'><i id='" + page + "_icon-footer-" + devices[i].id + "' class='icon-" + devices[i].type + "' style='font-size: 2em;'></i><span></span></div>";
            if (true) {
                element += "  <div class='footer-right' onclick='document.getElementById(\"" + page + "_deviceOptions_" + devices[i].id + "\").style.display = \"block\";'>";
                element += "    &gt; ";
                element += "  </div>";
            }
            element += "</div>";
            elements += element;
        }
    }
    return elements;*/
}

function getRoomName(roomId) {
    let locationId = JSON.parse(localStorage.getItem("selectedLocation")).locationId;
    let rooms = JSON.parse(localStorage.getItem("roomList_" + locationId));
    let roomName = "";

    rooms.forEach(room => {
        if (roomId == room.roomId && room.roomAlias != "InteliApps") {
            roomName = room.roomAlias + ": ";
        }
    });
    return roomName;
}

function showHelp(page, help) {
    /*isTileHelpPressed = true;
    document.getElementById(page + "_helpAlert").style.display = "block";
    document.getElementById(page + "_helpTxt").innerHTML = help;*/
    if (!help) {
        help = "Este dispositivo no cuenta con ayuda para mostrar.";
    }
    alert(help);
}

function closeDeviceOptions(deviceId) {
    document.getElementById(deviceId).style.display = "none";
}

function addDeviceToShortcuts(deviceId) {
    let locationIdentifier = JSON.parse(localStorage.getItem("selectedLocation")).locationIdentifier;
    let notPreferences = JSON.parse(localStorage.getItem("notifications_" + locationIdentifier));

    if (!notPreferences)
        notPreferences = [];

    if (!notPreferences.includes(deviceId)) {
        notPreferences.push(deviceId);
        localStorage.setItem("notifications_" + locationIdentifier, JSON.stringify(notPreferences));
        alert("El dispositivo ha sido agrgado a notificaciones correctamente.")
    } else {
        alert("El dispositivo ya ha sido agregado a notificaciones.")
    }
}

function addShortCutButton(page) {
    let element = "<div id='" + page + "_addShortCutButton' class='fijo st-tile' style='display:block;' onClick='showInputAlert(\"shortcuts\")'>";
    element += "  <div class='st-title'>Adicionar acceso directo</div>";
    element += "  <div class='st-icon'>";
    //element += "    <i class='intelihogarIcon icon-'></i>";
    element += "    +";
    element += "  </div>";
    element += "</div>";
    return element;
}

function createColorPickerWindow(page, device) {
    let element = "";
    element += "<div id='" + page + "_window_" + device.type + "_" + device.id + "' class='floating-background' style='display:none;'>";
    element += "    <div id='" + page + "_container_" + device.type + "_" + device.id + "' class='thermo-container'>";
    element += "        <div class='thermo-title color-title'>" + device.name + "</div>";
    element += "        <div id='" + page + "_colorWheel_" + device.id + "' class='color-picker'></div>";
    element += "        <div class='thermo-close-button' onclick='closeColorLight(\"" + page + "_window_" + device.type + "_" + device.id + "\")'>";
    element += "            <span style='font-weight: bold;'>&times;</span>";
    element += "        </div>";
    element += "    </div>";
    element += "</div>";
    document.getElementById(page + '_floating-windows').innerHTML += element;
}

function createContextualWindow(page, device) {
    let element = "";
    element += "<div id='" + page + "_deviceOptions_" + device.type + "_" + device.id + "' class='floating-background' style='display:none;'>";
    element += "    <div id='container' class='options-container'>";
    element += "       <div class='option-header'>";
    element += "           <span class='option-close' onclick='closeDeviceOptions(\"" + page + "_deviceOptions_" + device.type + "_" + device.id + "\")'>&times;</span>";
    element += "       </div>";
    element += "       <div id='" + page + "_contextualWindowDirectAccess_" + device.type + "_" + device.id + "' class='option-item ion-md-grid'>&nbsp;Acceso directo</div>";
    element += "       <div id='" + page + "_contextualWindowHelp_" + device.type + "_" + device.id + "' class='option-item ion-md-help-circle' onclick='showHelp(null," + device.help + ")'>&nbsp;Ayuda</div>";
    element += "       <div id='" + page + "_contextualWindowNotifications_" + device.type + "_" + device.id + "' class='option-item ion-md-notifications' onclick='addDeviceToShortcuts(\"" + device.type + "_" + device.id + "\")'>&nbsp;Notificación</div>";
    element += "       <div id='" + page + "_contextualWindowEdit_" + device.type + "_" + device.id + "' class='option-item ion-md-color-palette'>&nbsp;Personalizar</div>";
    element += "    </div>";
    element += "</div>";
    document.getElementById(page + '_floating-windows').innerHTML += element;
}

function renderThermostat(page, _deviceId, _sliderType, _value, _status) {
    let _radius = (screen.width <= 680) ? 75 : 150;
    let _width = (screen.width <= 680) ? 15 : 10;
    let _handlerSize = (screen.width <= 680) ? "+3" : "+5";
    $("#" + page + "_thermo-slider-temp_" + _deviceId).roundSlider({
        radius: 75,
        width: 15,
        handleSize: "+3",
        sliderType: _sliderType,
        circleShape: "pie",
        startAngle: 315,
        editableTooltip: false,
        min: 10,
        max: 40,
        value: _value,
        change: function (args) {
            var value = this.getValue();
            var valueInf = parseInt(value) - 1;
            var valueSup = parseInt(value) + 1;
            var settingValue = valueInf + "," + valueSup;
            console.log("Temperatura: " + value);
            sendCommand("thermostat", _deviceId, settingValue);
        }
    });
}

function renderRoundSlider(_deviceId, _sliderType, _value, _status) {
    let _radius = (screen.width <= 680) ? 75 : 150;
    let _width = (screen.width <= 680) ? 15 : 10;
    let _handlerSize = (screen.width <= 680) ? "+3" : "+5";
    document.getElementById("thermo-slider-temp").style.display = (_sliderType == "default") ? 'block' : 'none';
    $("#thermo-slider-temp").roundSlider({
        radius: 75,
        width: 15,
        handleSize: "+3",
        sliderType: _sliderType,
        circleShape: "pie",
        startAngle: 315,
        editableTooltip: false,
        min: 10,
        max: 40,
        value: _value,
        change: function (args) {
            var value = this.getValue();
            var valueInf = parseInt(value) - 1;
            var valueSup = parseInt(value) + 1;
            var settingValue = valueInf + "," + valueSup;
            console.log("Temperatura: " + value);
            sendCommand("thermostat", _deviceId, settingValue, isDone => {

            });
        }
    });
}

function showColorLights(page, deviceColorLight, _deviceId, _deviceStatus) {
    document.getElementById(page + "_" + deviceColorLight).style.display = "block";
    colorPickerId = _deviceId;
    let _size = (screen.width <= 680) ? 185 : 370;

    let colorArray = _deviceStatus.split(",");
    let _H = parseInt(colorArray[1]) * 3.60;
    let _S = parseInt(colorArray[2]);
    let _L = 50;
    let _color = {
        "h": _H,
        "s": _S,
        "l": _L
    };

    colorPicker = new iro.ColorPicker("#" + page + "_colorWheel_" + _deviceId, {
        width: (screen.width <= 360) ? 304 : (screen.width > 360 && screen.width <= 375) ? 330 : 370,
        height: (screen.width <= 360) ? 304 : (screen.width > 360 && screen.width <= 375) ? 330 : 370,
        markerRadius: 8,
        color: _color,
        borderWidth: 2,
        padding: 4,
        wheelLightness: true,
        css: {
            ".colorPickerContainer": {
                "background-color": "$color",
                "color": "$color"
            }
        },
        layout: [
            { 
              component: iro.ui.Wheel,
              options: {}
            },
          ]
    });

    colorPicker.on("input:end", function (color, changes) {
        var colorSelected = color.hsv;
        var hsv = parseInt(colorSelected.h) + "," + parseInt(colorSelected.s) + "," + parseInt(colorSelected.v);
        sendCommand("colorLight", "colorLight_" + colorPickerId, hsv);
        document.getElementById(page + "_colorLight_" + colorPickerId).getElementsByTagName('div')[1].getElementsByTagName('i')[0].setAttribute("style", "color:hsl(" + colorSelected.h + "," + colorSelected.s + "%,50%) !important");
    });
}

function showColorWheel(_deviceId, _deviceStatus, isDone) {
    colorPickerId = _deviceId;
    let _size = (screen.width <= 680) ? 185 : 370;

    // let colorArray = _deviceStatus;
    let _H = parseInt(_deviceStatus.hue) * 3.60;
    let _S = parseInt(_deviceStatus.saturation);
    let _L = 50;
    let _color = {
        "h": _H,
        "s": _S,
        "l": _L
    };

    colorPicker = new iro.ColorPicker("#colorWheel", {
        width: (screen.width <= 360) ? 304 : (screen.width > 360 && screen.width <= 375) ? 330 : 370,
        height: (screen.width <= 360) ? 304 : (screen.width > 360 && screen.width <= 375) ? 330 : 370,
        markerRadius: 8,
        color: _color,
        borderWidth: 2,
        padding: 4,
        wheelLightness: true,
        css: {
            ".colorPickerContainer": {
                "background-color": "$color",
                "color": "$color"
            }
        },
        layout: [
            { 
              component: iro.ui.Wheel,
              options: {}
            },
          ]
    });

    colorPicker.on("input:end", function (color, changes) {
        var colorSelected = color.hsv;
        let dismissData = {};
        dismissData.colorSelected = colorSelected;
        var hsv = parseInt(colorSelected.h) + "," + parseInt(colorSelected.s) + "," + parseInt(colorSelected.v);
        sendCommand("colorLight", colorPickerId, hsv, done => {
            dismissData.done = done;
            isDone(dismissData);
        });
    });
}

function closeColorLight(deviceColorLight) {
    document.getElementById(deviceColorLight).style.display = "none";
    localStorage.setItem("openColorLight", "");
    colorPicker = null;
    $("svg").remove();
}

function closeFloatingWindow(window) {
    document.getElementById(window).style.display = "none";
}

function showSecurityTiles() {
    let tiles = document.getElementsByClassName('variable');
    for (let i = 0; i <= tiles.length - 1; i++) {
        let element = tiles[i];
        if (element.getAttribute('id').includes('is')) {
            element.style.display = 'none';
            if (element.getAttribute('room') == "-1") {
                element.style.display = 'block';
            }
        }
    }
}

function showEnergyTiles() {
    let tiles = document.getElementsByClassName('variable');
    for (let i = 0; i <= tiles.length - 1; i++) {
        let element = tiles[i];
        if (element.getAttribute('id').includes('ie')) {
            element.style.display = 'none';
            if (element.getAttribute('room') == "-2") {
                element.style.display = 'block';
            }
        }
    }
}

/*********************** UPDATE ***********************/

function getDevicesStateFromST() {
    let url = "https://" + locationServer + "/api/smartapps/installations/" + locationIdentifier + "/data/?access_token=" + locationToken + "&callback=?";
    $.getJSON(url)
        .done(data => {
            updateDevices(data);
        })
        .fail((jqxhr, textStatus, error) => {
            console.log("Response: " + error + " Estado: " + textStatus);
        });
}

function updateDevices(devices) {
    updateDevicesByPage(devices, "da");
    updateDevicesByPage(devices, "cft");
    updateDevicesByPage(devices, "is");
    updateDevicesByPage(devices, "ie");
}

function updateDevicesByPage(devices, page) {
    /*updateHubState(devices.hub);

    updateDeviceIcon(devices.locks, "toggle", page);
    updateDeviceIcon(devices.garages, "toggle", page);
    updateDeviceIcon(devices.lockDoors, "toggle", page);
    updateDeviceIcon(devices.switches, "toggle", page);
    updateDeviceIcon(devices.sirenas, "toggle", page);
    updateDeviceIcon(devices.alarmas, "toggle", page);
    updateDeviceIcon(devices.alertas, "toggle", page);
    updateDeviceIcon(devices.momentary, "toggle", page);
    updateDeviceIcon(devices.switchLights, "toggle", page);
    updateDeviceIcon(devices.dimmers, "level", page);
    updateDeviceIcon(devices.curtains, "level", page);
    updateDeviceIcon(devices.windows, "level", page);
    updateDeviceIcon(devices.contacts, "none", page);
    updateDeviceIcon(devices.doorContacts, "none", page);
    updateDeviceIcon(devices.windowContacts, "none", page);
    updateDeviceIcon(devices.presence, "none", page);
    updateDeviceIcon(devices.motion, "none", page);
    updateDeviceIcon(devices.valves, "none", page);
    updateDeviceIcon(devices.smokes, "none", page);
    updateDeviceIcon(devices.water, "none", page);

    updateDeviceIcon(devices.inteliAppsGeneral, "toggle", page);
    updateDeviceIcon(devices.inteliAppsMedical, "toggle", page);
    updateDeviceIcon(devices.inteliAppsSecurity, "toggle", page);
    updateDeviceIcon(devices.inteliAppsEnergy, "toggle", page);

    updateDeviceState(devices.temperature, page);
    updateDeviceState(devices.humidity, page);
    updateDeviceState(devices.battery, page);
    updateDeviceState(devices.energy, page);
    updateDeviceState(devices.power, page);
    updateDeviceState(devices.monoxide, page);

    updateDeviceAdjustableLight(devices.adjustableLights, page);
    updateDeviceColorLight(devices.colorLights, page);
    updateDeviceThermostat(devices.shower, page);
    updateDeviceThermostat(devices.thermostats, page);

    //updateMode(state.modo);*/
}

function updateMode(state) {

}

function updateHubState(hub) {
    let locationTitle = (localStorage.getItem('selectedLocation') != null) ? JSON.parse(localStorage.getItem('selectedLocation')).locationName : 'I am an easter egg';
    if (hub) {
        if (hub.status == "on") {
            setHeaderColorTitle('#f8f8f8', locationTitle, '#424242');
            isHubOnline = true;
        } else {
            setHeaderColorTitle('firebrick', 'Casa desconectada', 'aliceblue');
            isHubOnline = false;
        }
    }
}

function updateDeviceIcon(devices, value, page = 'da') {
    if (devices) {
        for (let i = 0; i < devices.length; i++) {
            let element = document.getElementById(page + "_" + devices[i].type + "_" + devices[i].id);
            if (element) {
                let status = (devices[i].status) ? "-" + devices[i].status : "";
                if (devices[i].type == "inteliAppsSecurity" || devices[i].type == "inteliAppsGeneral" || devices[i].type == "inteliAppsMedical" || devices[i].type == "inteliAppsEnergy" || devices[i].type == "routine") {
                    element.getElementsByTagName("div")[1].getElementsByTagName("img")[0].setAttribute("src", "../assets/imgs/inteliApps/" + devices[i].type + status + ".png");
                } else {
                    status = (value == "level" && devices[i].level > 0) ? "-on" : status;
                    element.getElementsByTagName("div")[1].getElementsByTagName("i")[0].setAttribute("class", "intelihogarIcon icon-" + devices[i].type + status);
                }
                if (value != "none") {
                    // element.setAttribute("onclick", "sendCommand('" + devices[i].type + "', '" + devices[i].type + "_" + devices[i].id + "', 'toggle')");
                    if (value == "level") {
                        element.getElementsByTagName("div")[2].style.display = "block";
                        let level = devices[i].level;
                        element.getElementsByTagName("div")[2].getElementsByTagName("input")[0].setAttribute("value", level);
                        element.getElementsByTagName("div")[2].getElementsByTagName("input")[0].setAttribute("onchange", "sendCommand('" + devices[i].type + "', '" + devices[i].type + "_" + devices[i].id + "', this.value)");
                    }
                }
                if (document.getElementById("deviceOptions_" + devices[i].id)) { }
                else {
                    createContextualWindow(page, devices[i]);
                }
            } else {
                console.log(`Dispositivo <<${devices[i].name}>> no se encuentra en su base de datos.`);
            }
        }
    }
}

function updateDeviceState(devices, page = 'da') {
    if (devices) {
        for (let i = 0; i < devices.length; i++) {
            var element = document.getElementById(page + "_" + devices[i].type + "_" + devices[i].id);
            if (element) {
                let status = devices[i].status;
                if (status == "") status = "!";
                element.getElementsByTagName("div")[1].getElementsByTagName("p")[0].innerHTML = status + getMeterUnit(devices[i].type);
                element.getElementsByTagName("div")[3].style.display = "block";
                if (document.getElementById("deviceOptions_" + devices[i].id)) { }
                else {
                    createContextualWindow(page, devices[i]);
                }
            } else {
                console.log(`Dispositivo <<${devices[i].name}>> no se encuentra en su base de datos.`);
            }
        }
    }
}

function getMeterUnit(type) {
    let unit = "";
    switch (type) {
        case "temperature":
            unit = "°C";
            break;
        case "humidity":
        case "battery":
            unit = "%";
            break;
        case "energy":
        case "power":
        case "generator":
            unit = "kWh";
            break;
    }
    return unit;
}

function updateDeviceAdjustableLight(devices, page = 'da') {
    if (devices) {
        for (let i = 0; i < devices.length; i++) {
            let element = document.getElementById(page + "_" + devices[i].type + "_" + devices[i].id);
            if (element) {
                element.getElementsByTagName("div")[1].getElementsByTagName("i")[0].setAttribute("class", "intelihogarIcon icon-" + devices[i].type + "-" + devices[i].status);
                element.getElementsByTagName("div")[2].style.display = "block";
                let level = devices[i].level;
                element.getElementsByTagName("div")[2].getElementsByTagName("input")[0].value = level;
                element.getElementsByTagName("div")[2].getElementsByTagName("label")[0].innerHTML = level;
                if (document.getElementById("deviceOptions_" + devices[i].id)) { }
                else {
                    createContextualWindow(page, devices[i]);
                }
            } else {
                console.log(`Dispositivo <<${devices[i].name}>> no se encuentra en su base de datos.`);
            }
        }
    }
}

function updateDeviceColorLight(devices, page = 'da') {
    if (devices) {
        for (let i = 0; i < devices.length; i++) {
            var element = document.getElementById(page + "_" + devices[i].type + "_" + devices[i].id);
            let status = devices[i].status;
            if (!Array.isArray(status))
                status = status.split("[")[1].split("]")[0].split(",");
            if (element) {
                element.getElementsByTagName("div")[1].getElementsByTagName("i")[0].setAttribute("class", "intelihogarIcon icon-" + devices[i].type + "-" + status[0]);
                let hue = status[1] * 3.6;
                element.getElementsByTagName("div")[1].getElementsByTagName("i")[0].setAttribute("style", "color:hsl(" + hue + "," + status[2] + "%, 50%) !important");
                element.getElementsByTagName("div")[2].style.display = "block";
                var level = (status[0] == "on") ? status[3] : "0";
                element.getElementsByTagName("div")[2].getElementsByTagName("input")[0].setAttribute("value", level);
                element.getElementsByTagName("div")[2].getElementsByTagName("label")[0].innerHTML = level;
                element.setAttribute("onclick", "showColorLights('" + page + "','window_" + devices[i].type + "_" + devices[i].id + "', '" + devices[i].id + "', '" + status + "')");

                if (document.getElementById(page + "_window_" + devices[i].type + "_" + devices[i].id) == null) {
                    createColorPickerWindow(page, devices[i]);
                }
                if (document.getElementById("deviceOptions_" + devices[i].id)) { }
                else {
                    createContextualWindow(page, devices[i]);
                }
            } else {
                console.log(`Dispositivo <<${devices[i].name}>> no se encuentra en su base de datos.`);
            }
        }
    }
}

function updateDeviceThermostat(devices, page) {
    if (devices) {
        for (let i = 0; i < devices.length; i++) {
            let element = document.getElementById(page + "_" + devices[i].type + "_" + devices[i].id);
            let status = devices[i].status;
            if (!Array.isArray(status))
                status = status.split("[")[1].split(",");
            if (element) {
                element.getElementsByTagName("div")[1].getElementsByTagName("i")[0].setAttribute("class", "intelihogarIcon icon-" + devices[i].type + "-" + status[4]);
                element.getElementsByTagName("div")[3].style.display = "block";
                element.getElementsByTagName("div")[3].getElementsByTagName("span")[0].innerHTML = status[0] + "°C";
                element.getElementsByTagName("div")[3].getElementsByTagName("i")[0].setAttribute("class", "icon-temperature");
                element.setAttribute("onclick", "document.getElementById('" + page + "_window_" + devices[i].type + "_" + devices[i].id + "').style.display = 'block';");
                if (document.getElementById(page + "_window_" + devices[i].type + "_" + devices[i].id) == null)
                    createWindowThermostat(page, devices[i]);
                else
                    updateWindowThermostat(page, devices[i]);
                if (document.getElementById("deviceOptions_" + devices[i].id)) { }
                else {
                    createContextualWindow(page, devices[i]);
                }
            } else {
                console.log(`Dispositivo <<${devices[i].name}>> no se encuentra en su base de datos.`);
            }
        }
    }
}

function createWindowThermostat(page, device) {
    let element = "";
    element += "<div id='" + page + "_window_" + device.type + '_' + device.id + "' class='floating-background' style='display:none;'>";
    element += "    <div id='" + page + "_container_" + device.type + "_" + device.id + "' class='thermo-container'>";
    element += "        <div class='thermo-title'>" + device.name + "</div>";
    element += "        <div class='thermo-mode' onclick='sendCommand(\"thermostat\", \"" + device.type + "_" + device.id + "\", \"toggle\");'>";
    element += "            <span id='" + page + "_thermo-mode_thermostat-temp_" + device.id + "'>";
    element += (device.status[4] == 'off' || device.status[4] == null) ? 'Apagado' : (device.status[4] == 'heat') ? 'Calentando' : (device.status[4] == 'auto') ? 'Auto' : (device.status[4] == 'cool') ? 'Enfriando' : 'Extra';
    element += "            </span>";
    element += "        </div>";
    element += "        <div class='thermo-temp' onclick='sendCommand(\"thermostat\", \"" + device.type + "_" + device.id + "\", \"toggle\");'>" + Math.round(device.status[0]) + "°c</div>";
    element += "        <div id = '" + page + "_thermo-mode-icon_" + device.id + "' class='thermo-icon' onclick='sendCommand(\"thermostat\", \"" + device.type + "_" + device.id + "\", \"toggle\");'>";
    element += "            set<i class='intelihogarIcon icon-thermostat-" + device.status[4] + "' style='visibility: hidden'></i>";
    element += "        </div>";
    element += "        <div class='thermo-close' onclick='sendCommand(\"thermostat\", \"" + device.type + "_" + device.id + "\", \"setThermostatOff\");'><i class='intelihogarIcon icon-Apagar'></i></div>";
    element += "        <div class='thermo-close-button' onclick='closeFloatingWindow(\"" + page + "_window_" + device.type + '_' + device.id + "\")'>";
    element += "            <span style='font-weight: bold;'>&times;</span>";
    element += "        </div>";
    element += "        <div id='" + page + "_thermo-slider-temp_" + device.id + "' class='thermo-slider'></div>";
    element += "    </div>";
    element += "</div>";
    document.getElementById(page + "_floating-windows").innerHTML += element;
}

function updateWindowThermostat(page, device) {
    var element = document.getElementById(page + "_window_" + device.type + "_" + device.id);
    let status = device.status;
    if (!Array.isArray(status))
        status = status.split("[")[1].split(",");
    if (element) {
        element.getElementsByTagName("div")[0].getElementsByTagName("div")[1].getElementsByTagName("span")[0].innerHTML = (status[4] == 'off' || status[4] == null) ? 'Apagado' : (status[4] == 'heat') ? 'Calentando' : (status[4] == 'auto') ? 'Auto' : (status[4] == 'cool') ? 'Enfriando' : 'Extra';
        element.getElementsByTagName("div")[0].getElementsByTagName("div")[2].innerHTML = Math.round(status[0]) + "°c";
        element.getElementsByTagName("div")[0].getElementsByTagName("div")[3].innerHTML = "&nbsp;SET<i class='intelihogarIcon icon-thermostat-" + status[4] + "' style='visibility:hidden'></i>";
        var _sliderType = (status[4] == "off" || status[4] == null) ? "none" : "default";
        renderThermostat(page, device.id, _sliderType, parseInt(status[1]) + 1, status[4]);
        element.getElementsByTagName("div")[0].getElementsByTagName("div")[6].style.display = (_sliderType == "default") ? "block" : "none";
        if (document.getElementById("deviceOptions_" + device.id)) { }
        else {
            createContextualWindow(page, device);
        }
    } else {
        console.log(`Dispositivo <<${devices[i].name}>> no se encuentra en su base de datos.`);
    }
}

/*********************** OTHERS ***********************/

function hideButtons() {
    document.getElementById("backButton").style.display = "none";
    let deviceList = document.getElementsByClassName("variable");
    if (deviceList.length > 0) {
        for (let j = 0; j < deviceList.length; j++) {
            deviceList[j].style.display = "none";
        }
    }
    deviceList = document.getElementsByClassName("fijo");
    if (deviceList.length > 0) {
        for (let j = 0; j < deviceList.length; j++) {
            deviceList[j].style.display = "block";
        }
    }
    setTabTitle("");
}

function showDeviceOptions(device) {
    console.log(JSON.stringify(device));
}