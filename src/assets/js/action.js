//<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.0.0/crypto-js.min.js"></script>
const {
    JsonFormatter
} = require("tslint/lib/formatters");

var isTileHelpPressed = false;

function sendCommand(type, id, value, isDone) {
    if (!isTileHelpPressed) {
        if (type == "routine") {
            sendMultipleAjaxCommand(id, done => {
                isDone(done);
            });
        } else {
            sendAjaxCommand(type, id, value, done => {
                isDone(done);
            });
        }
    } else {
        isTileHelpPressed = false;
    }
}

function sendMultipleAjaxCommand(id, isDone) {
    let devices = JSON.parse(localStorage.getItem("devices"));
    devices.forEach(device => {
        if (device.id == id) {
            let actions = device.devices;
            actions.forEach(action => {
                sendDeviceCommand(action, done => {
                    isDone(done);
                });
            });
        }
    });
}

function sendDeviceCommand(id, isDone) {
    let devices = JSON.parse(localStorage.getItem("devices"));
    devices.forEach(device => {
        if (device.id == id) {
            sendAjaxCommand(device.type, device.id, "toggle", done => {
                isDone(done);
            });
        }
    });
}

function sendAjaxCommand(type, id, value, isDone) {
    console.log(value, "VALUE")
    let devices = JSON.parse(localStorage.getItem("devices_" + JSON.parse(localStorage.getItem("selectedLocation")).locationId));
    devices.forEach(device => {
        if (device.id == id) {
            if (device.group) {
                let devicesId = device.group.split(',');
                    devicesId.forEach(element => {
                        sendAjaxCommandToST_new(type, element, value, done => {
                            isDone(done);
                        });
                    });           
            } else {
                    sendAjaxCommandToST_new(type, id, value, done => {
                        isDone(done);
                    });
            }
        }
    });
}

function sendAjaxCommandToST_new(type, id, value, isDone) {
    if (isCommanAccepted(type, id, value)) {
        const location = JSON.parse(localStorage.getItem("selectedLocation"));
        const server = location.locationServer;
        const location_id = location.locationIdentifier;
        const token = location.locationAccessToken;

        const paramType = "&type=" + type;
        const paramId = "&id=" + id.split("_")[1];
        let paramValue = "&value=";
        console.log(value);
        paramValue += (type == "colorLight" && value.split(",").length == 1 && (value != "coldColor" && value != "warmColor")) ? "intensidad," + value + "," + value : value;
        const paramDate = "&date=" + getNewDate();
        const url = "https://" + server + "/api/smartapps/installations/" + location_id + "/command/?access_token=" + token + paramType + paramId + paramValue + "&log=Ionic (0)" + paramDate + "&callback=?";
        console.log(url);
        // jQuery.getJSON(url)
        //     .done((data) => {
        //         console.log(`Comando de dispositivo <<${id}>> enviado correctamente`);
        //         isDone(true);
        //     })
        //     .fail((jqxhr, textStatus, error) => {
        //         isDone(false);
        //         console.log("Response: " + error + " Estado: " + textStatus);
        //     });

        let request = new XMLHttpRequest();

        request.onreadystatechange = function () {
            if (request.readyState == 4) {
                if (request.status == 200) {
                    console.log(`Comando de dispositivo <<${id}>> enviado correctamente`);
                    isDone(true);
                    // bio.innerHTML = request.responseText;
                } else {
                    isDone(true);
                    console.log("Response: " + request.status + " Estado: " + request.statusText);
                }
            }
        }

        request.open('Get', url);
        request.send();
    } else {
        isDone(true);
    }
}

function sendAjaxCommandToST(type, id, value, isDone) {
    if (isCommanAccepted(type, id, value)) {
        const location = JSON.parse(localStorage.getItem("selectedLocation"));
        const server = location.locationServer;
        const location_id = location.locationIdentifier;
        const token = location.locationAccessToken;

        const paramType = "&type=" + type;
        const paramId = "&id=" + id.split("_")[1];
        let paramValue = "&value=";
        paramValue += (type == "colorLight" && value.split(",").length == 1) ? "intensidad," + value + "," + value : value;
        const paramDate = "&date=" + getNewDate();
        const url = "https://" + server + "/api/smartapps/installations/" + location_id + "/command/?access_token=" + token + paramType + paramId + paramValue + "&log=Ionic (0)" + paramDate + "&callback=?";
        console.log(url);
        jQuery.getJSON(url)
            .done((data) => {
                console.log(`Comando de dispositivo <<${id}>> enviado correctamente`);
                isDone(true);
            })
            .fail((jqxhr, textStatus, error) => {
                isDone(false);
                console.log("Response: " + error + " Estado: " + textStatus);
            });
    } else {
        isDone(true);
    }
}

function isCommanAccepted(type, id, value) {
    if (type == "garage" || type == "lockDoor" || type == 'outerDoor') {
        return confirm("¿Seguro desea ejecutar esta acción?");
    }
    return true;
}

function getNewDate() {
    let date = new Date();
    return date.getTime();
}

function displayOptions(deviceType, devicesId) {
    isTilePressed = true;
    //alert("Opciones del dispositivo " + deviceType);
    options(deviceType, devicesId);
}

function options(deviceType, devicesId) {
    var tilePressed = setTimeout(function () {
        alert("Opciones del dispositivo " + deviceType);
        isTilePressed = false;
    }, 2000);
}

 