function Reloj() {
    this.canvas = document.getElementsByTagName('canvas')[0];
    this.canvas.width = 100;
    this.canvas.height = 100;
    this.ctx = this.canvas.getContext('2d');

    this.setHora();
    var self = this;
    this.intervalo = setInterval(function () { self.actualizar(); }, 1000);
}

Reloj.prototype.setHora = function () {
    var hora = new Date();
    this.segundos = hora.getSeconds();
    this.minutos = hora.getMinutes();
    this.horas = (hora.getHours() > 12 ? hora.getHours() - 12 : (hora.getHours() == 0) ? 12 : hora.getHours());

    this.rad12 = 2 * Math.PI / 12;
    this.rad60 = 2 * Math.PI / 60;
}

Reloj.prototype.incrementar = function () {
    this.segundos = (++this.segundos) % 60;
    if (this.segundos == 0) {
        this.minutos = (++this.minutos) % 60;
        if (this.minutos == 0) {
            this.horas = (++this.horas) % 12;
        }
    }
}

Reloj.prototype.dibujar = function () {
    this.ctx.clearRect(0, 0, 100, 100);
    this.ctx.lineWidth = 2;
    this.ctx.strokeStyle = "black";
    this.ctx.beginPath();
    this.ctx.arc(50, 45, 35, 0, 2 * Math.PI, false);
    this.ctx.stroke();

    this.ctx.save();
    this.ctx.translate(50, 45);

    this.ctx.save();
    this.ctx.rotate(this.horas * this.rad12 + this.minutos / 60 * this.rad12);
    this.ctx.lineWidth = 4;
    this.ctx.strokeStyle = "blue";
    this.ctx.beginPath();
    this.ctx.moveTo(0, 5);
    this.ctx.lineTo(0, -20);
    this.ctx.stroke();
    this.ctx.restore();

    this.ctx.save();
    this.ctx.rotate(this.minutos * this.rad60 + this.segundos / 60 * this.rad60);
    this.ctx.lineWidth = 3;
    this.ctx.strokeStyle = "black";
    this.ctx.beginPath();
    this.ctx.moveTo(0, 5);
    this.ctx.lineTo(0, -25);
    this.ctx.stroke();
    this.ctx.restore();

    this.ctx.save();
    this.ctx.rotate(this.segundos * this.rad60);
    this.ctx.lineWidth = 1;
    this.ctx.strokeStyle = "red";
    this.ctx.beginPath();
    this.ctx.moveTo(0, 5);
    this.ctx.lineTo(0, -30);
    this.ctx.stroke();
    this.ctx.restore();

    this.ctx.restore();
}

Reloj.prototype.actualizar = function () {
    this.incrementar();
    this.dibujar();
}

/*
var espera = 7000;
setTimeout(function () {
    if (document.getElementsByTagName("canvas")) {
        var reloj = new Reloj();
        espera = 0;
    }
}, espera);
*/