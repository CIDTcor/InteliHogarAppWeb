import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'hometabs', pathMatch: 'full' },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'configurations',
    loadChildren: () => import('./pages/configurations/configurations.module').then( m => m.ConfigurationsPageModule)
  },
  {
    path: 'help',
    loadChildren: () => import('./pages/help/help.module').then( m => m.HelpPageModule)
  },
  {
    path: 'locations',
    loadChildren: () => import('./pages/locations/locations.module').then( m => m.LocationsPageModule)
  },
  {
    path: 'loading',
    loadChildren: () => import('./pages/loading/loading.module').then( m => m.LoadingPageModule)
  },
  {
    path: 'terms-and-conds',
    loadChildren: () => import('./pages/terms-and-conds/terms-and-conds.module').then( m => m.TermsAndCondsPageModule)
  },
  {
    path: 'hometabs',
    loadChildren: () => import('./pages/hometabs/hometabs.module').then( m => m.HometabsPageModule)
  },
  {
    path: 'user-profile',
    loadChildren: () => import('./pages/tabsModals/user-profile/user-profile.module').then( m => m.UserProfilePageModule)
  },
  {
    path: 'change-device-room',
    loadChildren: () => import('./modals/change-device-room/change-device-room.module').then( m => m.ChangeDeviceRoomPageModule)
  },
  {
    path: 'color-light-window',
    loadChildren: () => import('./modals/color-light-window/color-light-window.module').then( m => m.ColorLightWindowPageModule)
  },
  {
    path: 'conditions-modal',
    loadChildren: () => import('./modals/conditions-modal/conditions-modal.module').then( m => m.ConditionsModalPageModule)
  },
  {
    path: 'graphic-config-modal',
    loadChildren: () => import('./modals/graphic-config-modal/graphic-config-modal.module').then( m => m.GraphicConfigModalPageModule)
  },
  {
    path: 'graphic-modal',
    loadChildren: () => import('./modals/graphic-modal/graphic-modal.module').then( m => m.GraphicModalPageModule)
  },
  {
    path: 'order-list-window',
    loadChildren: () => import('./modals/order-list-window/order-list-window.module').then( m => m.OrderListWindowPageModule)
  },
  {
    path: 'thermostat-window',
    loadChildren: () => import('./modals/thermostat-window/thermostat-window.module').then( m => m.ThermostatWindowPageModule)
  },
  {
    path: 'edit-location',
    loadChildren: () => import('./modals/edit-location/edit-location.module').then( m => m.EditLocationPageModule)
  },
  {
    path: 'locations-user-profile',
    loadChildren: () => import('./pages/tabsModals/locations-user-profile/locations-user-profile.module').then( m => m.LocationsUserProfilePageModule)
  },
  {
    path: 'hub-or-device-offline',
    loadChildren: () => import('./modals/hub-or-device-offline/hub-or-device-offline.module').then( m => m.HubOrDeviceOfflinePageModule)
  },
  {
    path: 'passwordless-auth',
    loadChildren: () => import('./modals/passwordless-auth/passwordless-auth.module').then( m => m.PasswordlessAuthPageModule)
  },
  {
    path: 'passwordless-auth',
    loadChildren: () => import('./modals/passwordless-auth/passwordless-auth.module').then( m => m.PasswordlessAuthPageModule)
  },




];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
