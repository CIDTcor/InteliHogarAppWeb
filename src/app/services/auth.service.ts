import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { Platform } from '@ionic/angular';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { auth } from 'firebase';
import { AlertController, ToastController } from '@ionic/angular';
import { File } from '@ionic-native/file/ngx';
import { SignInWithApple, AppleSignInResponse, AppleSignInErrorResponse, ASAuthorizationAppleIDRequest } from '@ionic-native/sign-in-with-apple/ngx';


@Injectable({
    providedIn: 'root'
})

export class AuthService {
    private auth: firebase.auth.Auth;
    constructor(
        private platform: Platform,
        private AFAuth: AngularFireAuth,
        private router: Router,
        private google: GooglePlus,
        private fb: Facebook,
        private alertCtrl: AlertController,
        private toastCtrl: ToastController,
        private file: File,
        private signInWithApple: SignInWithApple
    ) {
        this.auth = firebase.auth();
    }

    //#region nativeLogin
    loginWithFacebook() {
        return this.fb.login(['email', 'public_profile']).then((response: FacebookLoginResponse) => {
            const credential_fb = auth.FacebookAuthProvider.credential(response.authResponse.accessToken);
            return this.AFAuth.auth.signInWithCredential(credential_fb);
        });
    }

    loginWithGoogle() {
        return this.google.login({}).then(result => {
            const userData = result;
            return this.AFAuth.auth.signInWithCredential(auth.GoogleAuthProvider.credential(null, userData.accessToken));
        })
    }

    loginWithApple() {
        return this.signInWithApple.signin({
            requestedScopes: [
              ASAuthorizationAppleIDRequest.ASAuthorizationScopeFullName,
              ASAuthorizationAppleIDRequest.ASAuthorizationScopeEmail
            ]
          })
            .then((res: AppleSignInResponse) => {
              let provider = new firebase.auth.OAuthProvider('apple.com');
              const credential = provider.credential({
                  idToken: res.identityToken
              });

              return this.AFAuth.auth.signInWithCredential(credential);
            })
            .catch((error: AppleSignInErrorResponse) => {
              console.error(error);
            });
    }
    //#endregion nativeLogin

    //#region webLogin
    signInWithGoogleWeb() {
        let googleProvider = new firebase.auth.GoogleAuthProvider();
        googleProvider.setCustomParameters({
            prompt: 'select_account',
        });
        this.auth.signInWithRedirect(googleProvider)
            .then(() => {
                return this.auth.getRedirectResult();
            }).catch();
    }

    signInWithFacebookWeb() {
        var facebookProvider = new firebase.auth.FacebookAuthProvider();
        firebase.auth().signInWithRedirect(facebookProvider);
    }

    public signInWithEmail(email: string, password: string, sSave: boolean, callback) {
        this.auth.signInWithEmailAndPassword(email, password)
            .then(result => {
                if (sSave) {
                    localStorage.setItem("savedData", JSON.stringify({ userMail: email, userPass: password }));
                } else {
                    localStorage.removeItem("savedData");
                }
                callback(result, null);
            })
            .catch(error => {
                if (error) {
                    callback(null, error);
                }
            });
    }

    public createNewAccount(email: string, password: string, userName: string = null, callback) {
        this.auth.createUserWithEmailAndPassword(email, password)
            .then(user => {
                localStorage.setItem("tempName", userName);
                user.user.updateProfile({
                    displayName: userName,
                    photoURL: "../../assets/imgs/avatar_default.jpg"
                }).then(() => {
                    callback(user, null)
                });
            })
            .catch(error => {
                if (error) {
                    callback(null, error);
                }
            });
    }

    public updateUserProfile(newData: { displayName: string, photoURL: string }, callback): void {
        let user = this.auth.currentUser;
        user.updateProfile({
            displayName: (newData.displayName) ? newData.displayName : user.displayName,
            photoURL: (newData.photoURL) ? newData.photoURL : user.photoURL
        }).then(() => {
            callback(true, null);
        }).catch(error => {
            callback(null, error);
        });
    }

    public sendPasswordResetEmail(email: string, callback) {
        this.auth.sendPasswordResetEmail(email)
            .then(() => {
                callback(true, null);
            })
            .catch(error => {
                callback(null, error);
            });
    }
    //#endregion webLogin

    //#region logout
    logout() {
        let sessionUser = JSON.parse(localStorage.getItem("sessionUser"));
        let provider = sessionUser ? sessionUser.provider : "no provider";
        let userPlatforms = this.platform.platforms();
        if (provider == "facebook") {
            localStorage.clear();
            if (userPlatforms.includes("android") && !userPlatforms.includes("mobileweb")) {
                this.logoutWithFacebook().then(res => {
                    this.router.navigate(['login']);
                }).catch(err => {
                    console.log(err)
                });
            } else if (userPlatforms.includes("ios") && !userPlatforms.includes("mobileweb")) {
                this.logoutWithFacebook().then(res => {
                    this.router.navigate(['login']);
                }).catch(err => {
                    console.log(err)
                });
                //TODO: este tal vez se elimine pero hay que ver 
            } else {

            }
        }
        else if (provider == "password") {
            // localStorage.clear();

            if (userPlatforms.includes("android") && !userPlatforms.includes("mobileweb")) { 
                this.logOut(isDone => {
                    if (isDone) {
                        let savedData = JSON.parse(localStorage.getItem("savedData"));
                        localStorage.removeItem("sessionUser");
                        // localStorage.clear();
                        if (savedData) localStorage.setItem("savedData", JSON.stringify(savedData));
                        this.router.navigate(['login']);
                    }
                });
            } else if (userPlatforms.includes("ios") && !userPlatforms.includes("mobileweb")) { 
                this.logOut(isDone => {
                    if (isDone) {
                        let savedData = JSON.parse(localStorage.getItem("savedData"));
                        localStorage.removeItem("sessionUser");
                        // localStorage.clear();
                        if (savedData) localStorage.setItem("savedData", JSON.stringify(savedData));
                        this.router.navigate(['login']);
                    }
                });
            } else {
                this.logOut(isDone => {
                    if (isDone) {
                        let savedData = JSON.parse(localStorage.getItem("savedData"));
                        localStorage.removeItem("sessionUser");
                        // localStorage.clear();
                        if (savedData) localStorage.setItem("savedData", JSON.stringify(savedData));
                        this.router.navigate(['login']);
                    }
                });
            }
        } else {
            // localStorage.clear();

            if ((userPlatforms.includes("android") || userPlatforms.includes("ios")) && !userPlatforms.includes("mobileweb")) {
                this.logoutWithGoogle();
            } else {
                this.logOut(isDone => {
                    if (isDone) {
                        let savedData = JSON.parse(localStorage.getItem("savedData"));
                        localStorage.removeItem("sessionUser");
                        // localStorage.clear();
                        if (savedData) localStorage.setItem("savedData", JSON.stringify(savedData));
                        this.router.navigate(['login']);
                    }
                });
            }
        }

        localStorage.removeItem("statusTermsAndConds");
        // this.file.removeRecursively(this.file.cacheDirectory, 'tech.cidtcor.intelihogar').catch(err => console.log(err))
        // this.file.listDir(this.file.cacheDirectory,'org.chromium.android_webview').then((result)=>{

        //     console.log('files in org.chromium.android_webview directory: ', result);

        //     console.log('Started deleting files from cache folder!');

        //     for(let file of result){

        //         if(file.isFile == true){

        //             this.file.removeFile(this.file.cacheDirectory+'/org.chromium.android_webview/', file.name).then( data => {
        //                 console.log('file removed: ', this.file);
        //                 data.fileRemoved.getMetadata(function (metadata) {
        //                     let name = data.fileRemoved.name;
        //                     let size = metadata.size ;
        //                     let fullPath = data.fileRemoved.fullPath;
        //                     console.log('Deleted file: ', name, size, fullPath) ;
        //                     console.log('Name: ' + name + ' / Size: ' + size) ;
        //                 }) ;
        //             }).catch( error => {
        //                 file.getMetadata(function (metadata) {
        //                     let name = file.name ;
        //                     let size = metadata.size ;
        //                     console.log('Error deleting file from cache folder: ', error) ;
        //                     console.log('Name: ' + name + ' / Size: ' + size) ;
        //                 }) ;
        //             });

        //         }
        //     }
        // }) ;
    }


    //#region nativeLogout
    logoutWithFacebook() {
        return this.fb.logout().then((response: FacebookLoginResponse) => {
            localStorage.clear();
        });
    }

    logoutWithGoogle() {
        this.AFAuth.auth.signOut().then(res => {
            localStorage.clear();
            this.google.disconnect();
            this.router.navigate(['login']);
        })
    }
    //#endregion nativeLogout

    //#region logoutWeb
    public logOut(isDone) {
        this.auth.signOut().then(() => {
            isDone(true);
            console.log("logout succeeded");
        }).catch((error) => {
            isDone(false);
            console.log(error);
        });
    }


    public initOnGetRedirectResult(user) {
        this.auth.getRedirectResult().then(result => {
            if (result) {
                user(result.additionalUserInfo.profile, result.credential);
            }
        }).catch(function (error) {
            // Handle Errors here.
        });
    }


    public updateAccount(data, callback) {
        let currentUser = this.auth.currentUser;
        currentUser.updateProfile({
            displayName: (data.displayName) ? data.displayName : currentUser.displayName,
            photoURL: (data.photoURL) ? data.photoURL : currentUser.photoURL,
        })
            .then(result => {
                callback(result, null);
            })
            .catch(error => {
                callback(null, error);
            });
    }

    //#endregion logoutWeb

    //#endregion logout

    //#region LinkAccounts
    linkAccounts(error) {
        if (error.code == "auth/account-exists-with-different-credential") {
            this.AFAuth.auth.fetchSignInMethodsForEmail(error.email).then(providers => {
                let [provider] = providers;
                if (provider == "google.com") {
                    this.showLinkingAccountAlert(error);
                }
            });
        } else {
            console.log(error)
        }
    }

    async showLinkingAccountAlert(error) {
        let prompt = await this.alertCtrl.create(
            {
                header: 'Aviso',
                subHeader: 'Hemos detectado que has usado esta misma dirección de correo electrónico con otra cuenta, te redireccionaremos a iniciar sesión para sincronizar ambas cuentas (Nota: esto solo te lo pediremos una única vez)',
                buttons: [
                    'Cerrar sesión',
                    {
                        text: 'Continuar',
                        handler: data => {
                            this.loginWithGoogle().then(() => {
                                this.AFAuth.auth.currentUser.linkWithCredential(error.credential).then(result => {
                                    this.showToast('Sincronización exitosa');
                                }, error => {
                                    this.showToast('Hubo un error, intenta de nuevo');
                                    console.error(error);
                                });
                            })
                        }
                    }
                ]
            }
        );
        await prompt.present();
    }
    //#endregion LinkAccounts

    //#region commons
    getActualAuthStatus(callback) {
        this.AFAuth.authState.subscribe(res => {
            callback(res);
        });
    }
    async showToast(msg) {
        let toast = await this.toastCtrl.create({
            message: msg,
            duration: 3000
        });
        await toast.present();
    }

    //#endregion commons
}