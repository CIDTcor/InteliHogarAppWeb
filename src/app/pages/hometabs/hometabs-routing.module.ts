import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HometabsPage } from './hometabs.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'favorites'
  },
  {
    path: '',
    component: HometabsPage,
    children: [
      {
        path: 'confort',
        loadChildren: () => import('../tabs/confort/confort.module').then( m => m.ConfortPageModule)
      },
      {
        path: 'energy-saving',
        loadChildren: () => import('../tabs/energy-saving/energy-saving.module').then( m => m.EnergySavingPageModule)
      },
      {
        path: 'security',
        loadChildren: () => import('../tabs/security/security.module').then( m => m.SecurityPageModule)
      },
      {
        path: 'user-profile',
        loadChildren: () => import('../tabsModals/user-profile/user-profile.module').then( m => m.UserProfilePageModule)
      },
      {
        path: 'favorites',
        loadChildren: () => import('../tabs/favorites/favorites.module').then( m => m.FavoritesPageModule)
      },
      {
        path: 'create-edit-room',
        loadChildren: () => import('../tabsModals/create-edit-room/create-edit-room.module').then( m => m.CreateEditRoomPageModule)
      },
      {
        path: 'add-devices-and-favorites',
        loadChildren: () => import('../tabsModals/add-devices-and-favorites/add-devices-and-favorites.module').then( m => m.AddDevicesAndFavoritesPageModule)
      },
      {
        path: 'reorder-rooms',
        loadChildren: () => import('../tabsModals/reorder-rooms/reorder-rooms.module').then( m => m.ReorderRoomsPageModule)
      },
    
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HometabsPageRoutingModule {}
