import { Component, OnInit, NgZone } from '@angular/core';
import { InteliTab, InteliUser } from '../../../assets/Util/InteliInterfaces';
import { AlertController, NavController, MenuController, ModalController } from '@ionic/angular';
import { Network } from '@ionic-native/network/ngx';
import { Events } from '../../../assets/Util/events';
import { strings } from "../../../assets/Util/constants";
import * as deviceUtils from '../../../assets/Util/deviceUtil';
import * as roomUtils from '../../../assets/Util/roomUtils';
import { dataBase } from '../../../assets/Util/FirebaseCommunication';
import { smartThings } from '../../../assets/Util/SmartThingsCommunication';
import { Device } from '@ionic-native/device/ngx';
import { Router, ActivatedRoute } from '@angular/router';

import { HubOrDeviceOfflinePage } from '../../modals/hub-or-device-offline/hub-or-device-offline.page';

declare function updateHubState(hub);
declare function setHeaderColorTitle(backgroundColor, textTitle, colorTitle);
declare function sendCommand(type, id, value, isDone);
declare var isConnected;

@Component({
  selector: 'app-hometabs',
  templateUrl: './hometabs.page.html',
  styleUrls: ['./hometabs.page.scss'],
})
export class HometabsPage implements OnInit {

  public tabs: Array<InteliTab>;
  public selectedTab: number;
  private offlineModal;

  public user: InteliUser;
  public selectedLocation;
  private hub: any;
  public isKeyboardShowing = false;

  private inactiveAlertFlag: number = 0;
  private alarmInterval;

  public alertActive = false;
  private active = true;
  public appFullyLoaded = false;

  private connectSubscription;
  private disconnectSubscription;

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    private network: Network,
    private device: Device,
    private zone: NgZone,
    public events: Events,
    private menuCtrl: MenuController,
    private modalCtrl: ModalController,
    private router: Router,
    private activatedRoute: ActivatedRoute) {
    this.subscribeToLocationNameChange();
    this.subscribeToSelectedLocationUpdatedEvent();
    this.subscribeToAppFullyLoaded();
    this.subscribeToAccessLevelChanged();
    this.subscribeToKeyboardEvent();
  }
  

  ngOnInit() {
  

    // this.tabsSetup();
    let userInfo = JSON.parse(localStorage.getItem('sessionUser'));
    let selectedLocation = JSON.parse(localStorage.getItem('selectedLocation'));
    if (userInfo) {
      this.saveUserPhoto(userInfo.userImage, selectedLocation.locationId, userInfo.userMail);
      if (this.router.url.includes('locationId')) {      //CHECAR SI VIENE DE URL
        this.activatedRoute.queryParams.subscribe(params => {      //HACER CONSULTA A FB PARA OBTENER LOCATION INFO DE ESA LOCALIZACION
          let locationId = params['locationId']; 
          dataBase.getLocationInfo(locationId, info => {
              localStorage.setItem('selectedLocation', JSON.stringify(info));
              
              this.initPage();
              this.loadLocationUsers();
              this.events.publish('reloadUsers', null);
              this.events.publish('accessLevelChanged', null);   
          });
        });
      }
     
      if (selectedLocation) {
          this.initPage();
          this.loadLocationUsers();
          this.events.publish('reloadUsers', null);
          this.events.publish('accessLevelChanged', null);
     
      } else {
        this.navCtrl.navigateRoot('locations');
      }
    } else {
      this.navCtrl.navigateRoot('login');
      //setear el objeto con el selected id en el local storage
    }

  }
  private saveUserPhoto (photoUrl, locationId, userMail){
     dataBase.setUserImageInFirebase(locationId, photoUrl, userMail);

  }

  private async loadLocationUsers() {
    this.events.subscribe('reloadUsers', () => {
      dataBase.getAllLocationUsers(this.selectedLocation.locationId, callback => {
        localStorage.setItem("currentLocationUsers", JSON.stringify(callback));
        this.events.publish('reloadUserProfileUsers', null);
      });
    });
    //TODO: probably add a conditional to read only once
  }

  private subscribeToLocationNameChange() {
    this.events.subscribe('locationNameChanged', () => {
      this.selectedLocation = JSON.parse(localStorage.getItem('selectedLocation'));
    });
  }

  private subscribeToAppFullyLoaded() {
    this.events.subscribe('appFullyLoaded', () => {
      this.appFullyLoaded = true;
    });
  }

  private subscribeToAccessLevelChanged() {
    this.events.subscribe('accessLevelChanged', () => {
      dataBase.getCurrentUserAccessLevel(this.user.userMail, this.selectedLocation.locationId, accessLevel => {
        this.selectedLocation.accessLevel = accessLevel;
        localStorage.setItem('selectedLocation', JSON.stringify(this.selectedLocation));
      });
    });
  }

  private subscribeToKeyboardEvent() {
    this.events.subscribe('keyboard:updated', ev => {
      this.isKeyboardShowing = ev;
    });
  }

  public initPage() {
    let session = JSON.parse(localStorage.getItem(strings.session.sessionUser));
    if (session && session.sInit == true) {
     // this.checkNetworkConnection();
      this.setUserData();
      this.setLocation();
      this.getUserFavorites();
      this.getUserNotifications();
      this.setPages(); 
      roomUtils.getRooms(this.selectedLocation.locationKey, this.selectedLocation.locationId);
      deviceUtils.getDevices(this.selectedLocation.locationKey, this.selectedLocation.locationId);
      this.saveTokenInFirebase();
      // this.initAlarmListener();
      this.subscribeToUpdateUserEvent();
    }
  }

  //#region SET DATA (Location && User)
  private setUserData() {
    this.user = JSON.parse(localStorage.getItem("sessionUser"));
  }

  private getUserFavorites() {
    let userMail = this.convertDotToDash(this.user.userMail);
    dataBase.getCurrentUserFavorites(this.selectedLocation.locationId, userMail, resp => {
      if (resp) {
        let userFavorites = resp;
        localStorage.setItem(`shortcuts_${this.selectedLocation.locationId}`, JSON.stringify(userFavorites));
        this.events.publish('favoritesUpdated', 'data');
      }
    });
  }

  private getUserNotifications() {
    let userMail = this.convertDotToDash(this.user.userMail);
    dataBase.getCurrentUserNotifications(this.selectedLocation.locationId, userMail, resp => {
      if (resp) {
        let userNotifications = resp;
        localStorage.setItem(`notifications_${this.selectedLocation.locationId}`, JSON.stringify(userNotifications));

      }
    });
  }

  private setLocation() {
    this.selectedLocation = JSON.parse(localStorage.getItem("selectedLocation"));
    this.getLocationStatus();
    this.onUserStatusInLocationChange();
  }
  //#endregion

  private saveTokenInFirebase() {
    let locationPlan = JSON.parse(localStorage.getItem("selectedLocation")).locationPlan;
    let myPlanNotifications = JSON.parse(localStorage.getItem("subscriptionPlan"))[locationPlan].includes.notifications;
    if (myPlanNotifications != 'ninguna') {
      let device = {
        platform: this.device.platform,
        version: this.device.version,
        serial: this.device.serial,
        model: this.device.model,
        uuid: this.device.uuid,
        manufacturer: this.device.manufacturer,
        timestamp: this.getTimestamp()
      };
      dataBase.checkIfUserTokenIsAlreadyRegistered(this.selectedLocation.locationKey, this.device.uuid, this.user.userMail, device, isDeviceDeleted => {
        // if (isDeviceDeleted) {
        //   dataBase.saveToken(this.user.userMail, this.selectedLocation.locationKey, device, isDone => {
        //     if (isDone) {
        //       console.log("TOKEN", "Token actualizado: Nuevo Generado");
        //       // this.deleteOldToken();
        //     }
        //   });
        // }
      });
    } else {
      this.presentAlert("Notificaciones", "El plan actual no cuenta con notificaciones. Si desea recibir notificaciones, actualice a un plan superior")
    }
  }

  private async presentAlert(title, msg) {
    let alert = await this.alertCtrl.create({
      header: title,
      subHeader: msg,
      buttons: ['Aceptar']
    });
    await alert.present();
  }

  private deleteOldToken() {
    let oldToken = localStorage.getItem("oldToken");
    if (oldToken) {
      dataBase.deleteOldToken(this.selectedLocation.locationId, oldToken, isDone => {
        if (isDone) {
          console.log("TOKEN", "Token actualizado: Viejo Eliminado " + oldToken);
        }
      });
    }
  }

  private initSubscriptionPlanListener(): void {
    let subscriptionPlans = localStorage.getItem("subscriptionPlans");
    if (!subscriptionPlans) {
      dataBase.initSubscriptionPlanListener(plans => {
        localStorage.setItem('subscriptionPlan', JSON.stringify(plans));
      });
    }
  }

  private subscribeToUpdateUserEvent(): void {
    this.events.subscribe('user:updated', user => {
      this.user.userImage = user.userImage;
      this.user.userName = user.userName;
      this.refreshUI();
    });
  }
  //#endregion

  //#region Tabs Controller
  private setPages() {
    this.onLocationPlanUpdated();
  }

  public onTabSelected(ev: any): void {
    if (ev.index === 2) {
      this.selectedTab = ev.index;
    } else {
      this.selectedTab = ev.index;
      //this.superTabs.clearBadge(this.tabs[ev.index].id);
    }
    this.refreshUI();
  }

  private tabsSetup(): void {
    //this.superTabs.config.dragThreshold = 20;
    //this.superTabs.config.allowElementScroll = true;
    //this.onScreenOrientationChange();
    let user = JSON.parse(localStorage.getItem("sessionUser"));
    // document.getElementsByClassName('tabbar')[0].insertAdjacentHTML("beforeend", `<img (click)="testASD()" style="
    //     background-repeat: no-repeat;
    //     background-position: 50%;
    //     height: 35px;
    //     width: 35px;
    //     border-radius: 50%;
    //     background-image: url('${user.userImage}');
    //     background-size:contain;
    //     top: 10px;
    //     position: relative;
    //     left: -20px;
    //     z-index: -1;
    //     margin-left: -45px;">`);
  }

  //#endregion

  ionViewDidEnter() {
    this.getHub();
  }

  //#region HUB
  private getHub() {
    smartThings.getDevicesStateFromST(this.selectedLocation.locationServer, this.selectedLocation.locationIdentifier, this.selectedLocation.locationAccessToken, data => {
      this.hub = data.hub[0];
      // updateHubState(this.hub);
      this.getHubStateFromFB(this.hub);
    });
  }

  private getHubStateFromFB(hub) {
    dataBase.getHubState(hub, this.selectedLocation.locationKey, async data => {
      // updateHubState(data);
      if (data.status == 'off') {
        this.launchOfflineModal('hub');
      } else {
        if (this.modalCtrl.getTop()) {
          let modal = await this.modalCtrl.getTop();
          if (modal) {
            this.modalCtrl.dismiss();
          }
        }
      }
    });
  }
  
  private async launchOfflineModal(from) {
    this.offlineModal = await this.modalCtrl.create({
      component: HubOrDeviceOfflinePage,
      componentProps: { from: from },
      backdropDismiss: false,
      showBackdrop: true,
      cssClass: 'editLocationModal'
    })
    await this.offlineModal.present();
    this.offlineModal.onDidDismiss().then(data => {

    });
  }
  //#endregion

  //#region SECURITY
  public initAlarmListener() {
    dataBase.initAlarmListener(this.selectedLocation.locationKey, alarmActive => {
      if (alarmActive) {
        this.showAlert(alarmActive);
      }
    });
  }

  private async showAlert(element) {
    this.active = true;
    if (element.type == "alarmaIntrusos") {
      let alert = await this.alertCtrl.create({
        header: element.name + " activada",
        subHeader: 'Se activará en',
        backdropDismiss: true,
        cssClass: "alarm-alert",
        buttons: [
          /* {
             text: '¡A la v**** la ayuda!',
             handler: () => {
               this.active = false;
               this.alertActive = false;
               this.callEmergencyServices();
             }
           },*/
          {
            text: 'Apagar',
            handler: () => {
              console.log("alarm deactivated successfully");
              this.alertActive = false;
              this.active = false;
              if (this.alarmInterval)
                clearInterval(this.alarmInterval);
              this.sendCommand(element.type, element.id, 'off');
              // this.showConfirmationAlert(element, 1);
            }
          }
        ]
      });
      if (!this.alertActive) {
        this.startInterval(element, alert);
        await alert.present();
        this.alertActive = true;
      }
    } else {
      /*if (this.selectedTab != 3) {
        this.superTabs.setBadge("securityTab", 0);
        this.superTabs.badgeColor = "danger";
        this.superTabs.increaseBadge("securityTab", 1);
      }*/
      this.showAlarmAlert(element);
    }
  }

  private async showAlarmAlert(alarm) {
    let alert = await this.alertCtrl.create({
      header: '¡Atención!',
      subHeader: `Se ha activado la ${alarm.name.toUpperCase()}\n ¿Todo bien?`,
      backdropDismiss: false,
      buttons: [
        {
          text: '¡No!',
          handler: () => {
            this.callEmergencyServices();
          }
        },
        {
          text: 'Si',
          handler: () => {
            this.sendCommand(alarm.type, alarm.id, 'toggle');
          }
        }
      ]
    });
    await alert.present();
  }

  private startInterval(element, alert) {
    let alarmDate = element.date / 1000;
    let currentDate = new Date().getTime() / 1000;
    let differenceInSeconds = (currentDate - alarmDate < 0) ? 0 : Math.round(currentDate - alarmDate);
    let timeout = 30 - differenceInSeconds;
    this.zone.run(() => {
      this.alarmInterval = setInterval(() => {
        if (this.active) {
          if (timeout <= 0) {
            alert.dismiss();
            clearInterval(this.alarmInterval);
            this.alertActive = false;
            this.showConfirmationAlert(element, 1);
          }
          alert.subHeader('Se activará en ' + timeout + ' s');
          timeout--;
        }
      }, 1000);
    });
  }

  private async showConfirmationAlert(element, conf) {
    let text = (conf == 1) ? "Se detectó la activación de una alarma, ¿todo bien?" : "¿Seguro que todo está bien?";
    let alert = await this.alertCtrl.create({
      header: "Confirmación de alarma",
      subHeader: text,
      backdropDismiss: false,
      cssClass: "alarm-alert",
      buttons: [
        {
          text: 'No',
          handler: () => {
            this.callEmergencyServices();
            this.active = false;
            this.alertActive = false;
          }
        },
        {
          text: 'Si',
          handler: () => {
            // if (conf == 1) {
            //   setTimeout(() => {
            //     this.showConfirmationAlert(element, 2);
            //   }, 1000);
            // } else {
            //   console.log("alarm deactivated successfully");
            // this.changeIntrudersAlarmMode(element.type, element.id);
            // }
            this.sendCommand(element.type, element.id, "off");
            this.alertActive = false;
          }
        }
      ]
    });
    if (!this.alertActive) {
      await alert.present();
      this.alertActive = true;
    }
  }

  private callEmergencyServices(): void {
    // this.callNumber.isCallSupported()
    // .then(res => {
    //   if (res) {
    // this.callNumber.callNumber("911", true)
    //   .then(res => console.log("Llamando a emergencias", res))
    //   .catch(res => console.log("Error llamando a emergencias", res));
    //   } else {
    //     console.log("Ya te cargó el payaso, tu dispositivo no cuenta con funciones de llamada");
    //   }
    // });

  }

  public sendCommand(type, id, value) {
    sendCommand(type, id, value, isDone => {
    });
  }
  //#endregion

  //#region Location Plan
  private onLocationPlanUpdated(): void {
    dataBase.initLocationPlanUpdatedListener(this.selectedLocation.locationId, locationPlan => {
      if (!this.router.url.includes('locationId')) { 
      if (this.selectedLocation.locationPlan != locationPlan) {
        this.selectedLocation.locationPlan = locationPlan;
        localStorage.setItem('selectedLocation', JSON.stringify(this.selectedLocation));
        //this.showChangedLocationPlanAlert();
        // dataBase.deleteAllUsersExceptSuperUser(this.selectedLocation.locationId);
        this.showReloadAlert("Cambio de plan", "Su plan de suscripción ha cambiado")
        
      }
    } else {   
      this.showReloadAlert("Cambio de localizacion", "Su localizacion ha cambiado")
     // location.href = location.pathname;
    }
    });
  }
  //#endregion

  //#region User|Location Status
  private onUserStatusInLocationChange(): void {
    dataBase.userStatusInLocation(this.selectedLocation.locationId, this.user.userMail, status => {
      if (status) {
        if (!status.isActive) {
          this.deleteLocalLocationInfo();
          this.showReloadAlert("Usuario inactivo", "Se ha inhabilitado el acceso a esta localización");
        } else {
          this.updateSelectedLocation(status);
        }
      } else {
        this.deleteLocalLocationInfo();
        this.showReloadAlert("Acceso restringido", "Se te ha quitado el acceso a esta localización");
      }
    });
  }

  public navigateToUserProfile() {
    this.navCtrl.navigateForward('user-profile');
  }

  private getLocationStatus(): void {
    dataBase.getLocationStatus(this.selectedLocation.locationId, status => {
      if (!status.isActive) {
        this.deleteLocalLocationInfo();
        this.showReloadAlert("Localización inactiva", "La localización ha sido desactivada");
        this.selectedLocation = status;
        setHeaderColorTitle('#f8f8f8', this.selectedLocation.locationName, '#424242');
        this.updateSelectedLocation(status);
      }
    });
  }

  private updateSelectedLocation(status: any): void {
    //this.selectedLocation.accessLevel = (status.accessLevel) ? status.accessLevel : this.selectedLocation.accessLevel;
    this.selectedLocation.owner = (status.owner != null || status.owner != undefined) ? status.owner : this.selectedLocation.owner;
    this.selectedLocation.isActive = (status.isActive != null || status.isActive != undefined) ? status.isActive : this.selectedLocation.isActive;
    localStorage.setItem('selectedLocation', JSON.stringify(this.selectedLocation));
    this.publishSelectedLocationUpdateEvent();
  }

  private subscribeToSelectedLocationUpdatedEvent(): void {
    this.events.subscribe('selectedLocation:updated', data => {
      this.selectedLocation = data;
    });
  }

  private publishSelectedLocationUpdateEvent(): void {
    this.events.publish('selectedLocation:updated', this.selectedLocation);
  }

  private deleteLocalLocationInfo(): void {
    localStorage.removeItem("selectedLocation");
    localStorage.removeItem("roomList_" + this.selectedLocation.locationId);
    localStorage.removeItem("devices_" + this.selectedLocation.locationId);
    localStorage.removeItem("shortcuts_" + this.selectedLocation.locationId);
    localStorage.removeItem('statusTermsAndConds');
  }

  private async showReloadAlert(title: string, msg: string) {
    let alert = await this.alertCtrl.create({
      header: title,
      subHeader: msg + ", la aplicación se recargará en unos segundos...",
      backdropDismiss: false
    });
    await alert.present();
    if (!this.router.url.includes('locationId')) {
      setTimeout(() => {
        location.reload();
      }, 3000)
    } else { location.href = location.pathname; }
  }
  //#endregion

  //Utils
  private refreshUI(): void {
    this.zone.run(() => {

    });
  }

  private getTimestamp(): string {
    return Date.now().toString();
  }

  public openOpen() {
    this.menuCtrl.toggle('OptionsMenu');
  }

  private convertDotToDash(input) {
    return input.replace(/\./g, "-");
  }
  
  public scrollEvent = (event): void => {
    const footer = document.querySelector('.indexFooterStyle');
    const homeTabsNavBar = document.querySelector('#homeTabsTabStyle');
    const favoritesNavBar = document.querySelector('#favoritesTabStyle');
    const confortNavBar = document.querySelector('#confortTabsNavBar')
    if (this.isElementInViewPort(footer,window.innerHeight)) {
      homeTabsNavBar.classList.add('dontDisplay');
      favoritesNavBar.classList.remove('dontDisplay');
      confortNavBar.classList.remove('dontDisplay');
    } else {
      homeTabsNavBar.classList.remove('dontDisplay');
      favoritesNavBar.classList.add('dontDisplay');
      confortNavBar.classList.add('dontDisplay');
    }

  };

  isElementInViewPort(element: Element,  viewPortHeight: number) {
    let rect = element.getBoundingClientRect(); 
    return rect.top >= 0  && (rect.top <= viewPortHeight);
 }
}

/*********************** Tareas de Ingeniería ***********************/

//FIXME: BUG: core.js:1449 ERROR Error: Uncaught (in promise): TypeError: Cannot read property '2' of null
//TODO: Borrar Golden Pass