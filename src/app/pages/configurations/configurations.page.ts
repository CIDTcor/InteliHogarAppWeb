import { Component, OnInit } from '@angular/core';
import { ToastController, NavController, ModalController, AlertController } from "@ionic/angular";
import { Events } from '../../../assets/Util/events';
import * as utils from '../../../assets/Util/deviceUtil';
import * as roomUtils from '../../../assets/Util/roomUtils';
import { getUserId } from '../../../assets/Util/locationUtil';
import { strings, alerts, roomTypes } from "../../../assets/Util/constants";
import { ChangeDeviceRoomPage } from '../../modals/change-device-room/change-device-room.page';
import { translateDeviceType } from '../../../assets/Util/deviceUtil';
import { dataBase } from '../../../assets/Util/FirebaseCommunication';
import { OrderListWindowPage } from '../../modals/order-list-window/order-list-window.page';

@Component({
  selector: 'app-configurations',
  templateUrl: './configurations.page.html',
  styleUrls: ['./configurations.page.scss'],
})
export class ConfigurationsPage implements OnInit {
  private deviceList;
  private selectedLocation;
  public showRoutOpt = false;
  public showSceneOpt = false;
  public showRoomOpt = false;

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController,
    public events: Events,
  ) {
    this.init();
  }

  ngOnInit() {
  }

  private init() {
    this.selectedLocation = JSON.parse(localStorage.getItem('selectedLocation'));
    this.deviceList = utils.getLocationDevices(this.selectedLocation.locationId);
  }

  //#region Input devices alert
  public async showInputDevicesAlert(type: string) {
    let alert: any;
    switch (type) {
      case strings.actions.shortcut:
        // alert = this.createInputDevicesAlert(
        //   this.fillInputDevicesList("shortcuts"),
        //   strings.messages.shortcutConfig,
        //   strings.actions.shortcut,
        //   ""
        // );
        this.showInputDevicesModal("shortcuts", "favorites");
        break;
      case strings.actions.notification:
        // alert = this.createInputDevicesAlert(
        //   this.fillInputDevicesList("notifications"),
        //   strings.messages.notificationConfig,
        //   strings.actions.notification,
        //   ""
        // ); 
        this.showInputDevicesModal("notifications", "notifications");
        break;
      case "hiddenDevices":
        // alert = this.showHiddenDeselectedRoomvices();
        // alert.present();
        this.showInputDevicesModal("hiddenDevices", "hiddenDevices");
        break;
      case "deleteRoom":
        alert = await this.createInputDevicesAlert(
           this.fillDeleteRoomsList(),
          'Seleccione las habitaciones a eliminar',
          'deleteRoom',
          ""
        );
        await alert.present();
        break;
    }
  }

  private async showInputDevicesModal(type, room) {
    let devices = this.fillInputDevicesList(type);
    let modal = await this.modalCtrl.create(
      {
        component: ChangeDeviceRoomPage,
        componentProps: {
          devices: devices,
          selectedRoom: room
        },
        backdropDismiss: true
      }
    )
    await modal.present();

    modal.onDidDismiss().then(data => {
        this.savePreferences(data.data, "notifications");
    });

  }

  private async createInputDevicesAlert(inputs: any, title: string, action: string, name: string) {
    let alert = await this.alertCtrl.create(
      {
        header: title,
        inputs: this.addPossibleInputsDeleteRooms(inputs),
        buttons: [
          strings.actions.cancel,
          {
            text: strings.actions.ok,
            handler: data => {
              switch (action) {
                case strings.actions.shortcut:
                  this.savePreferences(data, "shortcuts");
                  break;
                case strings.actions.notification:
                  this.savePreferences(data, "notifications");
                  break;
                case "deleteRoom":
                  this.deleteRooms(data);
                  break;
                case "routine":
                  this.saveRoutines(data, name);
                  break;
                case "routineList":
                  this.removeRoutines(data);
                  break;
              }
            }
          }
        ]
      }
    );
    
    return alert;
  }

  private addPossibleInputsDeleteRooms(receivedInputs) {
    let inputs = [];
    receivedInputs.forEach(input => {
      inputs.push({
        type: "checkbox",
        label: input.label + translateDeviceType(input.type),
        value: input.value,
        checked: input.checked
      });
    });
    return inputs;
  }

  private fillInputDevicesList(variable) {
    let widgets = {
      label: strings.widgets.widgets,
      value: strings.widgets.widgets,
      checked: true,
      id: strings.widgets.widgets,
      name: strings.widgets.widgets
    };
    let checkedList = JSON.parse(
      localStorage.getItem(variable + "_" + this.selectedLocation.locationId)
    );
    let temp = [];
    //temp.push(widgets);
    this.deviceList.sort(function (a, b) {
      return a.name.localeCompare(b.name);
    });
    let devicesDB = temp.concat(this.deviceList);
    let isArraySplited = false;
    do {
      isArraySplited = false;
      for (let i in devicesDB) {
        if (variable == "shortcuts" || variable == "hiddenDevices" || this.isNotification(variable, devicesDB[i]) || this.isSwitchable(variable, devicesDB[i]) || this.isRoutine(variable, devicesDB[i])) {
          devicesDB[i].label = devicesDB[i].name;
          devicesDB[i].checked = checkedList && checkedList.includes(devicesDB[i].id) ? true : false;
          devicesDB[i].value = devicesDB[i].id;
          //TODO: leer el estado "checked" actual de los dispositivos ocultos
        } else {
          devicesDB.splice(parseInt(i), 1);
          isArraySplited = true;
        }
      }
    } while (isArraySplited);
    this.sortDevices(devicesDB);
    return devicesDB;
  }

  private sortDevices(devicesDB) {
    devicesDB.sort((a, b) => {
      return a.label.localeCompare(b.label);
    });

    devicesDB.sort((a, b) => {
      return b.checked - a.checked;
    });
  }

  private isNotification(variable, device) {
    return variable == "notifications" &&
      device.label != "Widgets" &&
      device.type != "routine" &&
      device.type != "mode" /*&&
      device.type != "inteliAppsSecurity" &&
      device.type != "inteliAppsGeneral" &&
      device.type != "inteliAppsEnergy" &&
      device.type != "inteliAppsMedical"*/;
  }

  private isSwitchable(variable, device) {
    return variable == "routine" &&
      (device.type == "swtich" ||
        device.type == "dimmer" ||
        device.type == "curtain" ||
        device.type == "window" ||
        device.type == "adjustableLight" ||
        device.type == "panic" ||
        device.type == "switchLight" ||
        device.type == "lock" ||
        device.type == "garage" ||
        device.type == "valve" ||
        device.type == "inteliAppsSecurity" ||
        device.type == "inteliAppsGeneral" ||
        device.type == "inteliAppsEnergy" ||
        device.type == "inteliAppsMedical");
  }

  private isRoutine(variable, device) {
    return variable == "routineList" && device.type == "routine";
  }
  //#endregion

  private savePreferences(list: any, type) {
    if (type == "notifications") {
      let locationPlan = JSON.parse(localStorage.getItem("selectedLocation")).locationPlan;
      let myPlanNotifications = JSON.parse(localStorage.getItem("subscriptionPlan"))[locationPlan].includes.notifications;
      dataBase.getCurrentLocationUsers(null).then(userNumber => {
        if (myPlanNotifications == 'completas') {
          dataBase.savePreferences(getUserId(), this.selectedLocation.locationId, list, isDone => {
            if (isDone) {
              utils.addDevicesListToNotifications(this.selectedLocation.locationId, list);
              this.presentToast("Dispositivos actualizados correctamente");
            }
          });
        } else {
          this.presentAlert("Notificaciones", "El plan actual no cuenta con notificaciones personalizadas. Si desea recibir notificaciones actualice a un plan superior")
        }
      });
    } else if (type == "shortcuts") {
      utils.addDevicesListToShortcuts(this.selectedLocation.locationId, list);
      this.presentToast("Favoritos actualizados correctamente");
    }
  }

  private async presentAlert(title, msg) {
    let alert = await this.alertCtrl.create(
      {
        header: title,
        subHeader: msg,
        buttons: ['Aceptar']
      }
    );
    await alert.present();
  }

  //#region Scenes
  public createScene() {

  }
  //#endregion

  //#region Routines

  private saveRoutines(data, name) {
    let routine = {
      accessLevel: 3,
      alias: name,
      help: "",
      id: `routine_${name}`,
      name: name,
      roomId: 0,
      type: "routine",
      devices: data
    }
    dataBase.saveRoutines(this.selectedLocation.locationKey, routine, isDone => {

    });
  }

  private removeRoutines(data) {
    dataBase.deleteRoutines(this.selectedLocation.locationKey, data, isDone => {

    });
  }
  //#endregion

  //#region Habitaciones
  public async showRoomOrderWindow() {
    let window = await this.modalCtrl.create(
      {
        component: OrderListWindowPage,
        componentProps: {
          rooms: JSON.parse(localStorage.getItem("roomList_" + this.selectedLocation.locationId)),
          location: this.selectedLocation
        },
        cssClass: 'order-list',
        backdropDismiss: true
      }
    )
    window.onDidDismiss().then(rooms => {
      this.publishRoomUpdateEvent(rooms);
    });
    await window.present();
  }

  public async showRoomNamePrompt() {
    let promp = await this.alertCtrl.create(
      {
        header: alerts.newRoom.nameTitle,
        subHeader: alerts.newRoom.nameMessage,
        inputs: [
          {
            name: 'name',
            placeholder: 'Nombre'
          }
        ],
        buttons: [
          'Cancelar',
          {
            text: 'Continuar',
            handler: data => {
              this.showTypeRoomAlert(data);
            }
          }
        ]
      }
    );
    await promp.present();
  }

  public async showTypeRoomAlert(data) {
    let newRoom = { roomAlias: data.name, roomId: null, roomType: null, roomOrder: null };
    let alert = await this.alertCtrl.create(
      {
        header: alerts.newRoom.typeTitle,
        subHeader: alerts.newRoom.typeMessage,
        inputs: this.addPossibleInputsTypeRoom(),
        buttons: [
          'Cancelar',
          {
            text: 'OK',
            handler: data => {
              let rooms = JSON.parse(localStorage.getItem("roomList_" + this.selectedLocation.locationId));
              newRoom.roomType = data;
              newRoom.roomOrder = roomUtils.generateNewRoomOrder(rooms);
              newRoom.roomId = roomUtils.generateNewRoomId(rooms);
              this.addNewRoom(newRoom);
            }
          }
        ]
      }
    );
    await alert.present();
  }

  private addPossibleInputsTypeRoom() {
    let inputs = [];
    roomTypes.sort(function (a, b) {
      return a.label.localeCompare(b.label);
    })

    roomTypes.forEach(room => {
      inputs.push(room);
    });
    return inputs;
  }

  private addNewRoom(newRoom) {
    dataBase.addNewRoom(newRoom, this.selectedLocation.locationKey, isDone => {
      if (isDone) {
        this.presentToast(`La habitación ${newRoom.roomAlias} se ha agregado correctamente`);
        this.publishRoomUpdateEvent(JSON.parse(localStorage.getItem('roomList_' + this.selectedLocation.locationId)));
      }
    });
  }

  private fillDeleteRoomsList(): Array<any> {
    let rooms = JSON.parse(localStorage.getItem("roomList_" + this.selectedLocation.locationId));
    let returnRooms = [];

    for (let i in rooms) {
      let element = { value: null, label: null };
      element.value = rooms[i].roomId;
      element.label = rooms[i].roomAlias;
      returnRooms.push(element);
    }

    returnRooms.sort((a, b) => {
      return a.label.localeCompare(b.label);
    });

    return returnRooms;
  }

  private deleteRooms(roomsId) {
    dataBase.deleteRooms(this.selectedLocation.locationKey, roomsId, isDone => {
      if (isDone) {
        this.presentToast("Habitaciones elimindas con éxito");
      }
    });
  }
  //#endregion

  //#region Options
  public openRoutineOptions() {
    this.showRoutOpt = !this.showRoutOpt;
  }

  public openRoomOptions() {
    if (this.selectedLocation.owner) {
      this.showRoomOpt = !this.showRoomOpt;
    } else {
      this.presentToast("Para acceder a esta opción requiere ser dueño de la localización");
    }
  }

  public openSceneOptions() {
    this.showSceneOpt = !this.showSceneOpt;
  }
  //#endregion

  private async presentToast(msg) {
    let toast = await this.toastCtrl.create(
      {
        message: msg,
        duration: 3000,
        position: 'bottom'
      }
    );
    await toast.present();
  }


  //#region Events
  private publishRoomUpdateEvent(rooms) {
    this.events.publish('rooms:updated', rooms);
  }
  //#endregion
  //#region Hidden Devices
  private async showHiddenDevices() {
    let alert = await this.alertCtrl.create(
      {
        header: 'Dispositivos ocultos',
        buttons: [
          strings.actions.cancel,
          {
            text: strings.actions.ok,
            handler: data => {
              devices.forEach(device => {
                device.visible = true;
                dataBase.updateDeviceVisibility(this.selectedLocation.locationKey, device);
              });
              data.forEach(device => {
                device.visible = false;
                dataBase.updateDeviceVisibility(this.selectedLocation.locationKey, device);
              });
            }
          }
        ]
      }
    );
    let devices = JSON.parse(localStorage.getItem("devices_" + this.selectedLocation.locationId));
    devices = this.orderDeviceList(devices);
    for (let device of devices) {
      alert.inputs.push({
        type: 'checkbox',
        id: device.key,
        label: device.name,
        checked: !device.visible,
        value: device
      });
    }
    return alert;
  }

  private orderDeviceList(devices) {
    devices.sort((a, b) => {
      return a.alias.localeCompare(b.alias);
    });

    devices.sort((a, b) => {
      return a.visible - b.visible;
    });
    return devices;
  }
  //#endregion

  public publishSavedConfigEvent() {
    this.events.publish('config:saved', true);
  }

}
