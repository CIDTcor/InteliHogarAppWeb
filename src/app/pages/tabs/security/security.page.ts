import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { NavController, AlertController, ActionSheetController, Platform, ToastController, LoadingController, ModalController, IonContent } from "@ionic/angular";
import { Vibration } from '@ionic-native/vibration/ngx';
import { strings } from "../../../../assets/Util/constants";
import * as utils from '../../../../assets/Util/deviceUtil';
import { smartThings } from '../../../../assets/Util/SmartThingsCommunication';
import { dataBase } from '../../../../assets/Util/FirebaseCommunication';
import { EditLocationPage } from '../../../modals/edit-location/edit-location.page';
import { Events } from '../../../../assets/Util/events';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

declare function sendCommand(type, id, value, isDone);
declare function setHeaderTitle(roomAlias, room, roomType);
@Component({
  selector: 'app-security',
  templateUrl: './security.page.html',
  styleUrls: ['./security.page.scss'],
})
export class SecurityPage implements OnInit {

  @ViewChild(IonContent, { static: true }) content: IonContent;
  public devicesSecurityAction = [];
  public devicesSecurityState = [];
  public modes = [];
  public sampleAlarms = [];
  public devices;
  public selectedLocation;
  public isAlarmActive = false;
  public popUpActive = false;
  private loader;
  public logs = [];
  private active = true;
  public countdown;
  public deviceCountdown;
  public deviceStatus;
  public deviceId;

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public actionSheetCtrl: ActionSheetController,
    public platform: Platform,
    public toastCtrl: ToastController,
    private vibration: Vibration,
    public loadingCtrl: LoadingController,
    private events: Events,
    private zone: NgZone,
    private inAppBrowser: InAppBrowser
  ) {
    this.subscribeToSelectedLocationUpdatedEvent();
    this.setLocation();
    this.subscribeToAlarmStatusEvent();
  }

  ngOnInit() {
    this.events.publish('appFullyLoaded', '');

  }
  scrollToTop() {
    this.content.scrollToTop(400);
  }

  private setLocation() {
    this.selectedLocation = JSON.parse(localStorage.getItem(strings.session.selectedLocation));
  }

  ionViewDidEnter() {
   // window.history.replaceState('','',`/${this.selectedLocation.locationName}`); //bryan

    document.getElementById('labelSecurity').style.color = '#ff553f';
    this.initTabSecurity();
    //let tabButton = document.getElementsByClassName('tabStyle')[0] as HTMLElement;
    // tabButton.style.setProperty('--color-selected', 'red');
    setHeaderTitle(this.selectedLocation.locationName, "security", null);
    const el = document.querySelector('.addBackground') as HTMLElement;
    el.style.setProperty('background', '#F4F8F8');
    const element = document.querySelector('.backBackground') as HTMLElement;
    element.style.setProperty('background', '#f4f8f8');
    this.scrollToTop();
    const homeTabsNavBar = document.querySelector('#homeTabsTabStyle');
    const securityNavBar = document.querySelector('#securityTabsNavBar');
    homeTabsNavBar.classList.remove('dontDisplay');
    securityNavBar.classList.add('dontDisplay');

  }

  private initTabSecurity() {
    this.devicesSecurityAction = [];
    this.modes = [];
    this.getDevices();

  }

  private getDevices() {
    this.devices = JSON.parse(localStorage.getItem("devices_" + this.selectedLocation.locationId));
    utils.setSecurityDeviceTypeList(this.devicesSecurityAction, this.modes, this.devices);
    this.getDevicesStateFromST();
    this.getStatesFromFB();
    this.fillAlarmsSample();

  }

  private subscribeToSelectedLocationUpdatedEvent(): void {
    this.events.subscribe('selectedLocation:updated', data => {
      this.selectedLocation = data;
    });
  }

  private subscribeToAlarmStatusEvent(): void {
    this.events.destroy('alarmActivated');
    this.events.subscribe('alarmActivated', deviceId => {
      console.log('alarmActivated publish TRIGGERED');
      this.deviceCountdown = 35;
      this.deviceId = deviceId;

      dataBase.getDeviceStatus(this.selectedLocation.locationId, deviceId, status => {
        this.deviceStatus = status;
      });

      dataBase.getDeviceDate(this.selectedLocation.locationId, deviceId, date => {
        let oldDate = new Date(date);
        this.deviceCountdown = 34 - Math.round(Math.abs((new Date().getTime() - oldDate.getTime()) / 1000));
      });
      var _this = this;
      var _database = dataBase;
      if (this.deviceCountdown > 0) {
        console.log("if device countdown");
        var countdown = setInterval(function () {
          console.log(_this.getCountdown());
          _this.setCountdown();
          _database.getDeviceStatus(_this.selectedLocation.locationId, _this.deviceId, status => {
            if ((_this.getCountdown() <= 0) || (status != 'active')) {
              clearInterval(countdown);
            }
          });
        }, 1000);
      }
    });
  }

  private setCountdown() {
    this.zone.run(() => {
      this.deviceCountdown--;
    });
  }

  private getCountdown() {
    return this.deviceCountdown;
  }

  //#endregion

  //#region Sample Alarms
  private fillAlarmsSample() {
    let contains = false;
    if (this.modes.length == 0) {
      this.sampleAlarms = [];
      this.sampleAlarms.push(utils.sampleAlarms[0]);
    }
    utils.sampleAlarms.forEach(sample => {
      for (let i in this.devicesSecurityAction) {
        if (sample.type == this.devicesSecurityAction[i].type) {
          contains = true;
          break;
        } else {
          contains = false;
        }
      }
      if (!contains && sample.type != 'alarmaIntrusos' && !this.existInSample(sample)) {
        this.sampleAlarms.push(sample);
      }
    });
  }

  private existInSample(sample): boolean {
    let flag = false;
    this.sampleAlarms.forEach(item => {
      if (item.id == sample.id) {
        flag = true;
      }
    });
    return flag;
  }

  public async showSpam(deviceType) {
    let modal = await this.modalCtrl.create({
      component: EditLocationPage,
      componentProps: { from: 'generalModal', title: "Aún no tienes este servicio instalado, ¿Deseas agregarlo?" },
      backdropDismiss: true,
      showBackdrop: true,
      cssClass: 'editLocationModal'
    })
    await modal.present();
    modal.onDidDismiss().then(data => {
      if (data.data) {
        this.redirectUserToStore(deviceType);
      }
    });
  }

  private redirectUserToStore(deviceType) {
    let URL;
    switch (deviceType) {
      case "alarmaIntrusos":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Alarma-De-Intrusos`;
        break;
      case "alarmaAgua":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Alarma-Fuga-Agua`;
        break;
      case "alarmaGas":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Alarma-fuga-gas`;
        break;
      case "alarmaIncendio":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Alarma-De-Incendio`;
        break;
      case "simulacionPresencia":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Simulacion-De-Presencia`;
        break;
    }
    //window.open(URL, "_system");
    this.inAppBrowser.create(URL, '_system');
  }
  //#endregion 

  //#region Command
  public sendCommand(type, id, value) {
    this.showLoader();
    sendCommand(type, id, value, isDone => {
      if (isDone) {
        this.loader.dismiss();
      }
    });
  }

  public async changeIntrudersAlarmMode(type, id) {
    this.launchEditModal('changeAlarmMode', type, id);
    // let alert = await this.alertCtrl.create({
    //   header: "Seleccionar modo",
    //   inputs: [
    //     {
    //       type: "radio",
    //       label: "Completa",
    //       value: "completa"
    //     },
    //     {
    //       type: "radio",
    //       label: "Noche",
    //       value: "noche"
    //     },
    //     {
    //       type: "radio",
    //       label: "Desactivada",
    //       value: "off"
    //     }
    //   ],
    //   buttons: [
    //     {
    //       text: 'Cancelar',
    //     },
    //     {
    //       text: 'Aceptar',
    //       handler: (data) => {
    //         this.sendCommand(type, id, data);
    //       }
    //     }
    //   ]
    // });
    // await alert.present();
  }
  //#endregion

  //#region States
  private getDevicesStateFromST() {
    smartThings.getDevicesStateFromST(this.selectedLocation.locationServer, this.selectedLocation.locationIdentifier, this.selectedLocation.locationAccessToken, data => {
      let devices = utils.devicesToArray(data);
      utils.updateSecurityDevicesByType(
        this.devicesSecurityAction,
        this.devicesSecurityState,
        this.modes,
        devices,
        {
          page: "sec"
        });
    });
  }

  private getStatesFromFB() {
    dataBase.getDeviceState(this.selectedLocation.locationKey, data => {
      this.zone.run(() => {
        utils.updateSecurityDevicesByType(
          this.devicesSecurityAction,
          this.devicesSecurityState,
          this.modes,
          data,
          {
            page: "sec"
          });
        this.refreshAlarms(data);
        this.updateLogs(data);
      });
    });

  }

  private refreshAlarms(data) {
    this.isAlarmActive = false;
    // let background = document.getElementsByClassName("scroll-content")[0];
    data.forEach((device, index) => {
      if (device.type == 'alarmaIntrusos') {
        // var background;
        // background.style.boxShadow = "initial";
        if (device.status == 'active') {
          //  this.manageAlarm(index);

          this.isAlarmActive = true;
        }
      }
    });

    if (this.isAlarmActive) {

    }
  }

  private manageAlarm(index) {
    var modes = this.modes;
    //modes[index].countdown = 5;
    var countdown = setInterval(function () {

      modes[index].countdown--;
      if (modes[index].countdown <= 0) {
        clearInterval(countdown);
      }
    }, 1000);
  }
  //#endregion

  //#region Utils
  private async showLoader() {
    this.loader = await this.loadingCtrl.create({
      cssClass: 'comfort-loader'
    });
    await this.loader.present();
  }

  public async showToast(msg) {
    let toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    await toast.present();
  }
  //#endregion

  //#region Others
  private updateLogs(data) {
    let date = new Date(data[0].eventDate);
    let day = (date.getDate() < 10) ? "0" + date.getDate() : date.getDate();
    let month = (date.getMonth() + 1 < 10) ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
    let hour = (date.getHours() < 10) ? "0" + date.getHours() : date.getHours();
    let minute = (date.getMinutes() < 10) ? "0" + date.getMinutes() : date.getMinutes();
    let log = {
      date: day + "/" + month + "/" + date.getFullYear(),
      time: hour + ":" + minute,
      event: data[0].displayName + " activo",
      timestamp: data[0].eventDate
    };
    this.logs.push(log);
    this.orderLogs();
  }

  private orderLogs() {
    this.logs.sort((a, b) => {
      return b.timestamp - a.timestamp;
    });
  }

  public activateListMode() {
    document.getElementById("block_deviceUI").style.display = "none";
    document.getElementById("list_deviceUI").style.display = "block";
    document.getElementById("logDivision").style.display = "none";
  }

  public activateBlockMode() {
    document.getElementById("block_deviceUI").style.display = "block";
    document.getElementById("list_deviceUI").style.display = "none";
    document.getElementById("logDivision").style.display = "block";
  }

  public rulesConfig() {

  }

  private async launchEditModal(from, type = null, id = null) {
    let modal = await this.modalCtrl.create({
      component: EditLocationPage,
      componentProps: { from: from },
      backdropDismiss: true,
      showBackdrop: true,
      cssClass: 'editLocationModal'
    })
    await modal.present();
    modal.onDidDismiss().then(data => {
      if (type && id) {
        this.sendCommand(type, id, data.data);
      }
    });
  }

  public getDeviceStatus(deviceId) {
    return dataBase.getDeviceStatus(this.selectedLocation.locationId, deviceId, status => {
      return status;
    });
  };
  public scrollEvent = (event): void => {
    const footer = document.querySelector('#securityFooter');
    const homeTabsNavBar = document.querySelector('#homeTabsTabStyle');
    //const favoritesNavBar = document.querySelector('#favoritesTabStyle');
    const securityNavBar = document.querySelector('#securityTabsNavBar');
    if (this.platform.is('desktop')) {
      if (this.isElementInViewPort(footer, window.innerHeight)) {
        homeTabsNavBar.classList.add('dontDisplay');
        //favoritesNavBar.classList.remove('dontDisplay');
        securityNavBar.classList.remove('dontDisplay');
      } else {
        homeTabsNavBar.classList.remove('dontDisplay');
        //favoritesNavBar.classList.add('dontDisplay');
        securityNavBar.classList.add('dontDisplay');
      }
    }

  };

  isElementInViewPort(element: Element, viewPortHeight: number) {
    let rect = element.getBoundingClientRect();
    return rect.top >= 0 && (rect.top <= viewPortHeight);
  }
}




  //#endregion
