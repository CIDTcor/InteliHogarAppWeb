import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import * as utils from '../../../../assets/Util/deviceUtil';
import { Device } from '../../../../assets/Util/Device';
import { NavController, AlertController, ActionSheetController, Platform, ToastController, LoadingController, ModalController, IonRouterOutlet, IonContent } from "@ionic/angular";
import { Vibration } from '@ionic-native/vibration/ngx';
import { Events } from '../../../../assets/Util/events';
import { strings } from "../../../../assets/Util/constants";
import { smartThings } from '../../../../assets/Util/SmartThingsCommunication';
import { dataBase } from '../../../../assets/Util/FirebaseCommunication';
import { tuya } from '../../../../assets/Util/TuyaCommunication';

import { ColorLightWindowPage } from '../../../modals/color-light-window/color-light-window.page';
import { ThermostatWindowPage } from '../../../modals/thermostat-window/thermostat-window.page';
import { ChangeDeviceRoomPage } from '../../../modals/change-device-room/change-device-room.page';
import { getUserId } from '../../../../assets/Util/locationUtil';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';

declare function sendCommand(type, id, value, isDone);
declare function setHeaderTitle(roomAlias, room, roomType);

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.page.html',
  styleUrls: ['./favorites.page.scss'],
})

export class FavoritesPage implements OnInit {

  @ViewChild(IonContent, { static: true }) content: IonContent;
  public selectedLocation;
  public canItBeActivated: boolean = true;
  public longPressTime = utils.sheetActiveTime;
  private loader;
  private loaderLoading = false;
  private style = document.querySelector('[data="test"]');
  public deviceList = Device.getInstance();
  public favorites = false;
  public devicesFullyLoaded = false;
  public currentPlatforms;


  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public actionSheetCtrl: ActionSheetController,
    public platform: Platform,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private vibration: Vibration,
    public events: Events,
    private routerOutlet: IonRouterOutlet,
    private zone: NgZone) {
    this.setLocation();
    this.currentPlatforms = this.platform.platforms();
  }


  ngOnInit() {
    this.events.subscribe('favoritesUpdated', data => {
      this.initShortcutsTab();
      this.getFavorites();
    });
    this.events.publish('appFullyLoaded', '');
    

  }

  scrollToTop() {
    this.content.scrollToTop(400);
  }

  ionViewDidEnter() {

    //window.history.replaceState('','',`/${this.selectedLocation.locationName}`); //bryan
    document.getElementById('labelFavorites').style.color = '#3f75ff';

    this.devicesFullyLoaded = false;
    //this.showLoadingView("cargando...");
    this.initShortcutsTab();
 
    this.setBackgroundColor();
    // this.setAllThumbsColors();
    setHeaderTitle(this.selectedLocation.locationName, "favorites", null);
    this.scrollToTop();
    const homeTabsNavBar = document.querySelector('#homeTabsTabStyle');
    const favoritesNavBar = document.querySelector('#favoritesTabStyle');
    const tabsButtonFooter = document.querySelector('#tabsButtonFooter');
    const buttonFooter = document.querySelector('#buttonFooter');
    homeTabsNavBar.classList.remove('dontDisplay');
    favoritesNavBar.classList.add('dontDisplay');
    tabsButtonFooter.classList.remove('dontDisplay');
    buttonFooter.classList.add('dontDisplay');
  }




  private setBackgroundColor() {
    const el = document.querySelector('.addBackground') as HTMLElement;
    el.style.setProperty('background', 'linear-gradient(90deg, #3F75FF 0%, #2AD8DA 100%)');
    const element = document.querySelector('.backBackground') as HTMLElement;
    element.style.setProperty('background', '#f4f8f8');
    let tabButton = document.getElementsByClassName('tabStyle')[0] as HTMLElement;
    tabButton.style.setProperty('--color-selected', '#5C8FF0');
  }

  private setAllThumbsColors() {
    this.style.innerHTML = "";
    this.deviceList.devicesColorLight.forEach(device => {
      let deviceColor = (device.color) ? device.color : "#437afe";
      this.setColorData(deviceColor, device.id, device.type);
      // const el = document.querySelector(`#slider_${device.id}`) as HTMLElement;
      // el.style.background = deviceColor;
      // document.documentElement.style.setProperty("--slider-thumb-color", deviceColor);
      // el.classList.add('color1'); 
    });
    this.deviceList.devicesMultiWhiteLight.forEach(device => {
      let deviceColor = (device.color) ? device.color : "hsl(189, 100%, 96%)";
      this.setColorData(deviceColor, device.id, device.type);
    });
  }

  private setColorData(deviceColor, deviceId, deviceType) {
    this.style.innerHTML += `.${deviceType}_${deviceId}::-webkit-slider-thumb { background: ${deviceColor} !important } `;
  }

  public manageColdAndWarmColors(deviceType, deviceId, temperature) {
    // let coldColor = {
    //   // hue: 189,
    //   hue: 52.5,
    //   level: 96,
    //   saturation: 100
    // }
    // let warmColor = {
    //   // hue: 51,
    //   hue: 14.16,
    //   level: 93,
    //   saturation: 100
    // }
    let devicesArray = deviceType == "colorLight" ? this.deviceList.devicesColorLight : this.deviceList.devicesMultiWhiteLight;
    let deviceColor;
    this.sendCommand(deviceType, deviceId, temperature);
    devicesArray.forEach(device => {
      if (device.id == deviceId) {
        if (temperature == "coldColor") {
          deviceColor = "hsl(189, 100%, 96%)";
          // setTimeout(() => {
          //   dataBase.saveDeviceColor(this.selectedLocation.locationId, deviceId, coldColor);
          // }, 700);
        } else {
          deviceColor = "hsl(51, 100%, 93%)";
          //   setTimeout(() => {
          //     dataBase.saveDeviceColor(this.selectedLocation.locationId, deviceId, warmColor);
          // }, 700);
        }
      }
    });
    this.setColorData(deviceColor, deviceId, deviceType);
  }

  private setLocation() {
    this.selectedLocation = JSON.parse(localStorage.getItem(strings.session.selectedLocation));
    this.subscribeToSelectedLocationUpdatedEvent();
  }

  private getFavorites() {
    let userFavorites = JSON.parse(localStorage.getItem(`shortcuts_${this.selectedLocation.locationId}`));
    if (userFavorites && userFavorites.length > 0) {
      this.favorites = true;
    } else {
      this.favorites = false;
    }
    // this.events.publish('favoritesUpdated', 'data');
  }

  private subscribeToSelectedLocationUpdatedEvent(): void {
    this.events.subscribe('selectedLocation:updated', (data: any) => {
      this.selectedLocation = data;
      this.refreshUI();
    });
  }

  private initShortcutsTab() {
    this.setLocation();
    this.deviceList.modes = [];
    this.deviceList.devicesAction = [];
    this.deviceList.devicesState = [];
    this.deviceList.devicesColorLight = [];
    this.deviceList.devicesMultiWhiteLight = [];
    this.deviceList.devicesThermostat = [];
    this.getDevices();
    this.subscribeToNotifcationsEvent();
    setTimeout(() => {
      //this.dismissLoading();
      this.devicesFullyLoaded = true;
      this.getNotificationDevices();
      this.getFavorites();

    }, 1000);
  }

  private resetDevicesLists() {
    this.deviceList.modes = [];
    this.deviceList.devicesAction = [];
    this.deviceList.devicesState = [];
    this.deviceList.devicesColorLight = [];
    this.deviceList.devicesMultiWhiteLight = [];
    this.deviceList.devicesThermostat = [];
  }

  public showLoadingView(message: string) {
    this.loaderLoading = true;
    this.loadingCtrl.create({
      message,
      showBackdrop: true
    }).then(load => {
      this.loader = load;
      load.present().then(() => { this.loaderLoading = false; });
    });
  }

  public dismissLoading() {
    const interval = setInterval(() => {
      if (this.loader || !this.loaderLoading) {
        this.loader.dismiss().then(() => { this.loader = null; clearInterval(interval) });
      } else if (!this.loader && !this.loaderLoading) {
        clearInterval(interval);
      }
    }, 500);
  }

  //#region ----Devices
  private getDevices() {
    dataBase.getDevices(this.selectedLocation.locationId, allDevices => {
      this.resetDevicesLists();
      localStorage.setItem(`devices_${this.selectedLocation.locationId}`, JSON.stringify(allDevices));
      utils.setDeviceTypeList(this.deviceList.devicesAction, this.deviceList.devicesState, this.deviceList.devicesColorLight, this.deviceList.devicesMultiWhiteLight, this.deviceList.devicesThermostat, this.deviceList.modes, this.fillShortcutsList(allDevices));
      this.getDevicesStateFromST();
     
    });
  }

  private fillShortcutsList(devices) {
    let returnList = [];
    let shortcuts = JSON.parse(localStorage.getItem('shortcuts_' + this.selectedLocation.locationId));

    if (devices) {
      devices.forEach(device => {
        for (let i in shortcuts) {
          if (device.id == shortcuts[i]) {
            returnList.push(device);
          }
        }
      });
    } else {
      console.error("Me colgué y no sé porque, tengo un pequeño control que me AYUDA a seguir trabajando pero NO ES una SOLUCIÓN cuando tengas mas tiempo checame por favor, tqm");
      //TODO: BUG CHECAR
    }
    return returnList;
  }

  //Obtener estados
  private getDevicesStateFromST() {
    smartThings.getDevicesStateFromST(this.selectedLocation.locationServer, this.selectedLocation.locationIdentifier, this.selectedLocation.locationAccessToken, data => {
      let devices = utils.devicesToArray(data);
      utils.updateDevicesByType(
        this.deviceList.devicesAction,
        this.deviceList.devicesState,
        this.deviceList.devicesColorLight,
        this.deviceList.devicesMultiWhiteLight,
        this.deviceList.devicesThermostat,
        this.deviceList.modes,
        devices,
        {
          page: "sc",
          icon: null,
          image: null
        });
      this.getStatesFromFB();
    });
  }

  private getStatesFromFB() {
    dataBase.getDeviceState(this.selectedLocation.locationKey, data => {
      utils.updateDevicesByType(
        this.deviceList.devicesAction,
        this.deviceList.devicesState,
        this.deviceList.devicesColorLight,
        this.deviceList.devicesMultiWhiteLight,
        this.deviceList.devicesThermostat,
        this.deviceList.modes,
        data,
        {
          page: "sc",
          icon: null,
          image: null
        });
    });
    this.setAllThumbsColors();
  }

  //Comandos de dispositivos
  public async openWindowDevice(device) {
    let modal;
    let modalComponent = (device.type == 'colorLight') ? ColorLightWindowPage : ThermostatWindowPage;
    modal = await this.modalCtrl.create({
      component: modalComponent,
      componentProps: { data: device },
      cssClass: 'device-window',
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      animated: true
    })
    this.canItBeActivated = true;
    await modal.present();
    modal.onDidDismiss().then(data => {
      // this.setSliderThumbColor(device);
      this.setAllThumbsColors();
    });
  }

  // public setSliderThumbColor(device) { 
  //     const el = document.querySelector(`#slider_${device.id}`);
  //     document.documentElement.style.setProperty("--slider-thumb-color", device.color);
  //     el.classList.add('color1');
  // }

  public sendCommand(type, id, value) {
    let tile = document.getElementById('sc_' + id);
    if (type != "window" && type != "curtain" && type != "smartBlind" && type != "lockDoor") {
      tile.classList.add('disabledTile');
      console.log("disabled tile added", type, tile)
    }
    if (JSON.stringify(id).includes('_')) { //SMARTHINGS DEVICE
      sendCommand(type, id, value, isDone => {
        if (isDone) {
          tile.classList.remove('disabledTile');
        } else {
          tile.classList.remove('disabledTile');
          this.showToast('Problema de conexión, comando no enviado');
        }
      });
    } else { //TUYA DEVICE
      if (type == 'lockDoor') {
        
        this.deviceList.devicesAction.forEach(device => {
          if (!device.id.includes('_') && device.type == 'lockDoor') {
            let tuyaLockDoor = document.getElementById('sc_' + device.id);
            tuyaLockDoor.classList.add('disabledTile');
          }
        });
      }
      tuya.sendTuyaCommand(type, id, value).then(isDone => {
        if (isDone) {
          if (type == 'lockDoor') {
            this.unlockDoorsTiles();
          } else {
            tile.classList.remove('disabledTile');
          }
        } else {
          this.showToast('Problema de conexión, comando no enviado');
        }
      });
    }

    this.canItBeActivated = true;
  }

  public unlockDoorsTiles() {
    console.log("tiles unlocked")
    this.deviceList.devicesAction.forEach(device => {
     // if (device.id != id) {
        if (!device.id.includes('_') && device.type == 'lockDoor') {
          let tuyaLockDoor = document.getElementById('sc_' + device.id);
          tuyaLockDoor.classList.remove('disabledTile');
        }
     // }
    });
  }

  public checkIfUserHasAccess(device, action) {
    console.log(device, "action");
    console.log(action, "action");
    if (device.accessLevel <= this.selectedLocation.accessLevel && this.selectedLocation.accessLevel != 1) {
      this.showToast('No cuentas con permiso para realizar esta acción.');
    } else {
      switch (action) {
        case 'coldColor':
        case 'warmColor':
          this.manageColdAndWarmColors(device.type, device.id, action);
          break;
        case 'changeColor':
        case 'thermostat':
          this.openWindowDevice(device)
          break;
        case 'mode':
          this.sendCommand(device.type, device.id, device.name);
          break;
        case 'open':
        case 'pause':
        case 'close':
          this.sendCommand(device.type, device.id, action);
          break;
        case 'toggle':
          this.sendCommand(device.type, device.id, device.status);
          break;
        default:
          this.sendCommand(device.type, device.id, action);
          break;
      }
    }
  }

  //#endregion

  //#region ----Opciones de dispositivo
  public async presentActionSheet(e, device) {
    if (this.canItBeActivated) {
      this.vibration.vibrate(utils.sheetVibrateTime);
      const actionSheet = await this.actionSheetCtrl.create({
        header: 'Acciones de dispositivo',
        buttons: this.addPossibleButtons(device),
        backdropDismiss: true
      });
      await actionSheet.present();
    }
  }

  private addPossibleButtons(device) {

    let buttons = [{
      text: 'Remover de favoritos',
      icon: !this.platform.is('ios') ? 'star' : null,
      handler: () => {
        this.showConfirmAlert('shortcuts', 'remove', device);
        // this.removeDeviceFromShortcuts(device);
      }
    }];
    if (this.selectedLocation.owner) {
      let button = {
        text: `Nivel de Acceso: ${(device.accessLevel == 1) ? 'Dueño' : (device.accessLevel == 2) ? 'Usuario' : (device.accessLevel == 3) ? 'Menor' : (device.accessLevel == 4) ? 'Invitado' : 'CondoR'}`,
        icon: !this.platform.is('ios') ? 'ribbon' : null,
        handler: () => {
          this.showAccessLevelAlert(device);
        }
      };
      buttons.push(button);
    }

    if (!utils.isItInNotifications(device)) {
      let button = {
        text: 'Agregar a notificaciones',
        icon: !this.platform.is('ios') ? 'notifications' : null,
        handler: () => {
          this.showConfirmAlert('notifications', 'add', device);
          // this.addDeviceToNotifications(device);
        }
      };
      buttons.push(button);
    } else {
      let button = {
        text: 'Remover de notificaciones',
        icon: !this.platform.is('ios') ? 'notifications' : null,
        handler: () => {
          this.showConfirmAlert('notifications', 'remove', device);
          // this.removeDeviceFromNotifications(device);
        }
      };
      buttons.push(button);
    }

    // buttons.push({
    //   text: 'Mostrar ayuda',
    //   icon: !this.platform.is('ios') ? 'help-circle' : null,
    //   handler: () => {
    //     this.showDeviceHepl(device);
    //   }
    // });
    buttons.push({
      icon: !this.platform.is('ios') ? 'create' : null,
      text: 'Cambiar nombre',
      handler: () => {
        this.changeDeviceName(device);
      }
    });
    buttons.push({
      text: 'Cancelar',
      icon: !this.platform.is('ios') ? 'close' : null,
      handler: () => {
      }
    });


    return buttons;
  }

  private async showConfirmAlert(type, action, device) {
    let alert = await this.alertCtrl.create({
      header: type == 'notifications' ? 'Notificación' : 'Acceso directo',
      subHeader: action == 'add' ? `¿Esta seguro que desea agregar ${device.alias} a ${type == 'notifications' ? 'notificaciones' : 'accesos directos'}?` : `¿Esta seguro que desea remover ${device.alias} de ${type == 'notifications' ? 'notificaciones' : 'accesos directos'}?`,
      buttons: ['Cancelar',
        {
          text: 'Aceptar',
          handler: () => {
            switch (type) {
              case 'shortcuts':
                this.removeDeviceFromShortcuts(device);
                break;
              case 'notifications':
                if (action == 'add') {
                  this.addDeviceToNotifications(device);
                } else {
                  this.removeDeviceFromNotifications(device);
                }
                break;
            }
          }
        }]
    });
    await alert.present();
  }

  //Accesos directos
  private removeDeviceFromShortcuts(device) {
    utils.removeDeviceFromShortcuts(this.selectedLocation.locationId, device);
    dataBase.removeSingleDeviceFromFavorites(this.selectedLocation.locationId, getUserId(), device.id);
    this.events.publish('favoritesUpdated', "data");
    this.showToast(`Dispositivo ${device.name} eliminado de accesos directos.`);
    this.ionViewDidEnter();
  }

  //Nivel de acceso
  private async showAccessLevelAlert(device) {
    let alert = await this.alertCtrl.create({
      header: 'Nivel de Acceso',
      cssClass: 'deviceAccessLevelClass',
      subHeader: 'Selecciona el nuevo nivel de acceso',
      inputs: [
        {
          type: 'radio',
          label: 'Solo dueños',
          value: '1',
          checked: (device.accessLevel == 1) ? true : false
        },
        {
          type: 'radio',
          label: 'Usuarios y dueños',
          value: '2',
          checked: (device.accessLevel == 2) ? true : false
        },
        {
          type: 'radio',
          label: 'Menor, usuarios y dueños',
          value: '3',
          checked: (device.accessLevel == 3) ? true : false
        },
        {
          type: 'radio',
          label: 'Todos',
          value: '4',
          checked: (device.accessLevel == 4) ? true : false
        }
      ],
      buttons: ['Cancelar',
        {
          text: "Aceptar",
          handler: (data) => {
            device.accessLevel = data;
            this.changeAccessLevel(device);
          }
        }
      ]
    });
    await alert.present();
  }

  private changeAccessLevel(device): void {
    dataBase.updateDevice(this.selectedLocation.locationId, device);
  }

  //Notificaciones
  public addDeviceToNotifications(device) {
    utils.addDeviceToNotifications(this.selectedLocation.locationId, device, devices => {
      dataBase.savePreferences(getUserId(), this.selectedLocation.locationId, devices, isDone => {
        if (isDone) {
          this.showToast(`Dispositivo ${device.alias} agregado a notificaciones.`);
          this.events.publish('notificationsUpdated', 'data');
        }
      });
    });
  }

  private removeDeviceFromNotifications(device) {
    utils.removeDeviceFromNotifications(this.selectedLocation.locationId, device, devices => {
      dataBase.savePreferences(getUserId(), this.selectedLocation.locationId, devices, isDone => {
        if (isDone) {
          this.showToast(`Dispositivo ${device.alias} removido de notificaciones.`);
          this.events.publish('notificationsUpdated', 'data');
        }
      });
    });
  }

  private async subscribeToNotifcationsEvent() {
    this.events.subscribe('notificationsUpdated', data => {
      let shortcutsList = JSON.parse(localStorage.getItem(`notifications_${this.selectedLocation.locationId}`));
      // console.log(shortcutsList);
      this.deviceList.modes.forEach(device => {
        if (shortcutsList.includes(device.id)) {
          device.notification = true;
        } else {
          device.notification = false;
        }
      });
      this.deviceList.devicesAction.forEach(device => {
        if (shortcutsList.includes(device.id)) {
          device.notification = true;
        } else {
          device.notification = false;
        }
      });
      this.deviceList.devicesState.forEach(device => {
        if (shortcutsList.includes(device.id)) {
          device.notification = true;
        } else {
          device.notification = false;
        }
      });
      this.deviceList.devicesColorLight.forEach(device => {
        if (shortcutsList.includes(device.id)) {
          device.notification = true;
        } else {
          device.notification = false;
        }
      });
      this.deviceList.devicesMultiWhiteLight.forEach(device => {
        if (shortcutsList.includes(device.id)) {
          device.notification = true;
        } else {
          device.notification = false;
        }
      });
      this.deviceList.devicesThermostat.forEach(device => {
        if (shortcutsList.includes(device.id)) {
          device.notification = true;
        } else {
          device.notification = false;
        }
      });
    });
  }

  private getNotificationDevices() {
    this.events.publish('notificationsUpdated', 'data');
  }

  //Ayuda
  public async showDeviceHepl(device) {
    let help = (device.help != "") ? device.help : 'No hay ayuda para mostrar';
    let alert = await this.alertCtrl.create({
      header: 'Ayuda de dispositivo',
      subHeader: help,
      buttons: ['Aceptar']
    });
    await alert.present();
  }

  //Cambiar Nombre
  private async changeDeviceName(device) {
    let alert = await this.alertCtrl.create({
      header: 'Cambiar nombre',
      subHeader: 'Ingrese el nuevo nombre de su dispositivo',
      inputs: [
        {
          type: 'text',
          value: device.alias
        }
      ],
      buttons: ['Cancelar',
        {
          text: 'Guardar',
          handler: (data) => {
            device.alias = data[0];
            dataBase.updateDeviceAlias(this.selectedLocation.locationId, device);
          }
        }
      ]
    });
    await alert.present();
  }
  //#endregion

  public async showToast(msg) {
    let toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

  //#region -----Agregar dispositivo
  public async showInputDevicesAlert() {
    // let devices = this.fillDevicesList("shortcuts");
    // let modal = await this.modalCtrl.create({
    //   component: ChangeDeviceRoomPage,
    //   componentProps: { devices: devices },
    //   backdropDismiss: true
    // })
    // await modal.present();
    // modal.onDidDismiss().then(data => {
    //   this.ionViewDidEnter();
    // });
    this.navCtrl.navigateForward('hometabs/add-devices-and-favorites')
  }

  private fillDevicesList(type) {
    let deviceList = JSON.parse(localStorage.getItem('devices_' + this.selectedLocation.locationId));
    let widgets = {
      label: strings.widgets.widgets,
      value: strings.widgets.widgets,
      checked: true,
      id: strings.widgets.widgets,
      name: strings.widgets.widgets
    };
    let checkedList = JSON.parse(localStorage.getItem(type + "_" + this.selectedLocation.locationId));
    let temp = [];
    //temp.push(widgets);

    let devicesDB = temp.concat(deviceList);
    for (let i in devicesDB) {
      devicesDB[i].label = devicesDB[i].name;
      devicesDB[i].checked =
        checkedList && checkedList.includes(devicesDB[i].id) ? true : false;
      devicesDB[i].value = devicesDB[i].id;
    }
    devicesDB.sort((a, b) => {
      return a.label.localeCompare(b.label);
    });
    devicesDB.sort((a, b) => {
      return b.checked - a.checked;
    });
    return devicesDB;
  }

  private savePreferences(shortcuts: any, variable) {
    utils.addDevicesListToShortcuts(this.selectedLocation.locationId, shortcuts);
  }
  //#endregion

  private refreshUI(): void {
    this.zone.run(() => {

    });
  }

  public disabledActionSheet(e): void {
    this.canItBeActivated = false;
  }

  public proxy(event, color) {
    this.disabledActionSheet(event);
  }

  public scrollEvent = (event): void => {
    const footer = document.querySelector('#favoritesFooter');
    const homeTabsNavBar = document.querySelector('#homeTabsTabStyle');
    const favoritesNavBar = document.querySelector('#favoritesTabStyle');
    const tabsButtonFooter = document.querySelector('#tabsButtonFooter');
    const buttonFooter = document.querySelector('#buttonFooter');
    if (this.platform.is('desktop')) {
      if (this.isElementInViewPort(footer, window.innerHeight)) {
        homeTabsNavBar.classList.add('dontDisplay');
        favoritesNavBar.classList.remove('dontDisplay');
        tabsButtonFooter.classList.add('dontDisplay');
        buttonFooter.classList.remove('dontDisplay');
      } else {
        homeTabsNavBar.classList.remove('dontDisplay');
        favoritesNavBar.classList.add('dontDisplay');
        tabsButtonFooter.classList.remove('dontDisplay');
        buttonFooter.classList.add('dontDisplay');
      }
    }

  };

  isElementInViewPort(element: Element, viewPortHeight: number) {
    let rect = element.getBoundingClientRect();
    return rect.top >= 0 && (rect.top <= viewPortHeight);
  }
}
