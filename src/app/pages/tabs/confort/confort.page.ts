import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { NavController, AlertController, ActionSheetController, Platform, ToastController, LoadingController, ModalController, IonContent } from "@ionic/angular";
import { Vibration } from '@ionic-native/vibration/ngx';
import { Events } from '../../../../assets/Util/events';
import { Device } from '../../../../assets/Util/Device';
import { smartThings } from '../../../../assets/Util/SmartThingsCommunication';
import * as utils from '../../../../assets/Util/deviceUtil';
import * as roomUtils from '../../../../assets/Util/roomUtils';
import { dataBase } from '../../../../assets/Util/FirebaseCommunication';
import { ColorLightWindowPage } from '../../../modals/color-light-window/color-light-window.page';
import { ThermostatWindowPage } from '../../../modals/thermostat-window/thermostat-window.page';
import { ChangeDeviceRoomPage } from '../../../modals/change-device-room/change-device-room.page';
import { getUserId } from '../../../../assets/Util/locationUtil';
import { OrderListWindowPage } from '../../../modals/order-list-window/order-list-window.page';
import { roomTypes, strings, alerts } from '../../../../assets/Util/constants';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

import { HometabsPageModule } from '../../hometabs/hometabs.module';
import { HometabsPage } from '../../hometabs/hometabs.page';
import { ClassGetter } from '@angular/compiler/src/output/output_ast';


declare function sendCommand(type, id, value, isDone);
declare function setHeaderTitle(roomAlias, room, roomType);

@Component({
  selector: 'app-confort',
  templateUrl: './confort.page.html',
  styleUrls: ['./confort.page.scss'],
})
export class ConfortPage implements OnInit {
  @ViewChild(IonContent, { static: true }) content: IonContent;
  server = "graph.api.smartthings.com";
  location_id = "53342c97-fab7-4c7a-be5a-9a268db73cc2";
  token = "0003f00c-1591-45a5-a94e-73622f14770b";
  stateReference: any;
  public rooms;
  private noRoom = -120;
  public selectedRoom = this.noRoom;
  public selectedLocation;
  private canItBeActivated: boolean = true;
  private loader;
  public deviceList = Device.getInstance();
  public maxNumbOfInteliApps = 1;
  public isTileDisabled = false;
  private style = document.querySelector('[data="test"]');
  public currentPlatforms;
  private previousUrl;
  private currentUrl;
  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public actionSheetCtrl: ActionSheetController,
    public platform: Platform,
    public toastCtrl: ToastController,
    private vibration: Vibration,
    public loadingCtrl: LoadingController,
    public events: Events,
    private route: ActivatedRoute,
    private router: Router,
    private zone: NgZone,
    private hometabsPageModule: HometabsPageModule) {
    this.setLocation();
    this.currentPlatforms = this.platform.platforms();
    router.events.subscribe(event => {
      console.log(event);
      if (event instanceof NavigationEnd) {
        this.previousUrl = this.currentUrl;
        this.currentUrl = event.url;
      };
    });
  }

  ngOnInit() {
    
    this.events.publish('appFullyLoaded', '');
    this.subscribeToNavParams();
   
  }
  scrollToTop() {
    this.content.scrollToTop(400);
  }
  //#region Hooks
  ionViewDidLeave() {
    this.selectedRoom = this.noRoom;
    // const background = document.getElementsByClassName('backBackground')[0] as HTMLElement;
    // background.style.background = '#F4F8F8';
  }

  ionViewDidEnter() {

 
    //window.history.replaceState('','',`/${this.selectedLocation.locationName}`); //bryan
    document.getElementById('labelConfort').style.color = '#c23fff';
   

    this.initTabComfort();
    this.setBackgroundColor();
    setHeaderTitle(this.selectedLocation.locationName, "confort", null);
    this.scrollToTop();
    const homeTabsNavBar = document.querySelector('#homeTabsTabStyle');
    const confortNavBar = document.querySelector('#confortTabsNavBar');
    homeTabsNavBar.classList.remove('dontDisplay');
    confortNavBar.classList.add('dontDisplay');

  }

  //#endregion

  private subscribeToNavParams() {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.showAddedDevicesLoader();
        this.selectedRoom = this.router.getCurrentNavigation().extras.state.actualRoom;
        const background = document.getElementsByClassName('backBackground')[0] as HTMLElement;
        background.style.background = 'linear-gradient(90deg, #5C8FF0 0%, #C456E5 100%)';
        setTimeout(() => {
          this.events.publish('config:saved', null);
        }, 1000);
      }
    });
  }

  private async showAddedDevicesLoader() {
    let loader = await this.loadingCtrl.create({
      message: 'Cargando...',
      cssClass: 'comfort-loader'
    });
    await loader.present();
    setTimeout(async () => {
      await loader.dismiss();
    }, 1500);
  }

  private setBackgroundColor() {
    const el = document.querySelector('.addBackground') as HTMLElement;
    el.style.setProperty('background', 'linear-gradient(90deg, #5C8FF0 0%, #C456E5 100%)');
    if (this.selectedRoom == this.noRoom) {
      const background = document.getElementsByClassName('backBackground')[0] as HTMLElement;
      background.style.background = '#F4F8F8';
    }
    let tabButton = document.getElementsByClassName('tabStyle')[0] as HTMLElement;
    tabButton.style.setProperty('--color-selected', '#C456E5');
  }

  public manageColdAndWarmColors(deviceType, deviceId, temperature) {
    // let coldColor = {
    //   // hue: 189,
    //   hue: 52.5,
    //   level: 96,
    //   saturation: 100
    // }
    // let warmColor = {
    //   // hue: 51,
    //   hue: 14.16,
    //   level: 93,
    //   saturation: 100
    // }
    let devicesArray = deviceType == "colorLight" ? this.deviceList.devicesColorLight : this.deviceList.devicesMultiWhiteLight;
    let deviceColor;
    this.sendCommand(deviceType, deviceId, temperature);
    devicesArray.forEach(device => {
      if (device.id == deviceId) {
        if (temperature == "coldColor") {
          deviceColor = "hsl(189, 100%, 96%)";
          // setTimeout(() => {
          //   dataBase.saveDeviceColor(this.selectedLocation.locationId, deviceId, coldColor);
          // }, 700);
        } else {
          deviceColor = "hsl(51, 100%, 93%)";
          //   setTimeout(() => {
          //   dataBase.saveDeviceColor(this.selectedLocation.locationId, deviceId, warmColor);
          // }, 700);
        }
      }
    });
    this.setColorData(deviceColor, deviceId, deviceType);
  }
  private setColorData(deviceColor, deviceId, deviceType) {
    this.style.innerHTML += `.${deviceType}_${deviceId}::-webkit-slider-thumb { background: ${deviceColor} !important } `;
  }

  private setAllThumbsColors() {
    this.style.innerHTML = "";
    this.deviceList.devicesColorLight.forEach(device => {
      let deviceColor = (device.color) ? device.color : "#437afe";
      this.setColorData(deviceColor, device.id, device.type);
      // const el = document.querySelector(`#slider_${device.id}`) as HTMLElement;
      // el.style.background = deviceColor;
      // document.documentElement.style.setProperty("--slider-thumb-color", deviceColor);
      // el.classList.add('color1'); 
    });
    this.deviceList.devicesMultiWhiteLight.forEach(device => {
      let deviceColor = (device.color) ? device.color : "#437afe";
      this.setColorData(deviceColor, device.id, device.type);
    });
  }

  //#region Events
  private suscribeToRoomUpdateEvent() {
    this.events.subscribe('rooms:updated', rooms => {
      this.rooms = rooms;
    });

  }
  //#endregion

  //#region Init
  private setLocation() {
    this.selectedLocation = JSON.parse(localStorage.getItem(strings.session.selectedLocation));
    this.maxNumbOfInteliApps = JSON.parse(localStorage.getItem("subscriptionPlan"))[this.selectedLocation.locationPlan].includes.inteliApps;
    this.subscribeToSelectedLocationUpdatedEvent();
  }

  private subscribeToSelectedLocationUpdatedEvent(): void {
    this.events.subscribe('selectedLocation:updated', data => {
      this.selectedLocation = data;
      this.refreshUI();
    });
  }

  private subscribeToSavedConfigEvent() {
    this.events.subscribe('config:saved', data => {
      this.initTabComfort();
    });
  }

  private initTabComfort() {
    this.deviceList.modes = [];
    this.deviceList.devicesAction = [];
    this.deviceList.devicesState = [];
    this.deviceList.devicesColorLight = [];
    this.deviceList.devicesMultiWhiteLight = [];
    this.deviceList.devicesThermostat = [];
    this.setRooms();
    this.getDevices();
    this.subscribeToNotifcationsEvent();
    this.suscribeToRoomUpdateEvent();
    this.subscribeToSavedConfigEvent();
    this.getNotificationDevices();
    const content = document.getElementById('confortContent');
    const roomsContent = document.getElementById('roomsContent');
    if (this.previousUrl != '/hometabs/add-devices-and-favorites') {
      roomsContent.style.height = 'auto';
      roomsContent.style.minHeight = '80%';
      roomsContent.style.padding = '0';
      content.style.height = '0';
      content.style.minHeight = '0';
    }

  }
  //#endregion

  //#region Rooms
  private setRooms() {
    this.rooms = JSON.parse(localStorage.getItem("roomList_" + this.selectedLocation.locationId));
    this.refreshUI();
  }

  public selectRoom(room) {
   // console.log(room.roomId);
    const p1 = JSON.parse(localStorage.getItem("roomList_" + this.selectedLocation.locationId));
    //console.log(p1);
    const p2 = p1.filter(cuartoEspecifico => cuartoEspecifico.roomId == room.roomId);
      console.log(p2[0].roomAlias);
    
 


    this.selectedRoom = room.roomId;
    setHeaderTitle(room.roomAlias, "room", room.roomType);
    const background = document.getElementsByClassName('backBackground')[0] as HTMLElement;
    background.style.background = 'linear-gradient(90deg, #5C8FF0 0%, #C456E5 100%)';
    const roomsContent = document.getElementById('roomsContent');
    const content = document.getElementById('confortContent');
    roomsContent.style.height = '0';
    roomsContent.setAttribute('style', 'padding: 0 !important');
    roomsContent.style.minHeight = '0';
    content.style.minHeight = '80%';
    content.style.height = 'auto';
    content.style.paddingBottom = '95px';
    this.refreshUI();
  }

  public goBack() {
    this.selectedRoom = this.noRoom;
    setHeaderTitle(this.selectedLocation.locationName, "confort", null);
    const background = document.getElementsByClassName('backBackground')[0] as HTMLElement;
    background.style.background = '#F4F8F8';
    this.refreshUI();
    const roomsContent = document.getElementById('roomsContent');
    roomsContent.style.height = 'auto';
    roomsContent.style.minHeight = '80%';
    roomsContent.style.padding = '0';
    const content = document.getElementById('confortContent');
    content.style.height = '0';
    content.style.minHeight = '0';
    this.content.scrollToTop(400);
  }
  //#endregion

  //#region Devices
  private getDevices() {
    utils.getDevices(this.selectedLocation.locationId, this.selectedLocation.locationId)
    this.deviceList.allDevices = JSON.parse(localStorage.getItem("devices_" + this.selectedLocation.locationId));
    utils.setDeviceTypeList(this.deviceList.devicesAction, this.deviceList.devicesState, this.deviceList.devicesColorLight, this.deviceList.devicesMultiWhiteLight, this.deviceList.devicesThermostat, this.deviceList.modes, this.deviceList.allDevices);
    this.getDevicesStateFromST();
  }

  private async subscribeToNotifcationsEvent() {
    this.events.subscribe('notificationsUpdated', data => {
      let shortcutsList = JSON.parse(localStorage.getItem(`notifications_${this.selectedLocation.locationId}`));
      // console.log(shortcutsList);
      this.deviceList.modes.forEach(device => {
        if (shortcutsList.includes(device.id)) {
          device.notification = true;
        } else {
          device.notification = false;
        }
      });
      this.deviceList.devicesAction.forEach(device => {
        if (shortcutsList.includes(device.id)) {
          device.notification = true;
        } else {
          device.notification = false;
        }
      });
      this.deviceList.devicesState.forEach(device => {
        if (shortcutsList.includes(device.id)) {
          device.notification = true;
        } else {
          device.notification = false;
        }
      });
      this.deviceList.devicesColorLight.forEach(device => {
        if (shortcutsList.includes(device.id)) {
          device.notification = true;
        } else {
          device.notification = false;
        }
      });
      this.deviceList.devicesMultiWhiteLight.forEach(device => {
        if (shortcutsList.includes(device.id)) {
          device.notification = true;
        } else {
          device.notification = false;
        }
      });
      this.deviceList.devicesThermostat.forEach(device => {
        if (shortcutsList.includes(device.id)) {
          device.notification = true;
        } else {
          device.notification = false;
        }
      });
    });
  }

  private getNotificationDevices() {
    this.events.publish('notificationsUpdated', 'data');
  }

  //Obtener estados
  private getDevicesStateFromST() {
    smartThings.getDevicesStateFromST(this.selectedLocation.locationServer, this.selectedLocation.locationIdentifier, this.selectedLocation.locationAccessToken, data => {
      let devices = utils.devicesToArray(data);
      utils.updateDevicesByType(
        this.deviceList.devicesAction,
        this.deviceList.devicesState,
        this.deviceList.devicesColorLight,
        this.deviceList.devicesMultiWhiteLight,
        this.deviceList.devicesThermostat,
        this.deviceList.modes,
        devices,
        {
          page: "cft",
          icon: null,
          image: null
        });
      this.getStatesFromFB();
    });
  }

  private getStatesFromFB() {
    dataBase.getDeviceState(this.selectedLocation.locationKey, data => {
      utils.updateDevicesByType(
        this.deviceList.devicesAction,
        this.deviceList.devicesState,
        this.deviceList.devicesColorLight,
        this.deviceList.devicesMultiWhiteLight,
        this.deviceList.devicesThermostat,
        this.deviceList.modes,
        data,
        {
          page: 'cft',
          icon: null,
          image: null
        });
      // this.refreshUI();
    });
    this.setAllThumbsColors();
  }
  //#endregion

  //#region Comandos de dispositivos
  public async openWindowDevice(device) {
    let modal;
    modal = await this.modalCtrl.create({
      component: (device.type == 'colorLight') ? ColorLightWindowPage : ThermostatWindowPage,
      componentProps: { data: device },
      cssClass: 'device-window'
    });
    this.canItBeActivated = true;
    await modal.present();
    modal.onDidDismiss().then(data => {
      // this.setSliderThumbColor(device);
      this.setAllThumbsColors();
    });
  }

  public sendCommand(type, id, value) {
    let tile = document.getElementById('cft_' + id);
    if (type != "window" && type != "curtain" && type != "smartBlind") {
      tile.classList.add('disabledTile');
    }
    let img = tile.getElementsByClassName('inteliappImage')[0] as HTMLImageElement;
    if (img) {
      img.src = `${img.src.split('.png')[0]}-on.png`
    }
    sendCommand(type, id, value, isDone => {
      if (isDone) {
        tile.classList.remove('disabledTile');
        img.src = `${img.src.split('-on.png')[0]}.png`
      } else {
        tile.classList.remove('disabledTile');
        img.src = `${img.src.split('-on.png')[0]}.png`
        this.showToast('Problema de conexión, comando no enviado');
      }
    });
    this.canItBeActivated = true;
  }

  public checkIfUserHasAccess(device, action) {
    if (device.accessLevel <= this.selectedLocation.accessLevel && this.selectedLocation.accessLevel != 1) {
      this.showToast('No cuentas con permiso para realizar esta acción.');
    } else {
      switch (action) {
        case 'coldColor':
        case 'warmColor':
          this.manageColdAndWarmColors(device.type, device.id, action);
          break;
        case 'changeColor':
        case 'thermostat':
          this.openWindowDevice(device);
          break;
        case 'mode':
          this.sendCommand(device.type, device.id, device.name);
          break;
        case 'open':
        case 'pause':
        case 'close':
          this.sendCommand(device.type, device.id, action);
          break;
        default:
          this.sendCommand(device.type, device.id, action);
          break;
      }
    }
  }
  //#endregion

  //#region Opciones de dispositivo
  public async presentActionSheet(e, device) {
    if (this.canItBeActivated) {
      this.vibration.vibrate(utils.sheetVibrateTime);
      const actionSheet = await this.actionSheetCtrl.create({
        header: 'Acciones de dispositivo',
        buttons: this.addPosibleButtonsActionSheet(device),
        backdropDismiss: true
      });


      await actionSheet.present();
    }
  }

  private addPosibleButtonsActionSheet(device) {
    let buttons = [];
    //Favoritos
    if (!utils.isItInShortcuts(device)) {
      buttons.push({
        text: 'Agregar a favoritos',
        icon: !this.platform.is('ios') ? 'star' : null,
        handler: () => {
          this.showConfirmAlert('shortcuts', 'add', device);
          // this.addDeviceToShortcuts(device);
        }
      });
    } else {
      buttons.push({
        text: 'Remover de favoritos',
        icon: !this.platform.is('ios') ? 'star' : null,
        handler: () => {
          this.showConfirmAlert('shortcuts', 'remove', device);
          // this.removeDeviceFromShortcuts(device);
        }
      });
    }
    //Permisos
    if (this.selectedLocation.owner) {
      buttons.push({
        text: `Nivel de Acceso: ${(device.accessLevel == 1) ? 'Dueño' : (device.accessLevel == 2) ? 'Usuario' : (device.accessLevel == 3) ? 'Menor' : (device.accessLevel == 4) ? 'Invitado' : '¿?'}`,
        icon: !this.platform.is('ios') ? 'ribbon' : null,
        handler: () => {
          this.showAccessLevelAlert(device);
        }
      });
    }
    if (this.selectedLocation.owner) {
      buttons.push({
        text: `Cambiar de habitación`,
        icon: !this.platform.is('ios') ? 'albums' : null,
        handler: () => {
          this.showRoomAlert(device);
        }
      });
    }
    //Notificaciones
    if (!utils.isItInNotifications(device)) {
      buttons.push({
        text: 'Agregar a notificaciones',
        icon: !this.platform.is('ios') ? 'notifications' : null,
        handler: () => {
          this.showConfirmAlert('notifications', 'add', device);
          // this.addDeviceToNotifications(device);
        }
      });
    } else {
      buttons.push({
        text: 'Remover de notificaciones',
        icon: !this.platform.is('ios') ? 'notifications' : null,
        handler: () => {
          this.showConfirmAlert('notifications', 'remove', device);
          // this.removeDeviceFromNotifications(device);
        }
      });
    }

    //Ayuda
    // buttons.push({
    //   text: 'Mostrar ayuda',
    //   icon: !this.platform.is('ios') ? 'help-circle' : null,
    //   handler: () => {
    //     this.showDeviceHepl(device);
    //   }
    // });
    //Ocultar Dispositivo
    // if (this.selectedLocation.owner) {
    //   buttons.push({
    //     text: `Ocultar dispositivo`,
    //     icon: !this.platform.is('ios') ? 'eye-off' : null,
    //     handler: () => {
    //       this.showHideDeviceAlert(device);
    //     }
    //   });
    // }
    //Nombre
    buttons.push({
      icon: !this.platform.is('ios') ? 'create' : null,
      text: 'Cambiar nombre',
      handler: () => {
        this.changeDeviceName(device);
      }
    });
    //Cancelar
    buttons.push({
      text: 'Cancelar',
      icon: !this.platform.is('ios') ? 'close' : null,
      role: 'cancel',
      handler: () => {
      }
    });

    return buttons;
  }

  private async showConfirmAlert(type, action, device) {
    let alert = await this.alertCtrl.create({
      header: type == 'notifications' ? 'Notificación' : 'Acceso directo',
      cssClass: 'confortCssClass',
      subHeader: `¿Está seguro que desea ${action == 'add' ? 'agregar' : 'remover'} ${device.alias} ${action == 'add' ? 'a' : 'de'} ${type == 'notifications' ? 'notificaciones' : 'accesos directos'}?`,
      buttons: ['Cancelar',
        {
          text: 'Aceptar',
          handler: () => {
            switch (type) {
              case 'shortcuts':
                if (action == 'add') {
                  this.addDeviceToShortcuts(device);
                } else {
                  this.removeDeviceFromShortcuts(device);
                }
                break;
              case 'notifications':
                if (action == 'add') {
                  this.addDeviceToNotifications(device);
                } else {
                  this.removeDeviceFromNotifications(device);
                }
                break;
            }
          }
        }]
    });
    await alert.present();
  }

  //Accesos directos
  private addDeviceToShortcuts(device) {
    utils.addDeviceToShortcuts(this.selectedLocation.locationId, device);
    dataBase.addSingleDeviceToFavorites(this.selectedLocation.locationId, getUserId(), device.id);
    // this.events.publish('favoritesUpdated', "data");
    this.showToast(`Dispositivo ${device.alias} agregado a accesos directos.`);
  }

  private removeDeviceFromShortcuts(device) {
    utils.removeDeviceFromShortcuts(this.selectedLocation.locationId, device);
    dataBase.removeSingleDeviceFromFavorites(this.selectedLocation.locationId, getUserId(), device.id);
    // this.events.publish('favoritesUpdated', "data");
    this.showToast(`Dispositivo ${device.alias} eliminado de accesos directos.`);
  }

  //Notificaciones
  public addDeviceToNotifications(device) {
    utils.addDeviceToNotifications(this.selectedLocation.locationId, device, devices => {
      dataBase.savePreferences(getUserId(), this.selectedLocation.locationId, devices, isDone => {
        if (isDone) {
          this.showToast(`Dispositivo ${device.alias} agregado a notificaciones.`);
          this.events.publish('notificationsUpdated', 'data');
        }
      });
    });
  }

  private removeDeviceFromNotifications(device) {
    utils.removeDeviceFromNotifications(this.selectedLocation.locationId, device, devices => {
      dataBase.savePreferences(getUserId(), this.selectedLocation.locationId, devices, isDone => {
        if (isDone) {
          this.showToast(`Dispositivo ${device.alias} removido de notificaciones.`);
          this.events.publish('notificationsUpdated', 'data');
        }
      });
    });
  }

  //Access Level
  private async showAccessLevelAlert(device) {
    let alert = await this.alertCtrl.create({
      header: "Nivel de Acceso",
      cssClass: 'confortCssClass',
      subHeader: "Selecciona el nuevo nivel de acceso",
      inputs: [
        {
          type: 'radio',
          label: 'Solo dueños',
          value: '1',
          checked: (device.accessLevel == 1) ? true : false
        },
        {
          type: 'radio',
          label: 'Usuarios y dueños',
          value: '2',
          checked: (device.accessLevel == 2) ? true : false
        },
        {
          type: 'radio',
          label: 'Menor, usuarios y dueños',
          value: '3',
          checked: (device.accessLevel == 3) ? true : false
        },
        {
          type: 'radio',
          label: 'Todos',
          value: '4',
          checked: (device.accessLevel == 4) ? true : false
        }
      ],
      buttons: [{ text: "Cancelar", role: "cancel" },
      {
        text: "Aceptar",
        handler: (data) => {
          device.accessLevel = data;
          this.changeAccessLevel(device);
        }
      }]
    })
    await alert.present();
  }

  private changeAccessLevel(device): void {
    dataBase.updateDevice(this.selectedLocation.locationId, device);
  }

  //Cambiar Habitación 
  private async showRoomAlert(device) {
    let rooms = JSON.parse(localStorage.getItem("roomList_" + this.selectedLocation.locationId));
    let alert = await this.alertCtrl.create({
      header: 'Cambiar habitación',
      subHeader: 'Selecciona la nueva habitación',
      cssClass: 'confortCssClass',
      inputs: this.addPossibleInputsChangeRoom(rooms, device),
      buttons: ['Cancelar', {
        text: 'Aceptar',
        handler: (data) => {
          device.roomId = data;
          dataBase.updateDeviceRoomId(this.selectedLocation.locationKey, device);
        }
      }]
    });
    await alert.present();
  }

  private addPossibleInputsChangeRoom(rooms, device) {
    let inputs = [];
    for (let room of rooms) {
      let input = {
        type: 'radio',
        label: room.roomAlias,
        value: room.roomId,
        checked: (room.roomId == device.roomId) ? true : false
      }
      inputs.push(input);
    }
    return inputs;
  }

  //Ayuda
  public async showDeviceHepl(device) {
    let help = (device.help != "") ? device.help : 'No hay ayuda para mostrar';
    let alert = await this.alertCtrl.create({
      header: 'Ayuda de dispositivo',
      subHeader: help,
      buttons: ['Aceptar']
    });
    await alert.present();
  }

  //Cambiar Nombre
  private async changeDeviceName(device) {
    let alert = await this.alertCtrl.create({
      header: 'Cambiar nombre',
      subHeader: 'Ingrese el nuevo nombre de su dispositivo',
      cssClass: 'confortCssClass',
      inputs: [{
        type: 'text',
        value: device.alias
      }],
      buttons: ['Cancelar', {
        text: 'Guardar',
        handler: (data) => {
          device.alias = data[0];
          dataBase.updateDeviceAlias(this.selectedLocation.locationId, device);
        }
      }]
    });
    await alert.present();
  }
  //Esconder dispositivo
  private async showHideDeviceAlert(device) {
    let alert = await this.alertCtrl.create({
      header: 'Esconder dispositivo',
      subHeader: '¿Está seguro que desea ocultar este dispositivo?',
      cssClass: 'confortCssClass',
      buttons: ['Cancelar', {
        text: 'Aceptar',
        handler: () => {
          device.visible = false;
          dataBase.updateDeviceVisibility(this.selectedLocation.locationKey, device);
        }
      }]
    });
    await alert.present();
  }

  public async showToast(msg) {
    let toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    await toast.present();
  }

  public disabledActionSheet(e): void {
    this.canItBeActivated = false;
  }
  //#endregion

  //#region Opciones de habitación
  public async presentRoomsActionSheet(e, room) {
    this.vibration.vibrate(utils.sheetVibrateTime);
    let sheet = await this.actionSheetCtrl.create({
      header: 'Acciones de habitación',
      buttons: this.addPossibleRoomsActionSheet(room)
    });
    await sheet.present();
  }

  private addPossibleRoomsActionSheet(room) {
    let buttons = [];
    buttons.push({
      icon: !this.platform.is('ios') ? 'reorder-four-outline' : null,
      text: "Ordenar Habitaciones",
      handler: () => {
        this.showOrderWindow();
      }
    });
    buttons.push({
      icon: !this.platform.is('ios') ? 'create-outline' : null,
      text: 'Cambiar nombre',
      handler: () => {
        this.changeRoomName(room);
      }
    });
    if (this.selectedLocation.owner) {
      buttons.push({
        text: 'Eliminar habitación',
        icon: !this.platform.is('ios') ? 'trash-outline' : null,
        handler: () => {
          this.confirmDeleteRoomAlert(room);
        }
      });
    }
    buttons.push({
      text: 'Cancelar',
      icon: !this.platform.is('ios') ? 'close-outline' : null,
      role: 'cancel',
      handler: () => {
      }
    });
    return buttons;
  }

  //Ordenar
  private async showOrderWindow() {
    // let window = await this.modalCtrl.create({
    //   component: OrderListWindowPage,
    //   componentProps: { location: this.selectedLocation, rooms: this.rooms },
    //   cssClass: 'order-list',
    //   backdropDismiss: true
    // });

    // await window.present();
    // window.onDidDismiss().then(data => {
    //   this.rooms = data;
    // });7
    this.navCtrl.navigateForward('hometabs/reorder-rooms');
  }

  //Cambiar Nombre
  private async changeRoomName(room) {
    // let alert = await this.alertCtrl.create({
    //   header: 'Cambiar nombre',
    //   subHeader: 'Ingrese el nuevo nombre de su habitación',
    //   inputs: [
    //     {
    //       type: 'text',
    //       value: room.roomAlias
    //     }
    //   ],
    //   buttons: ['Cancelar',
    //     {
    //       text: 'Guardar',
    //       handler: (data) => {
    //         room.roomAlias = data[0];
    //         roomUtils.updateRoomsAliasDB(this.selectedLocation.locationId, room);
    //       }
    //     }
    //   ]
    // });
    // await alert.present();
    this.navCtrl.navigateForward('hometabs/create-edit-room', {
      state: {
        room: room
      }
    });
  }

  //Eliminar Habitación
  private deleteRoom(roomId) {
    dataBase.deleteRoom(roomId, this.selectedLocation.locationKey, isDone => {
      if (isDone) {
        this.showLoader();
        this.initTabComfort();
        // this.loader.dismiss();
      }
    })
  }

  public async confirmDeleteRoomAlert(room) {
    let alert = await this.alertCtrl.create({
      header: 'Atención',
      cssClass: 'confortCssClass',
      subHeader: 'Estás seguro que deseas eliminar la habitación ' + room.roomAlias,
      buttons: ['No',
        {
          text: 'Si',
          handler: () => {
            this.deleteRoom(room.roomId);
          }
        }
      ]
    });
    await alert.present();
  }
  //#endregion

  //#region Agregar Habitación
  public async showRoomPrompt() {
    // let prompt = await this.alertCtrl.create({
    //   header: alerts.newRoom.nameTitle,
    //   subHeader: alerts.newRoom.nameMessage,
    //   inputs: [
    //     {
    //       name: 'name',
    //       placeholder: 'Nombre'
    //     }
    //   ],
    //   buttons: ['Cancelar',
    //     {
    //       text: 'Continuar',
    //       handler: data => {
    //         this.showTypeRoomAlert(data);
    //       }
    //     }
    //   ]
    // });
    // await prompt.present();
    this.navCtrl.navigateForward('hometabs/create-edit-room');
  }

  public async showTypeRoomAlert(data) {
    let newRoom = { roomAlias: data.name, roomId: null, roomType: null, roomOrder: null };
    let alert = await this.alertCtrl.create({
      header: alerts.newRoom.typeTitle,
      subHeader: alerts.newRoom.typeMessage,
      inputs: this.addPossibleInputsTypeRoom(),
      buttons: ['Cancelar',
        {
          text: 'OK',
          handler: data => {
            newRoom.roomType = data;
            newRoom.roomOrder = roomUtils.generateNewRoomOrder(this.rooms);
            newRoom.roomId = roomUtils.generateNewRoomId(this.rooms);
            this.addNewRoom(newRoom);
          }
        }]
    });
    await alert.present();
  }

  private addPossibleInputsTypeRoom() {
    let inputs = [];
    roomTypes.sort((a, b) => {
      return a.label.localeCompare(b.label);
    });

    roomTypes.forEach(room => {
      inputs.push(room);
    });
    return inputs;
  }

  private addNewRoom(newRoom) {
    dataBase.addNewRoom(newRoom, this.selectedLocation.locationKey, isDone => {
      if (isDone) {
        this.showLoader();
        this.initTabComfort();
        // this.loader.dismiss();
      }
    })
  }
  //#endregion

  //#region Agregar dispositivo
  public async showAddDevicesAlert() {
    let devices = this.deviceList.allDevices;
    devices = this.orderDeviceList(devices);
    let modal = await this.modalCtrl.create({
      component: ChangeDeviceRoomPage,
      componentProps: { devices: devices, selectedRoom: this.selectedRoom },
      backdropDismiss: true
    });
    await modal.present();
  }

  private orderDeviceList(devices) {
    devices.forEach(device => {
      device.checked = (device.roomId == this.selectedRoom) ? true : false;
    });

    devices.sort((a, b) => {
      return a.alias.localeCompare(b.alias);
    });

    devices.sort((a, b) => {
      return b.checked - a.checked;
    });
    return devices;
  }

  public addDeviceToRoom() {
    this.navCtrl.navigateForward('hometabs/add-devices-and-favorites', {
      state: {
        actualRoom: this.selectedRoom
      }
    });
  }
  //#endregion

  //#region Generales
  private async showLoader() {
    this.loader = await this.loadingCtrl.create({
      cssClass: 'comfort-loader'
    });
    await this.loader.present();
    await this.loader.dismiss();
  }
  //#endregion

  private refreshUI(): void {
    this.zone.run(() => {

    });
  }

  public scrollEvent = (event): void => {
    const footer = document.querySelector('#confortFooter');
    const homeTabsNavBar = document.querySelector('#homeTabsTabStyle');
    const confortNavBar = document.querySelector('#confortTabsNavBar');
    const roomBackButton = document.querySelector('#roomBackButton');
    const addRoomButton = document.querySelector('#addRoomButton');
    const addRoomButtonFooter = document.querySelector('#addRoomButtonFooter');
    const roomBackButtonFooter = document.querySelector('#roomBackButtonFooter');
    const addDeviceButton = document.querySelector('#addDeviceButton');
    const addDeviceButtonFooter = document.querySelector('#addDeviceButtonFooter');
    //const content = document.querySelector('#content');
    console.log(this.platform, "platform");
    if (this.platform.is('desktop')) {
      if (this.isElementInViewPort(footer, window.innerHeight)) {
        console.log("confortNavBarMuestra")
        homeTabsNavBar.classList.add('dontDisplay');
        addRoomButton.classList.add('dontDisplay');
        confortNavBar.classList.remove('dontDisplay');
        roomBackButton.classList.add('dontDisplay');
       roomBackButtonFooter.classList.remove('dontDisplay');
        addDeviceButton.classList.add('dontDisplay');
        addDeviceButtonFooter.classList.remove('dontDisplay');
         addRoomButtonFooter.classList.remove('dontDisplay');
      } else {
        console.log("confortNavBar-no-Muestra")
        homeTabsNavBar.classList.remove('dontDisplay');
        addRoomButton.classList.remove('dontDisplay');
        confortNavBar.classList.add('dontDisplay');
        roomBackButton.classList.remove('dontDisplay');
        roomBackButtonFooter.classList.add('dontDisplay');
        addDeviceButton.classList.remove('dontDisplay');
        addDeviceButtonFooter.classList.add('dontDisplay');
        addRoomButtonFooter.classList.add('dontDisplay');
      }
    }
  };

  isElementInViewPort(element: Element, viewPortHeight: number) {
    let rect = element.getBoundingClientRect();
    return rect.top >= 0 && (rect.top <= viewPortHeight);
  }

  //TODO: Cambiar refrescamiento de vista con el método refreshUI en lugar de hacerlo a palo.

}
