import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConfortPage } from './confort.page';

describe('ConfortPage', () => {
  let component: ConfortPage;
  let fixture: ComponentFixture<ConfortPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfortPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConfortPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
