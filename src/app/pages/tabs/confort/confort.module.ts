import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { ConfortPageRoutingModule } from './confort-routing.module';

import { ConfortPage } from './confort.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConfortPageRoutingModule
  ],
  declarations: [ConfortPage]
})
export class ConfortPageModule {}
