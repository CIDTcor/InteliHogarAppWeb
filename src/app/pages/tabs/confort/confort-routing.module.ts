import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfortPage } from './confort.page';


const routes: Routes = [
  {
    path: '',
    component: ConfortPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfortPageRoutingModule {}
