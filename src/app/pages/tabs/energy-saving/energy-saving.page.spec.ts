import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EnergySavingPage } from './energy-saving.page';

describe('EnergySavingPage', () => {
  let component: EnergySavingPage;
  let fixture: ComponentFixture<EnergySavingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnergySavingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EnergySavingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
