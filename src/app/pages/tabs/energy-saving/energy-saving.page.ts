import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { NavController, ModalController, AlertController, IonContent, Platform } from "@ionic/angular";
import { smartThings } from '../../../../assets/Util/SmartThingsCommunication';
import { dataBase } from '../../../../assets/Util/FirebaseCommunication';
import * as deviceUtil from '../../../../assets/Util/deviceUtil';
import { GraphicModalPage } from './../../../modals/graphic-modal/graphic-modal.page';
import { EditLocationPage } from '../../../modals/edit-location/edit-location.page';
import { Events } from '../../../../assets/Util/events';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

declare function setHeaderTitle(roomAlias, room, roomType);

@Component({
  selector: 'app-energy-saving',
  templateUrl: './energy-saving.page.html',
  styleUrls: ['./energy-saving.page.scss'],
})
export class EnergySavingPage implements OnInit {
  @ViewChild(IonContent, { static: true }) content: IonContent;
  private selectedLocation: any;
  public energyMeterDevices: any[] = [];
  public energyGeneratorDevices: any[] = [];
  public energyDifferenceDevice = {};
  public batteries: any[] = [];
  public sampleEnergyDevices: any[] = [];

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    private zone: NgZone,
    public modalCtrl: ModalController,
    private events: Events,
    public alertCtrl: AlertController,
    private inAppBrowser: InAppBrowser
  ) {
    this.subscribeToSelectedLocationUpdatedEvent();
    this.init();
  }

  ngOnInit() {
    this.events.publish('appFullyLoaded', '');
  }
  scrollToTop() {
    this.content.scrollToTop(400);
  }

  ionViewDidEnter() {
   // window.history.replaceState('','',`/${this.selectedLocation.locationName}`); //bryan
    // let tabButton = document.getElementsByClassName('tabStyle')[0] as HTMLElement;
    // tabButton.style.setProperty('--color-selected', 'green');

    document.getElementById('labelEnergySaving').style.color = '#139730';

    setHeaderTitle(this.selectedLocation.locationName, "energy", null);
    const el = document.querySelector('.addBackground') as HTMLElement;
    el.style.setProperty('background', '#F4F8F8');
    const element = document.querySelector('.backBackground') as HTMLElement;
    element.style.setProperty('background', '#f4f8f8');
    this.scrollToTop();
    this.scrollEvent("event");
  }

  private init(): void {
    this.setLocation();
    this.setDevices();
    this.getDevicesStateFromST();
    this.getDevicesStateFromFB();
    this.getEnergyDifferenceDevice();
  }
  //#endregion

  //#region Setter
  private setLocation(): void {
    this.selectedLocation = JSON.parse(localStorage.getItem("selectedLocation"));
  }

  private setDevices(): void {
    let allDevices: any[] = JSON.parse(localStorage.getItem(`devices_${this.selectedLocation.locationId}`));
    deviceUtil.setEnergyDeviceTypeList(this.energyMeterDevices, this.energyGeneratorDevices, this.batteries, allDevices);
    this.fillAlarmsSample();
  }

  public async showNoEnergyDevicesAlert(deviceType) {
    let modal = await this.modalCtrl.create({
      component: EditLocationPage,
      componentProps: { from: 'generalModal', title: "Aún no tienes este servicio instalado, ¿Deseas agregarlo?" },
      backdropDismiss: true,
      showBackdrop: true,
      cssClass: 'editLocationModal'
    })
    await modal.present();
    modal.onDidDismiss().then(data => {
      if (data.data) {
        this.redirectUserToStore(deviceType);
      }
    });
  }

  private redirectUserToStore(deviceType) {
    let URL;
    switch (deviceType) {
      case "energy":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=-LxMEru0Op1XfplFSx7u`;
        break;
      case "generator":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=-LxMEru0Op1XfplFSx7u`;
        break;
    }
    //window.open(URL, "_system");
    this.inAppBrowser.create(URL, '_system');
  }

  private subscribeToSelectedLocationUpdatedEvent(): void {
    this.events.subscribe('selectedLocation:updated', data => {
      this.selectedLocation = data;
    });
  }
  //#endregion

  //#region Update
  private getDevicesStateFromST(): void {
    smartThings.getDevicesStateFromST(this.selectedLocation.locationServer, this.selectedLocation.locationIdentifier, this.selectedLocation.locationAccessToken, data => {
      let devices = deviceUtil.devicesToArray(data);
      deviceUtil.updateEnergyDevicesByType(this.energyMeterDevices, this.energyGeneratorDevices, this.energyDifferenceDevice, this.batteries, devices);
      this.refreshUI();
    });
  }

  private getDevicesStateFromFB(): void {
    dataBase.getDeviceState(this.selectedLocation.locationKey, devicesState => {
      deviceUtil.updateEnergyDevicesByType(this.energyMeterDevices, this.energyGeneratorDevices, this.energyDifferenceDevice, this.batteries, devicesState);
      this.refreshUI();
    });
  }
  //#endregion

  //#region sampleDevices
  private fillAlarmsSample() {
    let contains = false;
    if (this.energyMeterDevices.length == 0 && this.energyGeneratorDevices.length == 0 && this.batteries.length == 0) {
      this.sampleEnergyDevices = [];
      this.sampleEnergyDevices.push(deviceUtil.sampleEnergyDevices[0]);
    }
    deviceUtil.sampleEnergyDevices.forEach(sample => {
      for (let i in this.energyMeterDevices) {
        if (sample.type == this.energyMeterDevices[i].type) {
          contains = true;
          break;
        } else {
          contains = false;
        }
      }
      if (!contains && !this.existInSample(sample)) {
        this.sampleEnergyDevices.push(sample);
      }
    });
  }

  private existInSample(sample): boolean {
    let flag = false;
    this.sampleEnergyDevices.forEach(item => {
      if (item.id == sample.id) {
        flag = true;
      }
    });
    return flag;
  }
  //#endregion sampleDevices

  //#region Commands
  public async showGraphic(device: any) {
    let modal = await this.modalCtrl.create({
      component: GraphicModalPage,
      componentProps: {
        device: device,
        locationId: this.selectedLocation.locationId
      }
    })
    await modal.present();
  }

  public getEnergyDifferenceDevice() {
    this.energyDifferenceDevice = {
      energy: (this.energyMeterDevices[0] && this.energyMeterDevices[0].value && this.energyGeneratorDevices[0]) ? this.energyMeterDevices[0].value.energy - this.energyGeneratorDevices[0].value.energy : 0,
      type: 'difference'
    };
  }

  //#endregion

  //Refresher
  private refreshUI(): void {
    this.zone.run(() => {

    });
  }
  public scrollEvent = (event): void => {
    const footer = document.querySelector('#energyFooter');
    const homeTabsNavBar = document.querySelector('#homeTabsTabStyle');
    const energyNavBar = document.querySelector('#energyTabsNavBar');
    if (this.platform.is('desktop')){
      if (this.isElementInViewPort(footer, window.innerHeight)) {
        homeTabsNavBar.classList.add('dontDisplay');
        energyNavBar.classList.remove('dontDisplay');
      } else {
        homeTabsNavBar.classList.remove('dontDisplay');
        energyNavBar.classList.add('dontDisplay');
      }
    }

  };

  isElementInViewPort(element: Element, viewPortHeight: number) {
    let rect = element.getBoundingClientRect();
    return rect.top >= 0 && (rect.top <= viewPortHeight);
  }

}
