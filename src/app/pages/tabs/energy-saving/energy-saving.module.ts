import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EnergySavingPageRoutingModule } from './energy-saving-routing.module';

import { EnergySavingPage } from './energy-saving.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EnergySavingPageRoutingModule
  ],
  declarations: [EnergySavingPage]
})
export class EnergySavingPageModule {}
