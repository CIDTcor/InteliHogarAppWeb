import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.page.html',
  styleUrls: ['./loading.page.scss'],
})
export class LoadingPage implements OnInit {
  public frame: number = 0;
  private gradients: Array<String> = ['blue-color', 'purple-color', 'orange-color', 'green-color'];
  public gradient: String = this.gradients[0];

  constructor() { }

  ngOnInit() {
    this.initializeAnimation();
  }

  private initializeAnimation(): void {
    setInterval(() => {
      this.changeFrame();
      this.changeGradient();
    }, 250);
  }

  private changeFrame(): void {
    if (this.frame > 4) this.frame = -1;
    this.frame++;
  }

  private changeGradient(): void {
    if (this.frame == 0) {
      let index: number = this.gradients.indexOf(this.gradient);
      index++;
      if (index > 3) index = 0;
      this.gradient = this.gradients[index];
    }
  }

}
