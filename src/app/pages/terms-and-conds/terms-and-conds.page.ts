import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { dataBase } from '../../../assets/Util/FirebaseCommunication';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-terms-and-conds',
  templateUrl: './terms-and-conds.page.html',
  styleUrls: ['./terms-and-conds.page.scss'],
})
export class TermsAndCondsPage implements OnInit {
  public termsAndConditions;
  public privacyPolicy;
  public acceptAll;
  public bothChecked;
  checkAccept: HTMLElement;
  checkTos: HTMLElement;
  checkPp: HTMLElement;
  btnAcceptTerms: HTMLElement;

  constructor(
    public navCtrl: NavController,
    private authService: AuthService) {

  }

  ngOnInit() {
  }

  public checkAcceptAll(acceptBtn = null) {
    this.checkAccept = document.getElementById("checkAcceptAllAccept");
    this.checkTos = document.getElementById("checkAcceptAllTos");
    this.checkPp = document.getElementById("checkAcceptAllPp");
    this.btnAcceptTerms = document.getElementById("btnAcceptTerms");
    if (acceptBtn == 'accept') {
      if (this.acceptAll) {
        this.termsAndConditions = false;
        this.privacyPolicy = false;
        this.checkPp.setAttribute("checked", "false");
        this.checkTos.setAttribute("checked", "false");
      } else {
        this.termsAndConditions = true;
        this.privacyPolicy = true;
        this.bothChecked = true;
        this.checkPp.setAttribute("checked", "true");
        this.checkTos.setAttribute("checked", "true");
      }
      this.acceptAll = (this.termsAndConditions && this.privacyPolicy) ? true : false;
    } else if (acceptBtn == "pp") {
      (this.privacyPolicy) ? this.privacyPolicy = false : this.privacyPolicy = true;
      this.acceptAll = (this.termsAndConditions && this.privacyPolicy) ? true : false;
      this.checkAccept.setAttribute("checked", this.acceptAll);
    } else {
      (this.termsAndConditions) ? this.termsAndConditions = false : this.termsAndConditions = true;
      this.acceptAll = (this.termsAndConditions && this.privacyPolicy) ? true : false;
      this.checkAccept.setAttribute("checked", this.acceptAll);
    }
    (this.acceptAll) ? this.btnAcceptTerms.setAttribute("disabled", "false") : this.btnAcceptTerms.setAttribute("disabled", "true"); 
  }

  public acceptTerms() {
    dataBase.updateUserTermsAndConds(isAccepted => {
      if (isAccepted) {
        localStorage.setItem("statusTermsAndConds", "true");
        this.navCtrl.navigateRoot('locations')
      }
    });
  }

  public logout() {
    let savedData = JSON.parse(localStorage.getItem("savedData"));
    let subscriptionPlan = JSON.parse(localStorage.getItem("subscriptionPlan"));
    localStorage.clear();
    if (savedData) {
      localStorage.setItem("savedData", JSON.stringify(savedData));
    }
    if (subscriptionPlan) {
      localStorage.setItem("subscriptionPlan", JSON.stringify(subscriptionPlan));
    }
    // dataBase.logOut(isDone => {
    //   if (isDone) {
    //     this.navCtrl.navigateRoot('login');
    //   }
    // });
    this.authService.logout();
  }


}
