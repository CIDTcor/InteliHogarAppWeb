import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TermsAndCondsPageRoutingModule } from './terms-and-conds-routing.module';

import { TermsAndCondsPage } from './terms-and-conds.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TermsAndCondsPageRoutingModule
  ],
  declarations: [TermsAndCondsPage]
})
export class TermsAndCondsPageModule {}
