import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TermsAndCondsPage } from './terms-and-conds.page';

const routes: Routes = [
  {
    path: '',
    component: TermsAndCondsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TermsAndCondsPageRoutingModule {}
