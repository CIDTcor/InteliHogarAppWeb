import { Component, OnInit, NgZone } from '@angular/core';
import { Platform, AlertController, NavController, LoadingController, ToastController, ModalController } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';
import { dataBase } from '../../../assets/Util/FirebaseCommunication';
import { strings } from "../../../assets/Util/constants";
import { AuthService } from '../../services/auth.service';

import { EditLocationPage } from '../../modals/edit-location/edit-location.page';
import { Events } from '../../../assets/Util/events';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { isNgTemplate } from '@angular/compiler';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.page.html',
  styleUrls: ['./locations.page.scss','../hometabs/hometabs.page.scss'],
})
export class LocationsPage implements OnInit {
  public locations;
  private loader;
  private alertFlag = 0;
  public user;
  public locationsLoaded = false;
  public selectedLocation;

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public sanitizer: DomSanitizer,
    private platform: Platform,
    private zone: NgZone,
    private authService: AuthService,
    private modalCtrl: ModalController,
    private events: Events,
    private toastCtrl: ToastController,
    private inAppBrowser: InAppBrowser

  ) {
    this.user = JSON.parse(localStorage.getItem(strings.session.sessionUser));
    this.user = this.user ? this.user : JSON.parse(localStorage.getItem('savedData'));
    this.selectedLocation = JSON.parse(localStorage.getItem('selectedLocation'));
    this.initPage();
  }
  private initPage() {
    // this.checkTermsAndPrivacyStatus();
    this.locations = [];
    // this.showLoadingView();
    this.initSubscriptionPlanListener();
    this.getUserLocations();
    this.subscribeToUserUpdatedEvent();
  }

  ngOnInit() {
    let locationId = (JSON.parse(localStorage.getItem('locationId'))); //bryan
  
   
    if(locationId)
     { 
      dataBase.getLocationInfo(locationId, info => {
       // console.log(info, "info:::")
       // localStorage.setItem('selectedLocationTest', JSON.stringify(info));
             this.selectLocation(info);//bryan
          });
     }
  }

  // private checkTermsAndPrivacyStatus() {
  //   dataBase.checkTermsAndPrivacyStatus(isAccepted => {
  //     if (!isAccepted) {
  //       this.navCtrl.navigateRoot('terms-and-conds');
  //     }
  //   });
  // }

  private subscribeToUserUpdatedEvent() {
    this.events.subscribe('user:updated', user => {
      this.user.userImage = user.userImage;
      this.user.userName = user.userName;
    });
  }

  private async showLoadingView(message: string = "Cargando...") {
    this.loader = await this.loadingCtrl.create({
      message: message
    });
    await this.loader.present();
  }


  private initSubscriptionPlanListener(): void {
    let subscriptionPlans = localStorage.getItem("subscriptionPlans");
    if (!subscriptionPlans) {
      dataBase.initSubscriptionPlanListener(plans => {
        localStorage.setItem('subscriptionPlan', JSON.stringify(plans));
      });
    }
  }

  private getUserLocations() {
    dataBase.getUserLocations(this.user.userMail, locationsInfo => {
      if (locationsInfo.length > 0) {
        this.verifySelectedLocation(locationsInfo);
        this.getEveryLocationDevices();
        this.events.publish('accessLevelChanged', '');
      } 
      else {
       // this.loader.dismiss();
        //this.showEmptyLocationsAlert();
        this.locations = [];
        console.log("locations loaded");
        this.locationsLoaded = true;
      } 
      // let loaderInterval = setInterval(async () => {
      //   if (this.loadingCtrl.getTop()) {
      //     let modal = await this.loadingCtrl.getTop();
      //     if (modal) {
      //       this.loadingCtrl.dismiss();
      //     }
      //   } else {
      //     clearInterval(loaderInterval);
      //   }
      // }, 100);
    });
  }

  private getEveryLocationDevices() {
    this.locations.forEach(location => {
      dataBase.getDevices(location.locationId, devicesList => {
        devicesList.forEach(device => {
          if (location.subscriptionData) {
            if (!location.subscriptionData.subscriptedDevices.includes(device.id)) {
              location["devicesNotSubscribed"] = true;
              //TODO: añadir elemento para saber si el usuario tiene o no un plan de mantenimiento
            }
          }
        });
      });
    });
  }

  private async verifySelectedLocation(locationsInfo) {
    this.setLocations(locationsInfo);
    let selectedLocation = JSON.parse(
      localStorage.getItem(strings.session.selectedLocation)
    );
    if (selectedLocation) {
      this.updateLocationsSession(selectedLocation.locationId);
    }
    this.locationsLoaded = true;
 //   await this.loader.dismiss();
  }

  private async setLocations(locationsInfo) {
    for (let i in locationsInfo) {
      locationsInfo[i].locationMaps = this.sanitizer.bypassSecurityTrustResourceUrl(locationsInfo[i].locationMaps);
      locationsInfo[i].totalPlanPrice = this.getPlanPrice(locationsInfo[i].locationPlan,locationsInfo[i].subscriptionData);
    }
    this.locations = locationsInfo;
    this.refreshUI();
    // this.alertFlag++;
  }

  private getPlanPrice(locationPlanId,subscriptionData) {
    if (subscriptionData) {
      let totalPrice = parseInt(locationPlanId.substring(1)) + subscriptionData.maintenanceCost;
      return totalPrice;
    } else {
      return parseInt(locationPlanId.substring(1));
    }
  }
  // private async setLocations(locations) {
  //   for (let i in locations) {
  //     locations[i].locationMaps = this.sanitizer.bypassSecurityTrustResourceUrl(locations[i].locationMaps);
  //   }
  //   this.locations = locations;
  //   this.refreshUI();
  //   this.alertFlag++;
  //   await this.loader.dismiss();
  //   if (this.locations.length == 0 && this.alertFlag == 1) {
  //     this.showEmptyLocationsAlert();
  //   }
  // }

  private async showEmptyLocationsAlert() {
    let alert = await this.alertCtrl.create({
      header: 'Ups!',
      subHeader: 'Parece que aún no tienes localizaciones en tu cuenta, contacta con nosotros para adquirir tu primera o ingresa tu código si ya cuentas co uno',
      backdropDismiss: false,
      buttons: [
        {
          text: '¡Ya tengo código!',
          handler: () => {
            this.showLocationPrompt();
            this.alertFlag = 0;
          }
        },
        {
          text: "Contactar al servicio",
          handler: () => {
            //window.open("https://www.intelihogar.com/");
            this.inAppBrowser.create("https://www.intelihogar.com/", '_system');
            this.alertFlag = 0;
          }
        },
        {
          text: "Cerrar Sesión",
          handler: () => {
            this.alertFlag = 0;
            this.logout();
          }
        }
      ]
    });
    await alert.present();
  }

  private logout() {
    this.navCtrl.navigateRoot('loading');
    this.authService.logout();
    //   if (isDone) {
    //     let savedData = JSON.parse(localStorage.getItem("savedData"));
    //     localStorage.clear();
    //     if (savedData) localStorage.setItem("savedData", JSON.stringify(savedData));
    //     this.navCtrl.navigateRoot('login');
    //   }
    // });
  }

  public selectLocation(location) {
    this.checkIfUserCanAccessSelectedLocation(location.locationId, location.locationPlan, userCanAccess => {
      if (userCanAccess) {
        localStorage.setItem(strings.session.selectedLocation, JSON.stringify(location));
        this.events.publish('selectedLocation:updated', location);
        this.events.publish('reloadUsers', null);
        this.updateLocationsSession(location.locationId);
        this.navCtrl.navigateRoot('hometabs');
      } else {
        this.presentDeleteDevicesAlert(location);
      }
    });
  }

  private checkIfUserCanAccessSelectedLocation(locationId, locationPlan, callback) {
    let userPlatforms = this.platform.platforms();
    let myPlanPhones = JSON.parse(localStorage.getItem("subscriptionPlan"))[locationPlan].includes.phones;
    let myPlanUsers = JSON.parse(localStorage.getItem("subscriptionPlan"))[locationPlan].includes.users;
    Promise.all([dataBase.userExistsInLocation(locationId, userPlatforms), dataBase.getCurrentLocationPhones(locationId), dataBase.getCurrentLocationUserCount(locationId)])
      .then(response => {
        let userExists = response[0];
        let phonesNumber = response[1];
        let currentLocationUsers = response[2];
        if (currentLocationUsers <= myPlanUsers && phonesNumber <= myPlanPhones) {
          if (userExists || phonesNumber < myPlanPhones) {
            callback(true);
          } else {
            callback(false);
          }
        } else {
          callback(false);
        }
      });
  }

  private updateLocationsSession(id) {
    let session = this.locations;
    session.forEach(location => {
      if (id == location.locationId) location.selected = true;
      else location.selected = false;
    });
  }

  public navigateToUserProfile() {
    this.navCtrl.navigateForward('locations-user-profile');
  }

  public locationConfig(item) {
    item.showConfig = !item.showConfig;
  }

  // public async changeLocationName(location) {
  //   console.log(location);
  //   let prompt = await this.alertCtrl.create({
  //     header: "Cambiar nombre",
  //     subHeader: "ingrese el nuevo nombre de la localización",
  //     inputs: [
  //       {
  //         name: 'name',
  //         placeholder: 'Nombre'
  //       }
  //     ],
  //     buttons: ['Cancelar',
  //       {
  //         text: 'Cambiar',
  //         cssClass: (this.platform.is('android')) ? "primary-alert-button" : null,
  //         handler: data => {
  //           dataBase.changeLocationName(location.locationId, data.name);
  //           this.showLoadingView("Actualizando nombre...");
  //           this.getUserLocations();
  //         }
  //       }
  //     ]
  //   });

  //   await prompt.present();
  // }

  // public async deleteLocation(location) {
  //   let confirm = await this.alertCtrl.create({
  //     header: '¿Borrar localización?',
  //     subHeader: `¿Esta seguro que desea darse de baja de la localización ${location.locationName} ?`,
  //     buttons: [
  //       {
  //         text: 'No'
  //       },
  //       {
  //         text: 'Si',
  //         handler: () => {
  //           this.getLocationToDelete(location);
  //         }
  //       }
  //     ]
  //   });
  //   await confirm.present();
  // }

  // private getLocationToDelete(location) {
  //   let user = JSON.parse(localStorage.getItem(strings.session.sessionUser));
  //   let locationKey = location.locationId + "_" + user.userMail.replace(/\./g, "-");
  //   this.deleteLocationPreferences(location);
  //   dataBase.deleteLocation(locationKey);
  // }

  // private deleteLocationPreferences(location) {
  //   let selectedLocation = JSON.parse(localStorage.getItem(strings.session.selectedLocation));
  //   if (selectedLocation && selectedLocation.locationId == location.locationId) {
  //     localStorage.removeItem(strings.session.selectedLocation);
  //   }
  //   localStorage.removeItem(`shortcuts_${location.locationId}`);
  //   localStorage.removeItem(`devices_${location.locationId}`);
  //   localStorage.removeItem(`deviceAliasList_${location.locationId}`);
  //   localStorage.removeItem(`notifications_${location.locationId}`);
  //   localStorage.removeItem(`roomList_${location.locationId}`);
  //   this.navCtrl.navigateRoot('locations');
  // }

  // public async presentSharePrompt(location) {
  //   let prompt = await this.alertCtrl.create({
  //     header: 'Invitar usuario',
  //     subHeader: 'Ingrese el correo de la persona a invitar.',
  //     inputs: [{
  //       name: "mail",
  //       type: "email",
  //       placeholder: "Correo"
  //     }],
  //     buttons: [
  //       'Cancelar',
  //       {
  //         text: "Invitar",
  //         handler: data => {
  //           this.handleInvitation(data, location);
  //         }
  //       }
  //     ]
  //   });
  //   await prompt.present();
  // }

  // private handleInvitation(data, location) {
  //   let locationPlan = location.locationPlan;
  //   let myPlanUsers = JSON.parse(localStorage.getItem("subscriptionPlan"))[locationPlan].includes.users;
  //   dataBase.getCurrentLocationUsers(location).then(userNumber => {
  //     if ((userNumber <= myPlanUsers) || myPlanUsers == "ILIMITADOS") {
  //       this.sendInvitation(location, data.mail.toLowerCase());
  //       this.presentAlert("Invitación", "La invitación ha sido enviada correctamente.")
  //     } else {
  //       this.presentAlert("Invitación", "Ya ha invitado al número máximo que le permite su plan.")
  //     }
  //   });
  // }

  // private sendInvitation(location, mail) {

  //   // location.href = "mailto:someone@example.com?Subject=Hello%20again";
  //   // let user = JSON.parse(localStorage.getItem(strings.session.sessionUser)).userName
  //   // let email = {
  //   //   to: mail,
  //   //   subject: 'Invitación a localización',
  //   //   body: `<html>Has sido invitado a la localización de <strong>${user}</strong>.</html>`,
  //   //   isHtml: true
  //   // };
  //   // this.emailComposer.open(email);
  //   dataBase.getStatusTermsAndConds(mail, callback => {
  //     if (callback) {
  //       dataBase.createNewLocationUserInFirebase(location.locationId, mail, false, true);
  //     } else {
  //       dataBase.createNewLocationUserInFirebase(location.locationId, mail, false, false);
  //     }
  //   });
  // }

  public async showLocationPrompt() {
    let prompt = await this.alertCtrl.create({
      header: "Nueva localización",
      subHeader: "Entre el código de identificación del Hub",
      inputs: [
        {
          name: "title",
          placeholder: "Código"
        }
      ],
      buttons: [
        'Cancelar',
        {
          text: "Agregar",
          handler: data => {
            this.verificateLocation(data.title.toUpperCase() + "");
          }
        }
      ]
    });
    await prompt.present();
  }

  private verificateLocation(locationId) {
    dataBase.isItAnExistingLocation(locationId, exist => {
      if (exist) {
        dataBase.isFirstUser(locationId, isFirst => {
          if (isFirst) {
            this.createNewLocationUser(locationId);
          } else {
            this.presentAlert('Ups!', 'La localización seleccionada ya tiene dueño.');
          }
        });
      }
      this.presentAlert("Ups!", "Parece que la localización a la que quieres acceder no existe, verifica de nuevo.");
    });
  }

  private createNewLocationUser(locationId) {
    let userId = JSON.parse(localStorage.getItem("sessionUser")).userMail;
    dataBase.createNewLocationUser(locationId, userId);
  }

  public openStore(from, location = null) {
  
    let url;
    switch (from) {
      case 'store':
        url = `https://www.intelihogar.com/#!/tienda`;
        break;
      case 'cart':
        url = `https://www.intelihogar.com/#!/carrito`;
        break;
      case 'subscription':
        url = `https://www.intelihogar.com/#!/suscripciones?locationId=${location.locationId}&nameLocation=${location.locationName}`;
        break;
        
    }
    window.open(url, "_system");
    // this.inAppBrowser.create(url, '_blank');
  }

  public editUserInfo(info) {
    switch (info) {
      case 'name':
        this.launchEditUserModal('userName');
        break;
      case 'email':
        this.launchEditUserModal('userEmail');
        break;
      case 'password':
        this.launchEditUserModal('userPassword');
        break;
      case 'phone':
        this.launchEditUserModal('userPhoneNumber');
        break;
    }
  }

  private async launchEditUserModal(from) {
    let modal = await this.modalCtrl.create({
      component: EditLocationPage,
      componentProps: { from: from },
      backdropDismiss: true,
      showBackdrop: true,
      cssClass: 'editLocationModal'
    })
    await modal.present();
    modal.onDidDismiss().then(data => {
    });
  }

  public async openEditLocationModal(item) {
    let modal = await this.modalCtrl.create({
      component: EditLocationPage,
      componentProps: { from: 'locations', location: item },
      backdropDismiss: true,
      showBackdrop: true,
      cssClass: 'editLocationModal'
    })
    await modal.present();
    modal.onDidDismiss().then(data => {
      if (data.data) {
      //  this.showLoadingView("Actualizando nombre...");
       // this.getUserLocations();
      }
    });
  }

  public async presentAlert(title, msg) {
    let alert = await this.alertCtrl.create({
      header: title,
      subHeader: msg,
      buttons: ['Aceptar']
    });
    await alert.present();
  }

  private async presentDeleteDevicesAlert(location) {
    let alert = await this.alertCtrl.create({
      header: location.locationName,
      subHeader: `Lo sentimos, haz alcanzado el número máximo de usuarios y/o dispositivos permitido para tu plan. \n ¿Deseas eliminar usuarios y/o dispositivos?`,
      buttons: [
        {
          text: 'Ver mi hogar',
          handler: () => {
            dataBase.getCurrentUserAccessLevel(this.user.userMail, location.locationId, accessLevel => {
              if (accessLevel == "1") {
                this.manageLocationDevices(location);
              } else {
                this.showToast("No tienes permiso para realizar esta acción.")
              }
            });
          }
        },
        'Cancelar']
    });
    await alert.present();
  }

  private manageLocationDevices(location) {
    dataBase.getUserDevices(location.locationId, devices => {
      this.launchEditModal(location, devices);
    });
  }

  private async launchEditModal(location, devices) {
    let modal = await this.modalCtrl.create({
      component: EditLocationPage,
      componentProps: { from: 'locationDevices', location: location, locationDevices: devices },
      backdropDismiss: true,
      showBackdrop: true,
      cssClass: 'editLocationModal'
    })
    await modal.present();
    modal.onDidDismiss().then(data => {

    });
  }

  private refreshUI(): void {
    this.zone.run(() => {

    });
  }

  public async showToast(msg) {
    let toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    await toast.present();
  }

}
