import { Component, OnInit, NgZone } from '@angular/core';
import { LocationsPage } from '../locations/locations.page';
import { strings } from './../../../assets/Util/constants';
import { NavController, ToastController, AlertController, Platform, ModalController } from '@ionic/angular';
import { dataBase } from '../../../assets/Util/FirebaseCommunication';
import { AuthService } from '../../services/auth.service';
import { InteliUser } from '../../../assets/Util/InteliInterfaces';
import { AngularFireAuth } from '@angular/fire/auth';
import { AppComponent } from '../../app.component';
import { Events } from '../../../assets/Util/events';
import { SignInWithApple, AppleSignInResponse, AppleSignInErrorResponse, ASAuthorizationAppleIDRequest } from '@ionic-native/sign-in-with-apple/ngx';
import {Router, ActivatedRoute} from '@angular/router';
import { PasswordlessAuthComponent } from 'src/app/modals/passwordless-auth/passwordless-auth.page';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public isKeyboardShowing = false;
  public userPlatforms;
  //Component Type
  public passwordType: string = 'password';
  public passwordIcon: string = 'eye';

  //NgModels
  public email: string = "";
  public password: string = "";
  public saveSession: boolean = true;

  //Counters
  private wrongPasswordCounter = 0;
 
  constructor(
    public afAuth: AngularFireAuth,
    public navCtrl: NavController,
    private toastCtrl: ToastController,
    public alertCtrl: AlertController,
    private zone: NgZone,
    public platform: Platform,
    public appComponent: AppComponent,
    private events: Events,
    private authService: AuthService,
    private signInWithApple: SignInWithApple,
    private route: ActivatedRoute,
    public modalCtrl: ModalController,
    private router: Router) {
    this.userPlatforms = platform.platforms();
    // this.connectToHTTPS();
    let data = JSON.parse(localStorage.getItem("savedData"));
    this.setSavedData(data);
    this.subscribeToKeyboardEvent();
  }

  ngOnInit() {
    // this.logInWithAuth();
    if(this.router.url.includes('locationId')){
      const locationId: string = this.route.snapshot.queryParamMap.get('locationId');
    
       localStorage.setItem('locationId', JSON.stringify(locationId));
                  
       window.onbeforeunload = function() {
       localStorage.setItem('locationId', JSON.stringify(locationId));
        }
      }

    this.getAuthStatus();

  }

  private subscribeToKeyboardEvent() {
    this.events.subscribe('keyboard:updated', ev => {
      this.isKeyboardShowing = ev;
    });
  }
  //#region Session | Auth


  private getAuthStatus() {
    this.authService.getActualAuthStatus(res => {
      if (res && res.uid) {
        console.log(res,);
        this.setUserData(res);
      } else {
        this.navCtrl.navigateBack('/login');
      }
    });
  }

  private setUserData(user) {
    let displayName = localStorage.getItem("tempName");
    let userData: InteliUser = {
      provider: user.providerData[0].providerId,
      sInit: true,
      userImage: (user.photoURL) ? user.photoURL : "../../assets/imgs/avatar_default.jpg",
      userMail: user.email,
      userName: (user.displayName) ? user.displayName : (displayName) ? displayName : null,
      userPassword: null,
      userPhone: dataBase.getUserMainPhone(user.email.replace(/\./g, "-"))
    };
    localStorage.setItem("sessionUser", JSON.stringify(userData));
    this.getStatusTermsAndConditionsFromLS(userData);
    // this.userN = JSON.parse(localStorage.getItem("sessionUser")).userName;
    this.subscribeToUserNameUpdatedEvent();
  }
  
  private uploadImage(image: string, user): void {
    dataBase.uploadUserImage(user.userMail, image, (snap, error, path) => {
      if (snap) {
        console.log(snap); 
      }
      if (error) {
      }
      if (path) {
        console.log(path);
      }
    });
  }

  private subscribeToUserNameUpdatedEvent() {
    // this.events.subscribe('user:updated', user => {
    //   this.userN = JSON.parse(localStorage.getItem("sessionUser")).userName;
    // });
  }

  private getStatusTermsAndConditionsFromLS(userData) {
    let statusTermsAndConds = localStorage.getItem("statusTermsAndConds");
    if (statusTermsAndConds) {
      localStorage.setItem("statusTermsAndConds", "true");
      this.goToApp(userData);
    } else {
      this.getStatusTermsAndConditionsFromFB(userData);
    }
  }

  private getStatusTermsAndConditionsFromFB(userData) {
    dataBase.getStatusTermsAndConds(userData.userMail, callback => {
      if (callback) {
        localStorage.setItem("statusTermsAndConds", "true");
        this.goToApp(userData);
      } else {
        this.goToTermsAndConds();
      }
    })
  }

  public async openPasswordModal() {
    let modal;
    modal = await this.modalCtrl.create({
      component: PasswordlessAuthComponent,
      cssClass: 'device-window'
    });
   await modal.present();
  }

  private goToApp(userData: InteliUser) {
    // this.events.publish('user:updated', true);
    localStorage.removeItem("tempName");
    let selectedLocation = JSON.parse(localStorage.getItem("selectedLocation"));
    let locationId=(JSON.parse(localStorage.getItem('locationId')));
    
    if (selectedLocation && !locationId) {
      this.navCtrl.navigateRoot('hometabs');
      this.setUserImage(selectedLocation.locationId);
      //TODO: gestionar las imagenes de los usuarios que no inicien sesión con cuenta de google y que agreguen una imagen manualmente
    } else {
      this.navCtrl.navigateForward('locations')
    }
    //this.subscribeToNavigationEvents();
  }

  private goToTermsAndConds() {
    this.navCtrl.navigateRoot('terms-and-conds');
  }

  private setUserImage(locationId) {
    let user = JSON.parse(localStorage.getItem("sessionUser"));
    let userImage = user.userImage;
    dataBase.setUserImageInFirebase(locationId, userImage, user.userMail);
  }
  //#endregion



  // private connectToHTTPS() {
  //   let path = window.location.href.includes('https') ? null : 'https://localhost:8080'
  //   if (path) {
  //     location.href = path;
  //   }
  // }

  private setSavedData(data): void {
    if (data) {
      // this.zone.run(() => {
      this.email = data.userMail;
      this.password = data.userPass;
      // });
    }
  }

  //#region Login
  public login(): void {
    if (this.email == "" && this.password == "") {
      this.presentToast("Faltan datos de ingresar");
    }
    else {
      let sessionUser = JSON.parse(localStorage.getItem("sessionUser"));
      if (!sessionUser) {
        this.loginWithEmail();
      } else {
        this.navCtrl.navigateForward('locations');
        //if para ver si existe selectedlocation en local storage
        //si existe: redirigir a localizacion
        
      }
    }
  }

  private loginWithEmail(): void {
    this.authService.signInWithEmail(this.email, this.password, this.saveSession, (user, error) => {
    /*if (user) {
    } else */if (error) {
        switch (error.code) {
          case "auth/user-not-found":
            console.error("AUTHTENTICATION", error.message);
            this.showNoUserAlert();
            break;
          case "auth/wrong-password":
            this.wrongPasswordCounter++;
            if (this.wrongPasswordCounter > 1) {
              this.showWrongPasswordAlert();
            } else {
              console.error("AUTHTENTICATION", error.message);
              this.presentToast("Contraseña incorrecta.");
            }
            break;
          case "auth/invalid-email":
            console.error("AUTHTENTICATION", error.message);
            this.presentToast("Formato de correo inválido.");
            break;
          case "auth/too-many-requests":
            console.error("AUTHTENTICATION", error.message);
            this.presentToast("Demasiados intentos fallados. Intente nuevamente mas tarde");
            break;
          default:
            console.error("AUTHTENTICATION", error.message);
            this.presentToast("Ocurrió un problema, intente de nuevo.");
        }
      }
    });
  }

  public loginWithGoogle(): void {
    // localStorage.clear();
    let userPlatforms = this.platform.platforms();
    if ((userPlatforms.includes("android") || userPlatforms.includes("ios")) && !userPlatforms.includes("mobileweb")) {
      this.authService.loginWithGoogle().then(() => {
        this.navCtrl.navigateRoot('login');
      }).catch(err => {
        console.error(err);
      })
    } else {
      this.authService.signInWithGoogleWeb();
      //TODO: preguntarle al Elmo qué va aquí y qué respuesta esperamos recibir.
    }
  }

  public loginWithFacebook(): void {
    let userPlatforms = this.platform.platforms();
    if (userPlatforms.includes("android") && !userPlatforms.includes("mobileweb")) {
      this.authService.loginWithFacebook().then(res => {
        this.saveLoginFBDataInLS(res);
        this.navCtrl.navigateRoot('login')
      }).catch(err => {
        console.log(err)
        alert('hubo un error');
      })
    } else if (userPlatforms.includes("ios") && !userPlatforms.includes("mobileweb")) {
      this.authService.loginWithFacebook().then(res => {
        this.saveLoginFBDataInLS(res);
        this.navCtrl.navigateRoot('login')
      }).catch(err => {
        console.log(err)
        alert('hubo un error');
      })
      //TODO: puede ser eliminar pero como es iOS hay que ver primero.
    } else {
      this.authService.signInWithFacebookWeb();
      //TODO: preguntarle al Elmo qué va aquí y qué respuesta esperamos recibir.
    }
    // localStorage.clear();
  }

  public loginWithApple(): void {
    this.authService.loginWithApple().then(res => {
      this.saveLoginFBDataInLS(res);
      this.navCtrl.navigateRoot('login')
    }).catch(err => {
      console.log(err)
      alert('hubo un error ' + err);
    })
  }

  public saveLoginFBDataInLS(res) {
    let displayName = localStorage.getItem("tempName");
    let sessionUser: InteliUser = {
      provider: res.additionalUserInfo.providerId,
      sInit: true,
      userImage: (res.user.photoURL) ? res.user.photoURL : "../../assets/imgs/avatar_default.jpg",
      userMail: res.user.email,
      userName: (res.user.displayName) ? res.user.displayName : (displayName) ? displayName : res.user.email,
      userPassword: null,
      userPhone: dataBase.getUserMainPhone(res.user.email.replace(/\./g, "-"))
    };
    localStorage.setItem("sessionUser", JSON.stringify(sessionUser));
  }

  private async showWrongPasswordAlert() {
    let alert = await this.alertCtrl.create({
      header: "¿Necesitas ayuda?",
      subHeader: "Si olvidaste tu contraseña, no te preocupes nosotros te ayudamos",
      buttons: ["intentar nuevamente", {
        text: "Recuperar contraseña",
        handler: () => {
          this.showEmailPrompt()
        }
      }]
    });
    await alert.present();
  }

  public async showEmailPrompt() {
    let alert = await this.alertCtrl.create({
      header: "Ingresar correo",
      inputs: [{
        type: "email",
        name: "userMail"
      }],
      buttons: ["Cancelar", {
        text: "Enviar",
        handler: data => {
          this.authService.sendPasswordResetEmail(data.userMail, (wasSent, error) => {
            if (wasSent) {
              this.presentToast("Comprueba tu correo y sigue los pasos para restablecer tu contraseña");
            } else {
              switch (error.code) {
                case "auth/user-not-found":
                  this.presentToast("No existe usuario registrado con esa cuenta de correo");
                  this.showEmailPrompt();
                  break;
                case "auth/invalid-email":
                  this.presentToast("Formato de correo inválido");
                  this.showEmailPrompt();
                  break;
                default:
                  console.log(error);
              }
            }
          });
        }
      }]
    });
    await alert.present();
  }
  //#endregion

  //#region Create User
  private async showNoUserAlert() {
    let confirm = await this.alertCtrl.create({
      header: "Ups!",
      subHeader: "Parece que tu usuario es incorrecto o no existe, ¿Quieres volver a intentar o crear uno nuevo?",
      buttons: ["Volver a intentar", {
        text: 'Registrar',
        handler: () => {
          this.showNewUserPrompt();
        }
      }]
    });
    await confirm.present();
  }

  public async showNewUserPrompt() {
    let promtAlert = await this.alertCtrl.create({
      header: "Crear usuario",
      subHeader: "Ingrese los datos para crear nuevo usuario",
      inputs: [{
        name: 'userMail',
        type: 'email',
        placeholder: 'Dirección de correo'
      },
      {
        name: 'userName',
        placeholder: 'Nombre de usuario'
      },
      {
        name: 'userPassword',
        type: 'password',
        placeholder: 'Contraseña'
      },
      {
        name: 'userConfirmPassword',
        type: 'password',
        placeholder: 'Confirmar contraseña'
      }],
      buttons: ["Cancelar", {
        text: 'Crear',
        handler: (data) => {
          if (this.verificatePassword(data)) {
            this.addNewUser(data);
          }
        }
      }]
    });
    await promtAlert.present();
  }

  private verificatePassword(newUserData): boolean {
    if (newUserData.userPassword != "" && newUserData.userConfirmPassword != "" && newUserData.userMail != "" && newUserData.userName != "") {
      if (newUserData.userPassword == newUserData.userConfirmPassword) {
        return true;
      } else {
        this.presentToast("Las contraseñas no coinciden ingréselas de nuevo");
        this.showNewUserPrompt();
      }
    } else {
      this.presentToast("Algunos campos estan vacíos favor de llenarlos");
      this.showNewUserPrompt();
    }
    return false;
  }

  private addNewUser(newUserData): void {
    this.authService.createNewAccount(newUserData.userMail, newUserData.userPassword, newUserData.userName, (data, error) => {
      if (data) {
      } else if (error) {
        switch (error.code) {
          case "auth/weak-password":
            console.error("AUTHTENTICATION", error.message);
            this.showNewUserPrompt();
            this.presentToast("Contraseña corta, ingrese al menos 6 caracteres.");
            break;
          case "auth/invalid-email":
            console.error("AUTHTENTICATION", error.message);
            this.showNewUserPrompt();
            this.presentToast("Formato de correo inválido.");
            break;
          case "auth/email-already-in-use":
            console.error("AUTHTENTICATION", error.message);
            this.showNewUserPrompt();
            this.presentToast("Correo ya en uso.");
            break;
          default:
            console.error("AUTHTENTICATION", error.message);
            this.showNewUserPrompt();
            this.presentToast("Occurrió un error, vuelva a intentarlo.");
        }
      }
    });
  }
  //#endregion

  //#region Inputs Utils
  public showHideTextPass(): void {
    this.zone.run(() => {
      if (this.passwordType == 'password' && this.passwordIcon == 'eye') {
        this.passwordType = 'text';
        this.passwordIcon = 'eye-off'
      } else {
        this.passwordType = 'password';
        this.passwordIcon = 'eye'
      }
    });
  }

  public onKey(keyCode): void {
    if (keyCode == 13) this.login();
  }

  public updateUI(): void {
    this.zone.run(() => {

    });
  }

  public clearInputs(event): void {
    if (this.email == "") {
      this.password = "";
    }
  }
  //#endregion 

  //#region Utils
  private async presentToast(msg) {
    let toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom',
      cssClass: 'inteli-toast'
    });

    toast.present();
  }
  //#endregion


}
