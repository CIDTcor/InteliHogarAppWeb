import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateEditRoomPageRoutingModule } from './create-edit-room-routing.module';

import { CreateEditRoomPage } from './create-edit-room.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateEditRoomPageRoutingModule
  ],
  declarations: [CreateEditRoomPage]
})
export class CreateEditRoomPageModule {}
