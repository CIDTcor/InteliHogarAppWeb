import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateEditRoomPage } from './create-edit-room.page';

const routes: Routes = [
  {
    path: '',
    component: CreateEditRoomPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateEditRoomPageRoutingModule {}
