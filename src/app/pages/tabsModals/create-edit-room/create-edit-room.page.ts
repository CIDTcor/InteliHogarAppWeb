import { Component, OnInit, ViewChild } from '@angular/core';
import { iconSet } from "../../../../assets/Util/constants";
import { NavController, Platform, IonContent } from "@ionic/angular";
import * as roomUtils from '../../../../assets/Util/roomUtils';
import { dataBase } from '../../../../assets/Util/FirebaseCommunication';
import { ActivatedRoute, Router } from '@angular/router';
import { Events } from '../../../../assets/Util/events';

@Component({
  selector: 'app-create-edit-room',
  templateUrl: './create-edit-room.page.html',
  styleUrls: ['./create-edit-room.page.scss'],
})
export class CreateEditRoomPage implements OnInit {
  @ViewChild(IonContent, { static: true }) content: IonContent;
  public iconSet = iconSet;
  public selectedIcon = "icon-bed-owner";
  public roomName;
  public isKeyboardShowing = false;
  public currentPlatforms;
  private rooms;
  private selectedLocation;
  private navParams;
  previousUrl: string;

  constructor(
    private navCtrl: NavController,
    private route: ActivatedRoute,
    private router: Router,
    private events: Events,
    private platform: Platform
  ) {
    this.selectedLocation = JSON.parse(localStorage.getItem('selectedLocation'));
    this.rooms = JSON.parse(localStorage.getItem("roomList_" + this.selectedLocation.locationId));
    this.currentPlatforms = this.platform.platforms();
    this.subscribeToKeyboardEvent();
  }

  ngOnInit() {
   
    this.subscribeToNavParams();
  }

  ionViewDidEnter() {
    this.setBackgroundColor();
    this.scrollToTop();


  }
  scrollToTop() {
    this.content.scrollToTop(400);
  }

  private subscribeToNavParams() {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.navParams = this.router.getCurrentNavigation().extras.state.room;
      }
    });

    if (this.navParams) {
      this.selectedIcon = `icon-${this.navParams.roomType}`;
      this.roomName = this.navParams.roomAlias;
    }
  }

  private subscribeToKeyboardEvent() {
    this.events.subscribe('keyboard:updated', ev => {
      this.isKeyboardShowing = ev;
    });
  }

  private setBackgroundColor() {
    const el = document.querySelector('.addBackground') as HTMLElement;
    el.style.setProperty('background', 'linear-gradient(90deg, #5C8FF0 0%, #C456E5 100%)');
    const background = document.querySelector('.backBackground') as HTMLElement;
    background.style.setProperty('background', 'linear-gradient(90deg, #5C8FF0 0%, #C456E5 100%)');
  }

  public goBack() {
    this.navCtrl.back();
  }

  public updateRoom() {
    if (this.navParams) {
      this.updateExistingRoom();
    } else {
      this.selectedIcon = this.selectedIcon.substring(5, this.selectedIcon.length);
      let newRoom = { roomAlias: this.roomName, roomId: null, roomType: null, roomOrder: null };
      newRoom.roomType = this.selectedIcon;
      newRoom.roomOrder = roomUtils.generateNewRoomOrder(this.rooms);
      newRoom.roomId = roomUtils.generateNewRoomId(this.rooms);
      this.addNewRoom(newRoom);
    }
    this.navCtrl.back();
  }

  private updateExistingRoom() {
    let updatedRoom = this.navParams;
    updatedRoom.roomAlias = this.roomName;
    updatedRoom.roomType = this.selectedIcon.substring(5, this.selectedIcon.length);
    dataBase.updateRoomAliasAndIconDB(this.selectedLocation.locationId, updatedRoom);
  }

  private addNewRoom(newRoom) {
    console.log(newRoom);
    dataBase.addNewRoom(newRoom, this.selectedLocation.locationKey, isDone => {
    })
  }

  public scrollEvent = (event): void => {
    const footer = document.querySelector('#createEditFooter');
    const homeTabsNavBar = document.querySelector('#homeTabsTabStyle');
    const createEditTabsNavBar = document.querySelector('#createEditTabsNavBar');
    const backButton = document.querySelector('#editBackButton');
    const addRoomButton = document.querySelector('#da_addRoomButton');
    const footerBackButton = document.querySelector('#footerBackButton');
    const footerAddRoomButton = document.querySelector('#footerAddRoomButton');
    if (this.platform.is('desktop')) {
      if (this.isElementInViewPort(footer, window.innerHeight)) {
        console.log("confortNavBarMuestra")
        homeTabsNavBar.classList.add('dontDisplay');
        createEditTabsNavBar.classList.remove('dontDisplay');
        backButton.classList.add('dontDisplay');
        footerBackButton.classList.remove('dontDisplay');
        addRoomButton.classList.add('dontDisplay');
        footerAddRoomButton.classList.remove('dontDisplay');
      } else {
        console.log("confortNavBar-no-Muestra")
        homeTabsNavBar.classList.remove('dontDisplay');
        createEditTabsNavBar.classList.add('dontDisplay');
        backButton.classList.remove('dontDisplay');
        footerBackButton.classList.add('dontDisplay');
        addRoomButton.classList.remove('dontDisplay');
        footerAddRoomButton.classList.add('dontDisplay');

      }
    }

  };

  isElementInViewPort(element: Element, viewPortHeight: number) {
    let rect = element.getBoundingClientRect();
    return rect.top >= 0 && (rect.top <= viewPortHeight);
  }

}
