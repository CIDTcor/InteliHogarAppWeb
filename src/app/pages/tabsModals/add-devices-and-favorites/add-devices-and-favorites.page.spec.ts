import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddDevicesAndFavoritesPage } from './add-devices-and-favorites.page';

describe('AddDevicesAndFavoritesPage', () => {
  let component: AddDevicesAndFavoritesPage;
  let fixture: ComponentFixture<AddDevicesAndFavoritesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDevicesAndFavoritesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddDevicesAndFavoritesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
