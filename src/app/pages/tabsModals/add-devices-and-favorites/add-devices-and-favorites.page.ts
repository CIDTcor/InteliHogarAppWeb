import { Component, OnInit, ViewChild  } from '@angular/core';
import * as utils from '../../../../assets/Util/deviceUtil';
import { NavController, Platform,  IonContent  } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { dataBase } from '../../../../assets/Util/FirebaseCommunication';
import { Events } from '../../../../assets/Util/events';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-add-devices-and-favorites',
  templateUrl: './add-devices-and-favorites.page.html',
  styleUrls: ['./add-devices-and-favorites.page.scss'],
})
export class AddDevicesAndFavoritesPage implements OnInit {
  @ViewChild(IonContent, { static: true }) content: IonContent;
  private selectedLocation;
  private currentUser;
  private allDevices;
  private actualRoom;
  public showingAlias = true;
  private selectedDevices = [];
  private deselectedDevices = [];
  public pageGeneralTitle = "Mis favoritos:";
  public switchLightDevices = [];
  public adjustableLightDevices = [];
  public multiWhiteLightDevices = [];
  public colorLightDevices = [];
  public doorDevices = [];
  public windowDevices = [];
  public smartDoorDevices = [];
  public smartWindowDevices = [];
  public smartBlindDevices = [];
  public smartCurtainDevices = [];
  public garageDevices = [];
  public outerGarageDevices = [];
  public doorbellDevices = [];
  public heaterDevices = [];
  public coolerDevices = [];
  public thermostatDevices = [];
  public internalSirenDevices = [];
  public externalSirenDevices = [];
  public switchDevices = [];
  public valveDevices = [];
  public motionSensorDevices = [];
  public temperatureSensorDevices = [];
  public lightSensorDevices = [];
  public monoxideSensorDevices = [];
  public panicDevices = [];
  public inteliappDevices = [];
  public batteryDevices = [];
  public alarmDevices = [];
  public restDevices = [];
  public currentPlatforms;

  constructor(
    private navCtrl: NavController,
    private route: ActivatedRoute,
    private router: Router,
    private events: Events,
    public platform: Platform,
    private inAppBrowser: InAppBrowser
  ) {
    this.currentPlatforms = this.platform.platforms();
  }

  ngOnInit() {
    this.subscribeToNavParams();
    this.events.publish('appFullyLoaded', '');
    // this.getDataFromLocalStorage();
    // this.setDevices();
    // this.setPreSelectedDevices();
  }
  scrollToTop() {
    this.content.scrollToTop(400);
  }

  ionViewDidEnter() {
    this.resetDevices();
    this.getDataFromLocalStorage();
    this.setDevices();
    this.setPreSelectedDevices();
    document.getElementById('labelFavorites').style.color ='#3f75ff';
    this.scrollToTop();
  }

  private resetDevices() {
    this.selectedDevices = [];
    this.deselectedDevices = [];
    this.switchLightDevices = [];
    this.adjustableLightDevices = [];
    this.multiWhiteLightDevices = [];
    this.colorLightDevices = [];
    this.doorDevices = [];
    this.windowDevices = [];
    this.smartDoorDevices = [];
    this.smartWindowDevices = [];
    this.smartBlindDevices = [];
    this.smartCurtainDevices = [];
    this.garageDevices = [];
    this.outerGarageDevices = [];
    this.doorbellDevices = [];
    this.heaterDevices = [];
    this.coolerDevices = [];
    this.thermostatDevices = [];
    this.internalSirenDevices = [];
    this.externalSirenDevices = [];
    this.switchDevices = [];
    this.valveDevices = [];
    this.motionSensorDevices = [];
    this.temperatureSensorDevices = [];
    this.lightSensorDevices = [];
    this.monoxideSensorDevices = [];
    this.panicDevices = [];
    this.inteliappDevices = [];
    this.batteryDevices = [];
    this.alarmDevices = [];
    this.restDevices = [];
  }

  private subscribeToNavParams() {
    this.route.queryParams.subscribe(params => {
      this.actualRoom = null;
      const el = document.querySelector('.addBackground') as HTMLElement;
      const element = document.querySelector('.backBackground') as HTMLElement;
      if (this.router.getCurrentNavigation().extras.state) {
        this.actualRoom = this.router.getCurrentNavigation().extras.state.actualRoom;
        this.pageGeneralTitle = "Añadir dispositivos:"
        el.style.setProperty('background', 'linear-gradient(90deg, #5C8FF0 0%, #C456E5 100%)');
        element.style.setProperty('background', 'linear-gradient(90deg, #5C8FF0 0%, #C456E5 100%)');
      } else {
        this.pageGeneralTitle = "Mis favoritos:";
        el.style.setProperty('background', 'linear-gradient(90deg, #3F75FF 0%, #2AD8DA 100%)');
        element.style.setProperty('background', 'linear-gradient(90deg, #3F75FF 0%, #2AD8DA 100%)');
      }
    });
  }

  private getDataFromLocalStorage() {
    this.selectedLocation = JSON.parse(localStorage.getItem('selectedLocation'));
    this.currentUser = JSON.parse(localStorage.getItem('sessionUser'));
    this.allDevices = JSON.parse(localStorage.getItem(`devices_${this.selectedLocation.locationId}`));
    if (this.pageGeneralTitle == "Mis favoritos:") {
      let tempDevices = JSON.parse(localStorage.getItem(`shortcuts_${this.selectedLocation.locationId}`))
      if (tempDevices) {
        this.selectedDevices = tempDevices;
      } else {
        this.selectedDevices = [];
      }
    } else {
      this.allDevices.forEach(device => {
        if (device.roomId == this.actualRoom) {
          this.selectedDevices.push(device.id);
        }
      });
    }
  }

  private setPreSelectedDevices() {
    let intersectedDevices = this.allDevices.filter(element => this.selectedDevices.includes(element.id));
    intersectedDevices.forEach(device => {
      device.checked = true;
    });
  }

  private setDevices() {
    this.allDevices.forEach(device => {
      device.checked = false;
      switch (device.type) {
        case 'switchLight':
          this.switchLightDevices.push(device);
          break;
        case 'adjustableLight':
        case 'dimmer':
          this.adjustableLightDevices.push(device);
          break;
        case 'multiWhiteLight':
          this.multiWhiteLightDevices.push(device);
          break;
        case 'colorLight':
          this.colorLightDevices.push(device);
          break;
        case 'doorContact':
          this.doorDevices.push(device);
          break;
        case 'windowContact':
          this.windowDevices.push(device);
          break;
        case 'window':
          this.smartWindowDevices.push(device);
          break;
        case 'doorbell':
          this.doorbellDevices.push(device);
          break;
        case 'heater':
          this.heaterDevices.push(device);
          break;
        case 'cooler':
          this.coolerDevices.push(device);
          break;
        case 'garage':
          this.garageDevices.push(device);
          break;
        case 'internalSiren':
          this.internalSirenDevices.push(device);
          break;
        case 'externalSiren':
          this.externalSirenDevices.push(device);
          break;
        case 'curtain':
          this.smartCurtainDevices.push(device);
          break;
        case 'smartBlind':
          this.smartBlindDevices.push(device);
          break;
        case 'motion':
          this.motionSensorDevices.push(device);
          break;
        // case 'smoke':
        //   this.smokeDevices.push(device);
        //   break;
        case 'battery':
          this.batteryDevices.push(device);
          break;
        case 'inteliAppsGeneral':
        case 'inteliAppsEnergy':
        case 'inteliAppsSecurity':
        case 'simulacionPresencia':
        case 'helloHome':
          this.inteliappDevices.push(device);
          break;
        // case 'alarmaAgua':
        // case 'alarmaIntrusos':
        // case 'alarmaIncendio':
        // case 'alarmaGas':
        //   this.alarmDevices.push(device);
        //   break;
        // case 'helloHome':
        //   this.routines.push(device);
        //   break;
        case 'temperature':
          this.temperatureSensorDevices.push(device);
          break;
        case 'thermostat':
          this.thermostatDevices.push(device);
          break;
        case 'lightSensor':
          this.lightSensorDevices.push(device);
          break;
        case 'monoxide':
          this.monoxideSensorDevices.push(device);
          break;
        case 'switch':
          this.switchDevices.push(device);
          break;
        case 'panic':
          this.panicDevices.push(device);
          break;
        case 'lockDoor':
          this.smartDoorDevices.push(device);
          break;
        case 'outerDoor':
          this.outerGarageDevices.push(device);
          break;
        case 'valve':
          this.valveDevices.push(device);
          break;
        default:
          this.restDevices.push(device);
      }
    });
  }

  public selectDevice(device) {
    device.checked = !device.checked;
    if (device.checked) {
      this.selectedDevices.push(device.id);
    } else {
      this.selectedDevices = this.selectedDevices.filter(x => x !== device.id);
      this.deselectedDevices.push(device.id);
    }
  }

  public confirmDevices() {
    if (this.actualRoom) {
      let intersectedSelectedDevices = this.allDevices.filter(element => this.selectedDevices.includes(element.id));
      intersectedSelectedDevices.forEach(device => {
        device.roomId = this.actualRoom;
        dataBase.updateDeviceRoomId(this.selectedLocation.locationId, device);
      });
      let intersectedDeselectedDevices = this.allDevices.filter(element => this.deselectedDevices.includes(element.id));
      intersectedDeselectedDevices.forEach(device => {
        device.roomId = "noRoomAssigned";
        dataBase.updateDeviceRoomId(this.selectedLocation.locationId, device);
      });

    } else {
      let locationId = this.selectedLocation.locationId;
      let userMail = this.convertDotToDash(this.currentUser.userMail);
      utils.addDevicesListToShortcuts(locationId, this.selectedDevices);
      dataBase.addDevicesToFavorites(locationId, userMail, this.selectedDevices);
      this.events.publish('favoritesUpdated', "data");
    }
    this.goBack();
  }

  public openStore(deviceType) {
    let URL;
    switch (deviceType) {
      case "lockDoor":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Cerradura-PRO`;
        break;
      case "switchLight":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Iluminacion-On-Off`;
        break;
      case "adjustableLight":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Kit-Iluminacion-Atenuable`;
        break;
      case "multiWhiteLight":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Kit-Foco-Multicolor`;
        break;
      case "colorLight":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Kit-Foco-Multicolor`;
        break;
      case "doorContact":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Sensor-Puerta`;
        break;
      case "windowContact":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Sensor-Ventana`;
        break;
      case "window":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Ventana-Inteligente`;
        break;
      case "smartBlind":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Conversion-Persianas-Sheer`;
        break;
      case "curtain":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Conversion-Cortina-Inteligente`;
        break;
      case "garage":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Conversion-garaje`;
        break;
      case "outerDoor":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Conversion-Porton-Exterior`;
        break;
      case "doorbell":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Timbre-Exterior`;
        break;
      case "heater":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Calefaccion-Inteligente-Piso`;
        break;
      case "cooler":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Conversion-Aire-Lavado`;
        break;
      case "thermostat":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Termostato-Inteligente`;
        break;
      case "internalSiren":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Sirena-Interior`;
        break;
      case "externalSiren":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Sirena-Exterior`;
        break;
      case "switch":
        URL = `https://www.intelihogar.com/#!/tienda`;
        break;
      case "valve":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Valvula-Agua`;
        break;
      case "motion":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Sensor-Movimiento`;
        break;
      case "temperature":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Sensor-Temperatura`;
        break;
      case "lightSensor":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Sensor-Iluminacion`;
        break;
      case "monoxide":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Detector-Humo-CO`;
        break;
      case "panic":
        URL = `https://www.intelihogar.com/#!/detallesDeProducto?product=Boton-Panico`;
        break;
      case "inteliAppsGeneral":
        URL = `https://www.intelihogar.com/#!/tienda`;
        break;
      case "battery":
        URL = `https://www.intelihogar.com/#!/tienda`;
        break;
    }
    //window.open(URL, "_system");
    location.href = URL;
    // this.inAppBrowser.create(URL, '_system');
  }

  public goBack() {
    if (this.actualRoom) {
      this.navCtrl.navigateBack('hometabs/confort', {
        state: {
          actualRoom: this.actualRoom
        }
      })
      const roomsContent = document.getElementById('roomsContent');
      const content = document.getElementById('confortContent');
      roomsContent.style.height = '0';
      roomsContent.style.padding = '0';
      roomsContent.style.minHeight = '0';
      content.style.minHeight = '80%';
      content.style.height = 'auto';
      content.style.paddingBottom = '95px';
    } else {
      this.navCtrl.back();
    }
  }

  private convertDotToDash(input) {
    return input.replace(/\./g, "-");
  }
  public scrollEvent = (event): void => {
    const footer = document.querySelector('#addDevicesFooter');
    const homeTabsNavBar = document.querySelector('#homeTabsTabStyle');
    const confortNavBar = document.querySelector('#addDevicesNavBar');
    const addRoomButton = document.querySelector('#addRoomButtonDevices');
    const footerAddRoomButton = document.querySelector('#footerAddRoomButtonDevices');
    const footerBackButton = document.querySelector('#footerBackButtonDevices');
    const backButton = document.querySelector('#backButtonDevices');
    if (this.platform.is('desktop')) {
      if (this.isElementInViewPort(footer, window.innerHeight)) {
        console.log("confortNavBarMuestra")
        homeTabsNavBar.classList.add('dontDisplay');
        confortNavBar.classList.remove('dontDisplay');
        addRoomButton.classList.add('dontDisplay');
        footerAddRoomButton.classList.remove('dontDisplay');
        footerBackButton.classList.remove('dontDisplay');
        backButton.classList.add('dontDisplay');
      } else {
        console.log("confortNavBar-no-Muestra")
        homeTabsNavBar.classList.remove('dontDisplay');
        confortNavBar.classList.add('dontDisplay');
        addRoomButton.classList.remove('dontDisplay');
        footerAddRoomButton.classList.add('dontDisplay');
        footerAddRoomButton.classList.add('dontDisplay');
        footerBackButton.classList.add('dontDisplay');
        backButton.classList.remove('dontDisplay');

      }
    }

  };

  isElementInViewPort(element: Element, viewPortHeight: number) {
    let rect = element.getBoundingClientRect();
    return rect.top >= 0 && (rect.top <= viewPortHeight);
  }

}
