import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddDevicesAndFavoritesPageRoutingModule } from './add-devices-and-favorites-routing.module';

import { AddDevicesAndFavoritesPage } from './add-devices-and-favorites.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddDevicesAndFavoritesPageRoutingModule
  ],
  declarations: [AddDevicesAndFavoritesPage]
})
export class AddDevicesAndFavoritesPageModule {}
