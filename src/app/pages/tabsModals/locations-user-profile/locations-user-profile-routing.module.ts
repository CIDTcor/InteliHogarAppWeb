import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LocationsUserProfilePage } from './locations-user-profile.page';

const routes: Routes = [
  {
    path: '',
    component: LocationsUserProfilePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LocationsUserProfilePageRoutingModule {}
