import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LocationsUserProfilePageRoutingModule } from './locations-user-profile-routing.module';

import { LocationsUserProfilePage } from './locations-user-profile.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LocationsUserProfilePageRoutingModule
  ],
  declarations: [LocationsUserProfilePage]
})
export class LocationsUserProfilePageModule {}
