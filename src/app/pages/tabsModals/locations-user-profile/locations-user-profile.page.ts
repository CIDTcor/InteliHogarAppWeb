import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { dataBase } from '../../../../assets/Util/FirebaseCommunication';
import { AuthService } from '../../../services/auth.service';
import { EditLocationPage } from '../../../modals/edit-location/edit-location.page';
import { UserProfilePage } from '../../../pages/tabsModals/user-profile/user-profile.page';
import { Events } from '../../../../assets/Util/events';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-locations-user-profile',
  templateUrl: './locations-user-profile.page.html',
  styleUrls: ['./locations-user-profile.page.scss','../../hometabs/hometabs.page.scss'],
})
export class LocationsUserProfilePage implements OnInit {

  public user;
  public selectedLocation;
  constructor(
    private navCtrl: NavController,
    private authService: AuthService,
    private modalCtrl: ModalController,
    private userProfile: UserProfilePage,
    private events: Events,
    private inAppBrowser: InAppBrowser
  ) { }

  ngOnInit() {
    this.setUserInfo();
  }

  private setUserInfo() {
    this.user = JSON.parse(localStorage.getItem('sessionUser'));
    this.selectedLocation = JSON.parse(localStorage.getItem('selectedLocation'));
    this.events.subscribe('user:updated', user => {
      this.user.userImage = user.userImage;
      this.user.userName = user.userName;
    });
  }

  public openStore(from, location = null) {
    let url;
    switch (from) {
      case 'store':
        url = `https://www.intelihogar.com/#!/tienda`;
        break;
      case 'cart':
        url = `https://www.intelihogar.com/#!/carrito`;
        break;
    }
    //window.open(url, "_system");
    this.inAppBrowser.create(url, '_system');
  }

  public gobackToLocations() {
    this.navCtrl.back();
  }

  public editUserInfo(info) {
    switch (info) {
      case 'name':
        this.launchEditModal('userName');
        break;
      case 'email':
        this.launchEditModal('userEmail');
        break;
      case 'password':
        this.launchEditModal('userPassword');
        break;
      case 'phone':
        this.launchEditModal('userPhoneNumber');
        break;
    }
  }

  private async launchEditModal(from) {
    let modal = await this.modalCtrl.create({
      component: EditLocationPage,
      componentProps: { from: from },
      backdropDismiss: true,
      showBackdrop: true,
      cssClass: 'editLocationModal'
    })
    await modal.present();
    modal.onDidDismiss().then(data => {
    });
  }

  public logout() {
    this.navCtrl.navigateRoot('loading');
    let savedData = JSON.parse(localStorage.getItem("savedData"));
    let subscriptionPlan = JSON.parse(localStorage.getItem("subscriptionPlan"));
    let sessionUser = JSON.parse(localStorage.getItem("sessionUser"));
    localStorage.clear();
    if (savedData) {
      localStorage.setItem("savedData", JSON.stringify(savedData));
    }
    if (subscriptionPlan) {
      localStorage.setItem("subscriptionPlan", JSON.stringify(subscriptionPlan));
    }
    if (sessionUser) {
      localStorage.setItem("sessionUser", JSON.stringify(sessionUser));
    }
    // dataBase.logOut(isDone => {
    //   if (isDone) {
    //     alert.dismiss();
    //     this.navCtrl.navigateRoot('login');
    //   }
    // });
    this.authService.logout();
  }

  public showAvatarActionSheet() {
    this.userProfile.showAvatarActionSheet();
  }
}
