import { Component, OnInit, NgZone,  ViewChild} from '@angular/core';
import { UserProfile, InteliUser } from '../../../../assets/Util/InteliInterfaces';
import { NavController, AlertController, ActionSheetController, Platform, ToastController, LoadingController, ModalController, IonContent } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Events } from '../../../../assets/Util/events';
import { strings } from "../../../../assets/Util/constants";
import { dataBase } from '../../../../assets/Util/FirebaseCommunication';
import { AuthService } from '../../../services/auth.service';
import { EditLocationPage } from '../../../modals/edit-location/edit-location.page';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { DomSanitizer } from '@angular/platform-browser';
import { PasswordlessAuthComponent } from 'src/app/modals/passwordless-auth/passwordless-auth.page';
import * as firebase from 'firebase';




declare function setHeaderTitle(roomAlias, room, roomType);

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.page.html',
  styleUrls: ['./user-profile.page.scss','../../hometabs/hometabs.page.scss'],
})
export class UserProfilePage implements OnInit {
  @ViewChild(IonContent, { static: true }) content: IonContent;
  public user: InteliUser;
  public accessLevel: number;
  public allUserLocations;
  private loadingView;
  public selectedLocation;
  public userDevices;
  public showUserContent = true;
  public currentUser;
  public savedData;
  public locations;
  public state:boolean = false;

  constructor(
   
    public navCtrl: NavController,
    public sanitizer: DomSanitizer,
    public alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController,
    public platform: Platform,
    private zone: NgZone,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private camera: Camera,
    private authService: AuthService,
    private modalCtrl: ModalController,
    public events: Events,
    private inAppBrowser: InAppBrowser,
    
    ) {
    
      this.subscribeToUsersEvent();
  }

  ngOnInit() {

    /*
    if(localStorage.getItem('2faAuth'))  {
      let twofaAuth = localStorage.getItem('2faAuth');
      if(twofaAuth == 'true')
      {

      } 
    } */
    this.locations = [];  
    this.getPlanPrice();
    this.setUserData();
    this.getUserDevices(this.selectedLocation.locationId);
    this.events.publish('appFullyLoaded', '');


    
    
  }
  scrollToTop() {
    this.content.scrollToTop(400);
  }

  ionViewDidEnter() {
    const el = document.querySelector('.addBackground') as HTMLElement;
    el.style.setProperty('background', '#F4F8F8');
    const background = document.querySelector('.backBackground') as HTMLElement;
    background.style.setProperty('background', '#f4f8f8');
    let element = document.querySelector('.myProfileCaption') as HTMLElement;
    element.style.setProperty('font-weight', 'bold');
    setHeaderTitle(this.selectedLocation.locationName, "favorites", null);
    this.scrollToTop();
    const secondTab = document.querySelector('#secondTab');
    const userProfileTabStyle = document.querySelector('#userProfileTabStyle');
    userProfileTabStyle.classList.add('dontDisplay');
    secondTab.classList.remove('dontDisplay');
    
  }

  ionViewDidLeave() {
    let element = document.querySelector('.myProfileCaption') as HTMLElement;
    element.style.setProperty('font-weight', 'normal');
  }

  private subscribeToUsersEvent() {
    this.events.subscribe('reloadUserProfileUsers', () => {
      this.getUsersFromSelectedLocation();
    });
  }

  private setUserData(): void {
    this.getUsersFromSelectedLocation();
    let user = JSON.parse(localStorage.getItem("sessionUser"));
    this.savedData = JSON.parse(localStorage.getItem("savedData"));
    this.user = user;
    this.selectedLocation = JSON.parse(localStorage.getItem(strings.session.selectedLocation));

    if (user.userImage && user.userImage == "") {
      user.userImage = "../../assets/imgs/avatar_default.jpg"
    }
    this.setCurrentLocationAccessLevel();
    this.getCurrentUser();
    this.subscribeToEvent();
  }

  private getCurrentUser() {
    this.currentUser = JSON.parse(localStorage.getItem('sessionUser'));
  }

  private setCurrentLocationAccessLevel() {
    // let user = JSON.parse(localStorage.getItem(strings.session.sessionUser));
    // let location = JSON.parse(localStorage.getItem(strings.session.selectedLocation));
    // dataBase.getUserLocations(user.userMail, userLocations => {
    //   if (userLocations) {
    //     userLocations.forEach(element => {
    //       if (location.locationId == element.locationId) {
    //         this.accessLevel = element.accessLevel;
    //       }
    //     });
    //   }
    // });
    this.accessLevel = this.selectedLocation.accessLevel;

  }

  private async getUsersFromSelectedLocation() {
    this.allUserLocations = JSON.parse(localStorage.getItem("currentLocationUsers"));
    // dataBase.getAllLocationUsers(callback => {
    // this.allUserLocations = callback;
    // })
  }

  private subscribeToEvent() { 
    this.events.subscribe('selectedLocation:updated', data => {
      this.accessLevel = parseInt(data.accessLevel);
      this.selectedLocation = data;
    });
    this.events.subscribe('user:updated', user => {
      this.user = user;
    });
  }

  /*private publishNavigationEvent(root: boolean = true, page: any) {
    this.events.publish("goTo", root, page);
  }*/

  //#region Profile
  //Display Name
  public async changeDisplayName() {
    let prompt = await this.alertCtrl.create({
      header: 'Cambiar nombre de usuario',
      subHeader: 'Ingrese su nuevo nombre de usuario',
      inputs: [{
        type: "text",
        value: this.user.userName,
        name: "displayName"
      }],
      buttons: [
        {
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "Cambiar",
          cssClass: (this.platform.is('android')) ? "primary-alert-button" : null,
          handler: (data) => {
            let newData: UserProfile = {
              displayName: data.displayName,
              photoURL: null
            };
            this.showLoading("Actualizando...");
            this.updateProfile(newData);
          }
        }
      ]
    });
    await prompt.present();
  }

  public editUserInfo(info) {
    switch (info) {
      case 'name':
        this.launchEditModal('userName');
        break;
      case 'password':
        this.launchEditModal('userPassword');
        break;

      case 'phone1':
        this.launchEditModal('userPhoneNumber1');
        break;

      case 'phone2':
        this.launchEditModal('userPhoneNumber2');
        break;
        case 'mainPhone':
        this.launchEditModal('userMainPhone');
        break;
    }
  }

  public async launchEditModal(from) {
    let modal = await this.modalCtrl.create({
      component: EditLocationPage,
      componentProps: { from: from, location: this.selectedLocation },
      backdropDismiss: true,
      showBackdrop: true,
      cssClass: 'editLocationModal'
    })
    await modal.present();
    modal.onDidDismiss().then(data => {
      this.setUserData();
    });
   // prevent event when the modal is about to hide

  }

  //Avatar
  public async showAvatarActionSheet() {

    let sheet = await this.actionSheetCtrl.create(
      {
        header: 'Foto de perfil',
        buttons: [
          {
            text: "Tomar foto",
            icon: !this.platform.is('ios') ? "camera" : null,
            handler: () => {
             // this.showLoading();
              this.takePhoto();
            }
          },
          {
            text: "Subir una foto",
            icon: !this.platform.is('ios') ? "images" : null,
            handler: () => {
            this.showLoading();          
            
            
            }
          },
          {
            role: "cancel",
            icon: !this.platform.is('ios') ? "close" : null,
            text: "Cancelar"
          }
        ]
      });
    await sheet.present();
  }

  private takePhoto(): void {
    let options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.CAMERA,
      allowEdit: true,
      correctOrientation: true,
      cameraDirection: this.camera.Direction.FRONT
    };

    this.camera.getPicture(options)
      .then((imageData) => {
        let image: string = `data:image/jpeg;base64,${imageData}`;
        this.uploadImage(image);
      })
      .catch((error) => {
        this.catchError(error);
      });
  }

  private selectImage(): void {
  
    let options: CameraOptions = {
      quality: 70, 
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false,
      allowEdit: true,
      targetWidth: 300,
      targetHeight: 300
    };

    this.camera.getPicture(options)
      .then(imageData => {
        let image: string = `data:image/jpeg;base64,${imageData}`;
        this.uploadImage(image);
      })
      .catch(error => {
        this.catchError(error);
      });
  }

  private uploadImage(image: string): void {
    let user = JSON.parse(localStorage.getItem('sessionUser'));
    dataBase.uploadUserImage(user.userMail, image, (snap, error, path) => {
      if (snap) {
        this.setUploadProgress(snap.bytesTransferred, snap.totalBytes);
        console.log(snap);
      }
      if (error) {
        this.catchError(error);
      }
      if (path) {
        console.log(path);
        this.updateProfile({
          displayName: null,
          photoURL: path
        });
      }
    });
  }

  private setUploadProgress(bytesTransferred: any, totalBytes: any) {
    let progress = Math.round((bytesTransferred / totalBytes) * 100);
    this.loadingView.setContent(`${progress}% completado...`);
    this.refreshUI();
  }

  //Utils
  public updateProfile(newData: UserProfile): void {
    this.authService.updateUserProfile(newData, (isDone, error) => {
      if (error) {
        this.catchError(error);
      } else if (isDone) {
        this.saveNewData(newData);
      }
    });
  }

  private catchError(error): void {
    console.log(error);
    this.dismissLoading();
    if (error != "No Image Selected")
      this.showToast("Ocurrió un error, intente de nuevo más tarde");
  }

  private saveNewData(newData: UserProfile): void {
    this.user = JSON.parse(localStorage.getItem("sessionUser"));
    this.user.userName = (newData.displayName) ? newData.displayName : this.user.userName;
    this.user.userImage = (newData.photoURL) ? newData.photoURL : this.user.userImage;
    let locationId = JSON.parse(localStorage.getItem('selectedLocation')).locationId;
    localStorage.setItem("sessionUser", JSON.stringify(this.user));
    dataBase.setUserImageInFirebase(locationId, this.user.userImage, this.user.userMail);
    this.dismissLoading();
    this.showToast("Perfil actualizado correctamente");
    this.refreshUI();
    this.publishPorfileUpdateEvent();
  }

  private publishPorfileUpdateEvent(): void {
    this.events.publish('user:updated', this.user);
  }
  //#endregion

  //#region Account
  public async showConfirmPasswordChangeAlert() {
    let alert = await this.alertCtrl.create(
      {
        header: '¿Seguro?',
        subHeader: 'Si continua recibirá un correo con las instrucciones para restablecer su contraseña. ¿Está seguro que desea continuar?',
        buttons: [
          {
            text: "Cancelar",
            role: "cancel"
          },
          {
            text: "Aceptar",
            cssClass: (this.platform.is('android')) ? "primary-alert-button" : null,
            handler: () => {
              this.sendResetPasswordEmail();
            }
          }
        ]
      }
    );
    await alert.present();
  }

  public async showDeleteUserAlert(locationId, userMail) {
    let alert = await this.alertCtrl.create(
      {
        header: '¿Seguro?',
        subHeader: 'Si continua se eliminará por completo el usuario y todos los dispositivos asociados. ¿Desea Continuar?',
        buttons: [
          {
            text: "Cancelar",
            role: "cancel"
          },
          {
            text: "Aceptar",
            cssClass: (this.platform.is('android')) ? "primary-alert-button" : null,
            handler: () => {
              this.deleteUserLocationAndDevices(locationId, userMail);
              this.allUserLocations.forEach((user, i) => {
                if (user.userId == userMail) {
                  this.allUserLocations.splice(i, 1);
                  // TODO: POSIBLE FUTURO ERROR AL NO BORRAR LOCAL STORAGE
                }
              });
              // console.log(this.allUserLocations);
            }
          }
        ]
      }
    );
    await alert.present();
  }

  public sendResetPasswordEmail(): void {
    this.authService.sendPasswordResetEmail(this.user.userMail, (isDone, error) => {
      if (isDone) {
        this.showToast("Correo enviado correctamente, compruebe su bandeja de entrada");
      } else if (error) {
        this.catchError(error);
      }
    });
  }

  public showPasswordHelp(): void {
    let passwordMsg: string = "Te llegará un mensaje a tu bandeja de correo y ahí podras seguir las instrucciones para restablecer tu contraseña";
    this.showAlert("Ayuda", passwordMsg, null);
  }

  public goToWebPage(): void {
    let location = JSON.parse(localStorage.getItem("selectedLocation"));

    if (location.owner) {
      this.openWindow();
    } else {
      this.showAlert(null, "Necesita ser dueño de la localización para utilizar esta opción", null);
    }
  }
  public goToStoreWebPage(): void {
    let URL = `https://www.intelihogar.com/#!/tienda`;
    //window.open(URL, "_system");
    this.inAppBrowser.create(URL, '_system');
  }

  public goToSubscriptionPage() {
    let URL = `https://www.intelihogar.com/#!/suscripciones?locationId=${this.selectedLocation.locationId}&nameLocation=${this.selectedLocation.locationName}`;
    //window.open(URL, "_system");
    this.inAppBrowser.create(URL, '_system');
  }

  public async goToUserDevices(userId) {
    let currentUserDevices = []
    this.userDevices.forEach(device => {
      if (device.user == userId) {
        currentUserDevices.push(device);
      }
    });
    let modal = await this.modalCtrl.create({
      component: EditLocationPage,
      componentProps: { from: 'userDevices', location: this.selectedLocation, userId: userId, userDevices: currentUserDevices },
      backdropDismiss: true,
      showBackdrop: true,
      cssClass: 'editLocationModal'
    })
    await modal.present();
    modal.onDidDismiss().then(data => {
    });
  }

  private openWindow(): void {
    let user = JSON.parse(localStorage.getItem("sessionUser"));
    let locationId = JSON.parse(localStorage.getItem("selectedLocation")).locationId
    let URL = `https://www.intelihogar.com/backend/?userid=${user.userMail}&locationid=${locationId}&origin=movil`;

    //window.open(URL, "_system");
    this.inAppBrowser.create(URL, '_system');
  }

  public async showLogoutAlert() {
    this.logout();
    // let alert = await this.alertCtrl.create(
    //   {
    //     header: '¿Seguro?',
    //     subHeader: 'Si continuas perderás todos tus accesos directos y notificaciones. ¿Estás seguro?',
    //     buttons: [
    //       {
    //         text: "Cancelar",
    //         role: "cancel"
    //       },
    //       {
    //         text: "Continuar",
    //         cssClass: (this.platform.is('android')) ? "primary-alert-button" : null,
    //         handler: () => {
    //           alert.dismiss();
    //           this.logout(alert);
    //         }
    //       }
    //     ]
    //   }
    // );
    // await alert.present();
  }

  public logout(): void {
    // alert.dismiss();
    this.navCtrl.navigateRoot('loading');
    dataBase.deleteToken(JSON.parse(localStorage.getItem('selectedLocation')).locationId, isDone => {
      if (isDone) {
        let savedData = JSON.parse(localStorage.getItem("savedData"));
        let subscriptionPlan = JSON.parse(localStorage.getItem("subscriptionPlan"));
        let sessionUser = JSON.parse(localStorage.getItem("sessionUser"));
        let userToken = localStorage.getItem("token");
        localStorage.clear();
        if (savedData) {
          localStorage.setItem("savedData", JSON.stringify(savedData));
        }
        if (subscriptionPlan) {
          localStorage.setItem("subscriptionPlan", JSON.stringify(subscriptionPlan));
        }
        if (sessionUser) {
          localStorage.setItem("sessionUser", JSON.stringify(sessionUser));
        }
        if (userToken) {
          localStorage.setItem("token", userToken);
        }
        // dataBase.logOut(isDone => {
        //   if (isDone) {
        //     alert.dismiss();
        //     this.navCtrl.navigateRoot('login');
        //   }
        // });
        this.authService.logout();
      }
    });
  }

  private getUserDevices(locationId): void {
    dataBase.getUserDevices(locationId, callback => {
      this.userDevices = callback;
    });
  }

  //#endregion

  //#region UI Utils
  public async easterEgg() {
    let alert = await this.alertCtrl.create({
      header: 'I SEE YOU!',
      subHeader: 'I SEE YOU!',
      message: 'I SEE YOU!'
    });
    await alert.present();

    setTimeout(() => {
      alert.dismiss();
    }, 1000);
  }

  private async showToast(msg: string) {
    let toast = await this.toastCtrl.create({
      message: msg,
      buttons: ["Aceptar"],
      // showCloseButton: true,
      cssClass: 'inteli-toast',
      // dismissOnPageChange: true
    });

    await toast.present();
  }

  private async showLoading(msg: string = "Cargando...") {
    this.loadingView = await this.loadingCtrl.create(
      {
        message: msg,
        backdropDismiss: false
      }
    );
   await this.loadingView.present();
  }

  private dismissLoading(): void {
    if (this.loadingView) {
      this.loadingView.dismiss();
    }
  }

  private refreshUI(): void {
    this.zone.run(() => {

    });
  }

  private async showAlert(title: string, msg: string, action) {
    let alert = await this.alertCtrl.create(
      {
        header: (title) ? title : null,
        message: msg,
        buttons: (action) ? action : [
          {
            text: 'Aceptar',
            role: 'cancel'
          }
        ]
      }
    );
    await alert.present();
  }
  //#endregion
  //#region LocationUsers
  public async deleteUserDevice(locationId, deviceToken) {
    let alert = await this.alertCtrl.create(
      {
        header: 'Alerta',
        subHeader: 'Eliminar dispositivo',
        buttons: [
          'Cancelar',
          {
            text: 'Aceptar',
            handler: () => {
              dataBase.deleteUserDevice(locationId, deviceToken);
            }
          }
        ]
      }
    );
    await alert.present();
  }

  public async deleteUser(from, user, index) {
    let modal = await this.modalCtrl.create({
      component: EditLocationPage,
      componentProps: { from: from, title: 'Si continua se eliminará por completo el usuario y todos los dispositivos asociados. ¿Desea Continuar?' },
      backdropDismiss: true,
      showBackdrop: true,
      cssClass: 'editLocationModal'
    })
    await modal.present();
    modal.onDidDismiss().then(data => { 
     
      if (data.data == 'accept') {
        console.log(user, "user:;");
        if (user.superUser) {
            this.presentAlert("Alerta", "No puedes eliminar este usuario porque es el Administrador.");
          } else {
            this.deleteUserLocationAndDevices(this.selectedLocation.locationId, user.userId);
            this.events.publish('reloadUsers', null);
            this.getUsersFromSelectedLocation();
            this.allUserLocations.splice(index,1);
            console.log(this.allUserLocations);
            localStorage.setItem('currentLocationUsers', JSON.stringify(this.allUserLocations));
          }
      }
    });
  }
  public async presentAlert(title, msg) {
    let alert = await this.alertCtrl.create({
      header: title,
      subHeader: msg,
      buttons: ['Aceptar']
    });
    await alert.present();
  }



  public async changeUserAccessLevel(user) {
    let alert = await this.alertCtrl.create({
      header: "Nivel de acceso",
      cssClass: 'accessLevelCssClass',
      inputs: [
        {
          type: "radio",
          label: "Dueño",
          value: "1",
          checked: user.accessLevel == 1 ? true : false
        },
        {
          type: "radio",
          label: "Usuario",
          value: "2",
          checked: user.accessLevel == 2 ? true : false
        },
        {
          type: "radio",
          label: "Menor",
          value: "3",
          checked: user.accessLevel == 3 ? true : false
        },
        {
          type: "radio",
          label: "Invitado",
          value: "4",
          checked: user.accessLevel == 4 ? true : false
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
        },
        {
          text: 'Aceptar',
          handler: (data) => {
            console.log(data)
            let locationUserId = this.convertDotToDash(`${user.locationId}_${user.userId}`);
            user.accessLevel = data;
            user.owner = data == 1 ? true : false;
            dataBase.changeUserAccessLevel(locationUserId, user)
            // if (user.userId == this.currentUser.userMail) {
            //   this.accessLevel = data;
            // }
          }
        }
      ]
    });
    await alert.present();
  }

  public deleteUserLocationAndDevices(locationId, userMail) {
    dataBase.deleteUserLocation(locationId, userMail)
    dataBase.deleteAllUserDevices(locationId, userMail)
  }

  private convertDotToDash(input) {
    return input.replace(/\./g, "-");
  }

  public toggleContent(contentId) {
    document.getElementById(contentId).hidden = !document.getElementById(contentId).hidden;
    document.getElementById(`${contentId}_icon1`).hidden = !document.getElementById(`${contentId}_icon1`).hidden;
    document.getElementById(`${contentId}_icon2`).hidden = !document.getElementById(`${contentId}_icon1`).hidden;
  }
  public scrollEvent = (event): void => {
    const footer = document.querySelector('#userProfileFooter');
    const secondTab = document.querySelector('#secondTab');
    //const favoritesNavBar = document.querySelector('#favoritesTabStyle');
    const userProfileTabStyle = document.querySelector('#userProfileTabStyle');
    if (this.platform.is('desktop')) {
      if (this.isElementInViewPort(footer, window.innerHeight)) {
        console.log('muestra');
        secondTab.classList.add('dontDisplay');
        userProfileTabStyle.classList.remove('dontDisplay');
        //favoritesNavBar.classList.remove('dontDisplay');
      } else {
        console.log(' no muestra');
        secondTab.classList.remove('dontDisplay');
        userProfileTabStyle.classList.add('dontDisplay');
        //favoritesNavBar.classList.add('dontDisplay');
      }
    }

  };

  isElementInViewPort(element: Element, viewPortHeight: number) {
    let rect = element.getBoundingClientRect();
    return rect.top >= 0 && (rect.top <= viewPortHeight);
  }
  //#endregion LocationUsers

  //#region INVITATION

  //#endregion

  public openStore(from, location = null) {
    let url;
    switch (from) {
      case 'store':
        url = `https://www.intelihogar.com/#!/tienda`;
        break;
      case 'cart':
        url = `https://www.intelihogar.com/#!/carrito`;
        break;
      case 'subscription':
        url = `https://www.intelihogar.com/#!/suscripciones?locationId=${location.locationId}&nameLocation=${location.locationName}`;
        break;
    }
    window.open(url, "_system");
    // this.inAppBrowser.create(url, '_blank');
  }


  private getPlanPrice() {
    let locationInfoS = (JSON.parse(localStorage.getItem('selectedLocation'))); //bryan
    this.locations = locationInfoS;
   // console.log(locationInfoS, '\n', locationInfoS.locationPlan);
  }
  public savePhoto(ev):void{
    dataBase.uploadUserPhoto(this.user.userMail, ev.target.files[0]).then(response => {
      this.user.userImage = response;
     
     // console.log(this.user);

      let newData: UserProfile = {
        displayName: null,
        photoURL: response
      };
      this.updateProfile(newData);
        });
    

  }
   public openInputFile(){
     document.getElementById('inputImg').click();
    
   }
   public async smsAuth(){
   
    if(this.state == false){
        let modal;
        modal = await this.modalCtrl.create({
          component: PasswordlessAuthComponent,
          cssClass: 'device-window',
          backdropDismiss: true,
         keyboardClose: false,
        });
      await modal.present();
      }
  }
  


  

}
