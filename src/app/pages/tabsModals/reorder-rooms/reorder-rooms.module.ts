import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReorderRoomsPageRoutingModule } from './reorder-rooms-routing.module';

import { ReorderRoomsPage } from './reorder-rooms.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReorderRoomsPageRoutingModule
  ],
  declarations: [ReorderRoomsPage]
})
export class ReorderRoomsPageModule {}
