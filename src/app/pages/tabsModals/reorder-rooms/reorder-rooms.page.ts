import { Component, OnInit, ViewChild } from '@angular/core';
import { dataBase } from '../../../../assets/Util/FirebaseCommunication';
import { NavController, Platform, IonContent } from "@ionic/angular";

@Component({
  selector: 'app-reorder-rooms',
  templateUrl: './reorder-rooms.page.html',
  styleUrls: ['./reorder-rooms.page.scss'],
})
export class ReorderRoomsPage implements OnInit {
  @ViewChild(IonContent, { static: true }) content: IonContent;
  public roomsList = [];
  public selectedLocation;
  constructor(
    public platform: Platform,
    private navCtrl: NavController
    ) { }

  ngOnInit() {
    this.getDataFromLocalStorage();
  }
  scrollToTop() {
    this.content.scrollToTop(400);
  }

  ionViewDidEnter(){
    const background = document.querySelector('.backBackground') as HTMLElement;
    background.style.setProperty('background', 'linear-gradient(90deg, #5C8FF0 0%, #C456E5 100%)');
    this.scrollEvent("event");
  }

  private getDataFromLocalStorage() {
    this.selectedLocation = JSON.parse(localStorage.getItem('selectedLocation'));
    this.roomsList = JSON.parse(localStorage.getItem(`roomList_${this.selectedLocation.locationId}`));
  }

  public doReorder(ev: any) {
    ev.detail.complete(this.roomsList);
  }

  public goBack() {
    this.navCtrl.back();
  }

  public reorderDevices() {
    this.roomsList.forEach(device => {
      dataBase.updateRoomOrder(this.selectedLocation.locationId, device, this.roomsList.indexOf(device));
    });
    this.goBack();
  }
  public scrollEvent = (event): void => {
    const footer = document.querySelector('#reorderFooter');
    const homeTabsNavBar = document.querySelector('#homeTabsTabStyle');
    const reorderNavBar = document.querySelector('#reorderTabsNavBar');
    const reorderAddRoomButton = document.querySelector('#reorderAddRoomButton');
    const backButton = document.querySelector('#backButton');
    const reorderAddButtonFooter = document.querySelector('#reorderAddButtonFooter');
    const reorderBackButtonFooter = document.querySelector('#reorderBackButtonFooter');
    //const content = document.querySelector('#content');
    console.log(this.platform, "platform");
    if (this.platform.is('desktop')) {
      if (this.isElementInViewPort(footer, window.innerHeight)) {
        homeTabsNavBar.classList.add('dontDisplay');
       reorderNavBar.classList.remove('dontDisplay');
       reorderAddRoomButton.classList.add('dontDisplay');
       backButton.classList.add('dontDisplay');
       reorderAddButtonFooter.classList.remove('dontDisplay');
       reorderBackButtonFooter.classList.remove('dontDisplay');
      } else {
        homeTabsNavBar.classList.remove('dontDisplay');
        reorderNavBar.classList.add('dontDisplay');
        reorderAddRoomButton.classList.remove('dontDisplay');
         backButton.classList.remove('dontDisplay');
         reorderAddButtonFooter.classList.add('dontDisplay');
         reorderBackButtonFooter.classList.add('dontDisplay');
      }
    }
  };

  isElementInViewPort(element: Element, viewPortHeight: number) {
    let rect = element.getBoundingClientRect();
    return rect.top >= 0 && (rect.top <= viewPortHeight);
  }

}
