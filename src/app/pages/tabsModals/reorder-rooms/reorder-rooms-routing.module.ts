import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReorderRoomsPage } from './reorder-rooms.page';

const routes: Routes = [
  {
    path: '',
    component: ReorderRoomsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReorderRoomsPageRoutingModule {}
