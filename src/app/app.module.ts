import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { Network } from '@ionic-native/network/ngx';
import { Device } from '@ionic-native/device/ngx';
import { Vibration } from '@ionic-native/vibration/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { ChangeDeviceRoomPageModule } from 'src/app/modals/change-device-room/change-device-room.module';
import { ThermostatWindowPageModule } from 'src/app/modals/thermostat-window/thermostat-window.module';
import { ColorLightWindowPageModule } from 'src/app/modals/color-light-window/color-light-window.module';
import { OrderListWindowPageModule } from 'src/app/modals/order-list-window/order-list-window.module';
import { EditLocationPageModule } from 'src/app/modals/edit-location/edit-location.module';
import { HubOrDeviceOfflinePageModule } from 'src/app/modals/hub-or-device-offline/hub-or-device-offline.module';
import { FirebaseX } from "@ionic-native/firebase-x/ngx";

import { GooglePlus } from "@ionic-native/google-plus/ngx";
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireModule } from 'angularfire2';

import { Facebook } from "@ionic-native/facebook/ngx";
import { UserProfilePage } from './pages/tabsModals/user-profile/user-profile.page';
import { HAMMER_GESTURE_CONFIG, HammerGestureConfig } from '@angular/platform-browser';
import { File } from '@ionic-native/file/ngx';
import { SignInWithApple } from '@ionic-native/sign-in-with-apple/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { LocationsPage } from './pages/locations/locations.page';
import { MobileAccessibility } from '@ionic-native/mobile-accessibility/ngx';
import { PasswordlessAuthPageModule } from './modals/passwordless-auth/passwordless-auth.module';

export class MyHammerConfig extends HammerGestureConfig {
  overrides = <any>{
    'pinch': { enable: false },
    'rotate': { enable: false }
  }
}

export const firebaseConfig = {
  apiKey: "AIzaSyBtdVjV-sORR4YUSOYaadW-96iyIpdrCjI",
  authDomain: "intelihogardb.firebaseapp.com",
  databaseURL: "https://intelihogardb.firebaseio.com",
  projectId: "intelihogardb",
  storageBucket: "intelihogardb.appspot.com",
  messagingSenderId: "709660632353"
};

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    AppRoutingModule,
    ChangeDeviceRoomPageModule,
    ColorLightWindowPageModule,
    ThermostatWindowPageModule,
    OrderListWindowPageModule,
    EditLocationPageModule,
    HubOrDeviceOfflinePageModule,
    PasswordlessAuthPageModule
  ],
  providers: [
    Facebook,
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Device,
    Vibration,
    Network,
    FirebaseX,
    Camera,
    GooglePlus,
    AngularFireAuth,
    UserProfilePage,
    AppComponent,
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: MyHammerConfig
    },
    File,
    SignInWithApple,
    InAppBrowser,
    LocationsPage,
    MobileAccessibility

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
