import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { Router } from "@angular/router";

import * as firebase from "firebase";
import { WindowService } from "./WindowService";
import { environment } from "../../../environments/environment";
import { ModalController } from "@ionic/angular";

export class PhoneNumber {
  wholeNumber: string = "526352943942";

  get e164() {
    const num = this.wholeNumber;
    return `+${num}`;
  }
}

@Component({
  selector: "app-passwordless-auth",
  templateUrl: "./passwordless-auth.page.html",
  styleUrls: ["./passwordless-auth.page.scss"],
  providers: [WindowService],
})
export class PasswordlessAuthComponent implements OnInit {
  public state: boolean = false;
  public windowRef: any;
  public phoneNumber = new PhoneNumber();
  public inputValue: string;
  verificationCode: string;
  user: any;
  constructor(private modalCtrl: ModalController, private win: WindowService) {}

  ngOnInit() {
    this.windowRef = this.win.windowRef;
    // const new_fire = firebase.initializeApp(environment.firebase)
    this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
      "recaptcha-container",
      {
        size: "invisible",
        callback: function (response) {
          console.log(response);
        },
      }
    );
    this.windowRef.recaptchaVerifier.render();
  }

  sendLoginCode() {
    // Sign in the Google user first.

    let _this = this;
    firebase
      .auth()
      .signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then(function (result) {
        _this.nextPage("modal3");
        // Google user signed in. Check if phone number added.
        if (!result.user.phoneNumber) {
          // Ask user for phone number.
          var phoneNumber = "+526352943942";
          // You also need to provide a button element signInButtonElement
          // which the user would click to complete sign-in.
          // Get recaptcha token. Let's use invisible recaptcha and hook to the button.
          let signInButtonElement = document.getElementById("signInButton");
          var appVerifier = new firebase.auth.RecaptchaVerifier(
            signInButtonElement,
            {
              size: "invisible",
              callback: () => {
                console.log("callback!");
              },
            }
          );
          // This will wait for the button to be clicked the reCAPTCHA resolved.
          return result.user
            .linkWithPhoneNumber(phoneNumber, appVerifier)
            .then(function (confirmationResult) {
              // Ask user to provide the SMS code.
              var code = window.prompt("Provide your SMS code");
              // Complete sign-in.
              console.log("hola");
              return confirmationResult.confirm(code);
            });
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  

  verifyLoginCode() {
    this.windowRef.confirmationResult
      .confirm(this.verificationCode)
      .then((result) => {
        this.user = result.user;
        localStorage.setItem("2faAuth", "true");
      })
      .catch((error) => console.log(error, "Incorrect code entered?"));
  }

  public nextPage(info) {
    switch (info) {
      case "modal1":
        document.getElementById("confirmModal").style.display = "none";
        document.getElementById("infoVerify").style.display = "block";
        break;
      case "modal2":
        console.log("modal2");
        document.getElementById("infoVerify").style.display = "none";
        document.getElementById("confPassword").style.display = "block";
        this.sendLoginCode();

        break;

      case "modal3":
        document.getElementById("confPassword").style.display = "none";
        document.getElementById("screenVerify").style.display = "block";
        break;

      case "phone2":
        break;
    }
  }
  closeModal() {
    $("#2faToggle").prop("checked", false);
    this.modalCtrl.dismiss();
  }
}
