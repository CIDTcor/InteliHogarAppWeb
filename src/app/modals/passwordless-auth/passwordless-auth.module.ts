import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PasswordlessAuthPageRoutingModule } from './passwordless-auth-routing.module';

import { PasswordlessAuthComponent } from './passwordless-auth.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PasswordlessAuthPageRoutingModule
  ],
  declarations: [PasswordlessAuthComponent]
})
export class PasswordlessAuthPageModule {}
