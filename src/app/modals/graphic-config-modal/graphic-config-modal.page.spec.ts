import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GraphicConfigModalPage } from './graphic-config-modal.page';

describe('GraphicConfigModalPage', () => {
  let component: GraphicConfigModalPage;
  let fixture: ComponentFixture<GraphicConfigModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphicConfigModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GraphicConfigModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
