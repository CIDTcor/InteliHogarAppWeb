import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GraphicConfigModalPageRoutingModule } from './graphic-config-modal-routing.module';

import { GraphicConfigModalPage } from './graphic-config-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GraphicConfigModalPageRoutingModule
  ],
  declarations: [GraphicConfigModalPage]
})
export class GraphicConfigModalPageModule {}
