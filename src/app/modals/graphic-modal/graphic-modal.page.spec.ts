import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GraphicModalPage } from './graphic-modal.page';

describe('GraphicModalPage', () => {
  let component: GraphicModalPage;
  let fixture: ComponentFixture<GraphicModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphicModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GraphicModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
