import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GraphicModalPageRoutingModule } from './graphic-modal-routing.module';

import { GraphicModalPage } from './graphic-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GraphicModalPageRoutingModule
  ],
  declarations: [GraphicModalPage]
})
export class GraphicModalPageModule {}
