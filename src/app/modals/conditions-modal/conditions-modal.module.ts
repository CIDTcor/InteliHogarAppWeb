import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConditionsModalPageRoutingModule } from './conditions-modal-routing.module';

import { ConditionsModalPage } from './conditions-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConditionsModalPageRoutingModule
  ],
  declarations: [ConditionsModalPage]
})
export class ConditionsModalPageModule {}
