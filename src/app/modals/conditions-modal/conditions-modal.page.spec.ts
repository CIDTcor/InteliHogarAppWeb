import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConditionsModalPage } from './conditions-modal.page';

describe('ConditionsModalPage', () => {
  let component: ConditionsModalPage;
  let fixture: ComponentFixture<ConditionsModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConditionsModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConditionsModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
