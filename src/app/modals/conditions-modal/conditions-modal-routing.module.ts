import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConditionsModalPage } from './conditions-modal.page';

const routes: Routes = [
  {
    path: '',
    component: ConditionsModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConditionsModalPageRoutingModule {}
