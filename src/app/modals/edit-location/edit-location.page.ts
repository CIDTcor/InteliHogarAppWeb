import { Component, OnInit, NgZone } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { dataBase } from '../../../assets/Util/FirebaseCommunication';
import { strings } from "../../../assets/Util/constants";
import { AuthService } from '../../services/auth.service';
import { UserProfilePage } from '../../pages/tabsModals/user-profile/user-profile.page';
import { Events } from '../../../assets/Util/events';


@Component({
  selector: 'app-edit-location',
  templateUrl: './edit-location.page.html',
  styleUrls: ['./edit-location.page.scss'],
})
export class EditLocationPage implements OnInit {

  from
  location
  userId
  userDevices
  locationDevices
  title

  public modalTitle;
  public modalIcon;
  public modalIconText;
  public inputPlaceholder;
  public inputValue;
  public locationUsers;
  public currentUserName;

  public myPlanPhones;
  public myPlanUsers;
  public phonesNumber;
  public currentLocationUsers;

  public devicesWithNoUser = [];

  constructor(
    public modalCtrl: ModalController,
    private zone: NgZone,
    private alertCtrl: AlertController,
    private authService: AuthService,
    private userProfile: UserProfilePage,
    private events: Events
  ) { }

  ngOnInit() {
    this.currentUserName = JSON.parse(localStorage.getItem("sessionUser")).userName;
    this.adaptModal(this.from);
  }

  public adaptModal(display) {
    this.from = display
    switch (this.from) {
      case 'locations':
      case 'changeAlarmModal':
        break;
      case 'invite':
        this.modalTitle = "Escribe el correo del usuario a invitar:";
        this.modalIcon = "mail-outline";
        this.modalIconText = "Email:";
        this.inputPlaceholder = "Correo electrónico";
        break;
      case 'changeName':
        this.modalTitle = "Escribe el nuevo nombre de tu hogar:";
        this.modalIcon = "home-outline";
        this.modalIconText = "Nombre:";
        this.inputPlaceholder = this.location.locationName;
        break;
      case 'deleteLocation':
        this.modalTitle = "¿Estás seguro que quieres eliminar este hogar en tu perfil?";
        break;
      case 'userName':
        this.modalTitle = "Escribe el nuevo nombre de usuario:";
        this.modalIcon = "person-outline";
        this.modalIconText = "Nombre:";
        this.inputPlaceholder = this.currentUserName;
        break;
      case 'userEmail':
        this.modalTitle = "Escribe el nuevo correo:";
        this.modalIcon = "mail-outline";
        this.modalIconText = "Email:";
        this.inputPlaceholder = "Correo electrónico";
        break;
      case 'userPassword':
        this.modalTitle = "Si continúas recibirás un correo con las instrucciones para restablecer tu contraseña";
        break;
      case 'userPhoneNumber1':
      case 'userMainPhone':
      case 'userPhoneNumber2':
        this.modalTitle = "Escribe el nuevo numero de telefono:";
        this.modalIcon = "call-outline";
        this.modalIconText = "Numero:";
        this.inputPlaceholder = "Numero";
        break;
        
      case 'generalModal':
        this.modalTitle = this.title;
        break;
      case 'locationDevices':
        this.locationDevices;
        this.getCurrentPhoneAndUsersInLocation();
        this.removeRepeatedUsers();
        break;
    }
    this.refreshUI();
  }

  public acceptInviteDeleteButton() {
    switch (this.from) {
      case 'invite':
        this.handleInvitation();
        this.closeModal();
        break;
      case 'changeName':
        this.changeLocationName();
        this.closeModal('nameChanged');
        break;
      case 'deleteLocation':
        let currentUserMail = JSON.parse(localStorage.getItem(strings.session.sessionUser)).userMail;
        this.isUserSuperUser(currentUserMail,isSuperUser => {
          if (isSuperUser) {
            this.presentAlert("Alerta", "no puedes eliminar este hogar porque eres el dueño.");
          } else {
            this.deleteUserLocation();
          }
        });
        this.closeModal();
        break;
      case 'userName':
        this.userProfile.updateProfile({ displayName: this.inputValue, photoURL: null });
        this.closeModal();
        break;
      case 'userPassword':
        this.userProfile.sendResetPasswordEmail();
        this.closeModal();
        break;
      case 'userPhoneNumber1':
        this.updateUserPhoneNumber("userPhoneNumber1");
        this.closeModal();
        break;
      case 'userPhoneNumber2':
        this.updateUserPhoneNumber("userPhoneNumber2");
        this.closeModal();
        break;
        case 'userMainPhone':
          let user = JSON.parse(localStorage.getItem('sessionUser'));
          dataBase.updateUserMainPhone(user.userMail.replace(/\./g, "-"),this.inputValue);
          this.closeModal();
          break;
    }
  }
  

  //#region INVITATION
  private handleInvitation() {
    let locationPlan = this.location.locationPlan;
    let myPlanUsers = JSON.parse(localStorage.getItem("subscriptionPlan"))[locationPlan].includes.users;
    dataBase.getCurrentLocationUsers(this.location).then(userNumber => {
      if ((userNumber <= myPlanUsers) || myPlanUsers == "ILIMITADOS") {
        this.sendInvitation(this.location, this.inputValue.toLowerCase());
        this.presentAlert("Invitación", "La invitación ha sido enviada correctamente.")
      } else {
        this.presentAlert("Invitación", "Ya ha invitado al número máximo que le permite su plan.")
      }
    });
  }

  private sendInvitation(location, mail) {
    dataBase.getStatusTermsAndConds(mail, callback => {
      if (callback) {
        dataBase.createNewLocationUserInFirebase(location.locationId, mail, false, response => {
          // dataBase.createNewLocationUserInFirebase(location.locationId, mail, false, true, response => {
          if (response) {
            this.events.publish('reloadUsers', null);
          }
        });
      } else {
        dataBase.createNewLocationUserInFirebase(location.locationId, mail, false, response => {
          // dataBase.createNewLocationUserInFirebase(location.locationId, mail, false, false, response => {
          if (response) {
            this.events.publish('reloadUsers', null);
          }
        });
      }
    });
  }

  //#endregion

  //#region DELETE LOCATION

  private deleteUserLocation() {
    let location = this.location;
    let user = JSON.parse(localStorage.getItem(strings.session.sessionUser));
    let locationKey = location.locationId + "_" + user.userMail.replace(/\./g, "-");
    this.deleteLocationPreferences(location);
    dataBase.deleteLocation(locationKey);
  }

  private deleteLocationPreferences(location) {
    let selectedLocation = JSON.parse(localStorage.getItem(strings.session.selectedLocation));
    if (selectedLocation && selectedLocation.locationId == location.locationId) {
      localStorage.removeItem(strings.session.selectedLocation);
    }
    localStorage.removeItem(`shortcuts_${location.locationId}`);
    localStorage.removeItem(`devices_${location.locationId}`);
    localStorage.removeItem(`deviceAliasList_${location.locationId}`);
    localStorage.removeItem(`notifications_${location.locationId}`);
    localStorage.removeItem(`roomList_${location.locationId}`);
  }

  private isUserSuperUser(userMail,isSuperUser) {
    dataBase.getLocationSuperUser(this.location.locationId, response => {
      if (userMail == response.userId) {
        isSuperUser(true);
      } else {
        isSuperUser(false);
      }
    });
  }
  //#endregion

  //#region CHANGE LOCATION NAME
  private changeLocationName() {
    dataBase.changeLocationName(this.location.locationId, this.inputValue);
    let currentLocation = JSON.parse(localStorage.getItem('selectedLocation'));
    if (currentLocation) {
      this.updateLocationNameInLocalStorage();
    }
    let toolbar = document.getElementsByTagName('ion-toolbar')[0];
    let headerTitle = toolbar.getElementsByClassName('locationCaption')[0];
    if (headerTitle) {
      headerTitle.innerHTML = this.inputValue;
    }
  }

  private updateLocationNameInLocalStorage() {
    let currentLocation = JSON.parse(localStorage.getItem('selectedLocation'));
    currentLocation.locationName = this.inputValue;
    localStorage.setItem('selectedLocation', JSON.stringify(currentLocation));
    this.events.publish('selectedLocation:updated', currentLocation);
  }
  //#endregion

  //#region CHANGE ALARM MODE
  public changeAlarmMode(mode) {
    switch (mode) {
      case 'deactivate':
        this.closeModal('off');
        break;
      case 'nightMode':
        this.closeModal('noche');
        break;
      case 'activate':
        this.closeModal('completa');
        break;
    }
  }
  //#endregion

  //#region DELETE USER DEVICE
  public deleteUserDevice(deviceToken) {
    dataBase.deleteUserDevice(this.location.locationId, deviceToken);
    if (this.from == 'userDevices') {
      this.userDevices = this.userDevices.filter(x => x.pushToken !== deviceToken);
    } else {
      this.locationDevices = this.locationDevices.filter(x => x.pushToken !== deviceToken);
      this.devicesWithNoUser = this.devicesWithNoUser.filter(x => x.pushToken !== deviceToken);
    }
    this.removeRepeatedUsers();
  }
  //#endregion

  //#region CHANGE EMERGENCY CONTACTS

  private updateUserPhoneNumber(which) {
    let firstPhone = this.location.locationContacts.split(',')[0];
    let secondPhone = this.location.locationContacts.split(',')[1];
    switch (which) {
      case 'userPhoneNumber1':
        firstPhone = this.inputValue ? this.inputValue : "";
        break;
      case 'userPhoneNumber2':
        secondPhone = this.inputValue ? this.inputValue : "";
        break;
    }
    let result = `${firstPhone},${secondPhone}`;
    dataBase.updateEmergencyContacts(this.location.locationId, result);
    let selectedLocation = JSON.parse(localStorage.getItem('selectedLocation'));
    selectedLocation.locationContacts = result;
    localStorage.setItem('selectedLocation', JSON.stringify(selectedLocation));
  }

  //#endregion

  //#region LOCATION USERS

  private getCurrentPhoneAndUsersInLocation() {

    this.myPlanPhones = JSON.parse(localStorage.getItem("subscriptionPlan"))[this.location.locationPlan].includes.phones;
    this.myPlanUsers = JSON.parse(localStorage.getItem("subscriptionPlan"))[this.location.locationPlan].includes.users;
    Promise.all([dataBase.getCurrentLocationPhones(this.location.locationId), dataBase.getCurrentLocationUserCount(this.location.locationId)])
      .then(response => {
        this.phonesNumber = response[0];
        this.currentLocationUsers = response[1];
      });
  }

  private removeRepeatedUsers() {
    this.getCurrentPhoneAndUsersInLocation();
    // this.locationUsers = this.locationDevices.filter((v,i,a)=>a.findIndex(t=>(t.user === v.user))===i)
    dataBase.getAllLocationUsers(this.location.locationId, users => {
      this.locationUsers = users;
      this.locationUsers.forEach((user, index) => {
        this.locationUsers[index].devices = [];
        this.locationDevices.forEach((device, i) => {
          if (device.user == user.userId) {
            this.locationUsers[index].devices.push(device);
            this.locationDevices[i]["alreadyAssigned"] = true;
          } else {
            if (!this.devicesWithNoUser.includes(device) && !device.alreadyAssigned) {
              this.devicesWithNoUser.push(device);
              this.locationDevices[i]["alreadyAssigned"] = true;
            }
          }
        });
      });
    });
  }

  public async deleteUser(userId) {
    this.modalCtrl.dismiss();
    let modal = await this.modalCtrl.create({
      component: EditLocationPage,
      componentProps: { from: 'generalModal', title: 'Si continúa se eliminará por completo el usuario y todos los dispositivos asociados. ¿Desea Continuar?' },
      backdropDismiss: true,
      showBackdrop: true,
      cssClass: 'editLocationModal'
    })
    await modal.present();
    modal.onDidDismiss().then(data => {
      if (data.data == 'accept') {
        this.isUserSuperUser(userId, isSuperUser => {
          if (isSuperUser) {
            this.presentAlert("Alerta", "no puedes eliminar este usuario porque es el administrador.");
          } else {
            this.userProfile.deleteUserLocationAndDevices(this.location.locationId, userId);
            this.removeRepeatedUsers();
          }
        });
        this.closeModal();
      }
      this.launchEditModal(this.location, this.locationDevices);
    });
  }

  private async launchEditModal(location, devices) {
    let modal = await this.modalCtrl.create({
      component: EditLocationPage,
      componentProps: { from: 'locationDevices', location: location, locationDevices: devices },
      backdropDismiss: true,
      showBackdrop: true,
      cssClass: 'editLocationModal'
    })
    await modal.present();
    modal.onDidDismiss().then(data => {

    });
  }
  //#endregion
  public async presentAlert(title, msg) {
    let alert = await this.alertCtrl.create({
      header: title,
      subHeader: msg,
      buttons: ['Aceptar']
    });
    await alert.present();
  }

  public closeModal(dismissData = null) {
    this.modalCtrl.dismiss(dismissData);
  }

  private refreshUI(): void {
    this.zone.run(() => {

    });
  }

}
