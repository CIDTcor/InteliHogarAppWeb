import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ColorLightWindowPageRoutingModule } from './color-light-window-routing.module';

import { ColorLightWindowPage } from './color-light-window.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ColorLightWindowPageRoutingModule
  ],
  declarations: [ColorLightWindowPage]
})
export class ColorLightWindowPageModule {}
