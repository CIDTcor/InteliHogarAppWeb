import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ColorLightWindowPage } from './color-light-window.page';

const routes: Routes = [
  {
    path: '',
    component: ColorLightWindowPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ColorLightWindowPageRoutingModule {}
