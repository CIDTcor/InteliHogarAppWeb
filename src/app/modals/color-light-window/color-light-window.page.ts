import { Component, OnInit } from '@angular/core';
import { NavController, ModalController, ToastController, LoadingController } from '@ionic/angular';

declare function showColorWheel(_deviceId, _deviceStatus, isDone);

@Component({
  selector: 'app-color-light-window',
  templateUrl: './color-light-window.page.html',
  styleUrls: ['./color-light-window.page.scss'],
})
export class ColorLightWindowPage implements OnInit {
  public device;
  private loader;
  data;

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController
  ) {
    // this.device = this.data;
  }

  ngOnInit() {
    this.device = this.data;
    if (!this.device.value) {
      this.device.value = {
        hue: 0,
        saturation: 0
      };
    }
    let h = this.device.value.hue * 3.60;
    let s = this.device.value.saturation;
    let v = 50;
    const element = document.querySelector('.selectedColor') as HTMLElement;
    element.style.setProperty('background', `hsl(${h},${s}%,${v}%)`);

    showColorWheel(this.device.id, this.device.value, dismissData => {
      // this.showLoader();
      // this.loader.dismiss();
      if (!dismissData.done) {
        this.showToast('Conexión lenta, comando no enviado');
      } else {
        h = dismissData.colorSelected.h;
        s = dismissData.colorSelected.s;
        v = 50;
      element.style.setProperty('background', `hsl(${h},${s}%,${v}%)`);
      }
    });
  }

  private async showLoader() {
    this.loader = this.loadingCtrl.create({
      cssClass: 'comfort-loader'
    });
    await this.loader.present();
  }

  public async showToast(msg) {
    let toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    await toast.present();
  }

  public closeWindow() {
    this.modalCtrl.dismiss();
  }


}
