import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ColorLightWindowPage } from './color-light-window.page';

describe('ColorLightWindowPage', () => {
  let component: ColorLightWindowPage;
  let fixture: ComponentFixture<ColorLightWindowPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColorLightWindowPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ColorLightWindowPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
