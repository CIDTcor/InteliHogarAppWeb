import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChangeDeviceRoomPageRoutingModule } from './change-device-room-routing.module';

import { ChangeDeviceRoomPage } from './change-device-room.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChangeDeviceRoomPageRoutingModule
  ],
  declarations: [ChangeDeviceRoomPage]
})
export class ChangeDeviceRoomPageModule {}
