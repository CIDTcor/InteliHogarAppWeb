import { Component, OnInit, NgZone } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { dataBase } from '../../../assets/Util/FirebaseCommunication';
import * as utils from '../../../assets/Util/deviceUtil';

@Component({
  selector: 'app-change-device-room',
  templateUrl: './change-device-room.page.html',
  styleUrls: ['./change-device-room.page.scss'],
})
export class ChangeDeviceRoomPage implements OnInit {

  public lightDevices = [];
  public doorDevices = [];
  public windowDevices = [];
  public batteryDevices = [];
  public temperatureDevices = [];
  public garageDevices = [];
  public curtainDevices = [];  
  public motionDevices = [];  
  public smokeDevices = [];    
  public inteliApps = [];
  public routines = [];
  public alarms = [];
  public restDevices = [];
  public adjustableLightDevices = [];
  public colorLightDevices = [];
  public thermostatDevices = [];
  public switchDevices = [];
  public panicDevices = [];
  public lockDoorDevices = [];
  public outerDoorDevices = [];
  public valveDevices = [];

  devices;
  selectedRoom;


  constructor(public navCtrl: NavController,
      public modalCtrl: ModalController,
      private zone: NgZone) {
    this.refreshUI();
  }

  ngOnInit() {
    this.selectedRoom = this.selectedRoom ? this.selectedRoom : 'favorites';
    this.listAllDevices();
  }

  private listAllDevices() {
    this.devices.forEach(device => {
      switch (device.type) {
        case 'switchLight':
          this.lightDevices.push(device);
          break;
        case 'adjustableLight':
        case 'dimmer':
          this.adjustableLightDevices.push(device);
          break;
        case 'colorLight':
          this.colorLightDevices.push(device);
          break;
        case 'doorContact':
          this.doorDevices.push(device);
          break;
        case 'windowContact':
        case 'window':
          this.windowDevices.push(device);
          break;
        case 'garage':
          this.garageDevices.push(device);
          break;
        case 'curtain':
          this.curtainDevices.push(device);
          break;
        case 'motion':
          this.motionDevices.push(device);
          break;
        case 'smoke':
          this.smokeDevices.push(device);
          break;
        case 'battery':
          this.batteryDevices.push(device);
          break;
        case 'inteliAppsGeneral':
        case 'inteliAppsEnergy':
        case 'inteliAppsSecurity':
        case 'simulacionPresencia':
          this.inteliApps.push(device);
          break;
        case 'alarmaAgua':
        case 'alarmaIntrusos':
        case 'alarmaIncendio':
        case 'alarmaGas':
          this.alarms.push(device);
          break;
        case 'helloHome':
          this.routines.push(device);
          break;
        case 'temperature':
          this.temperatureDevices.push(device);
          break;
        case 'thermostat':
          this.thermostatDevices.push(device);
          break;
        case 'switch':
          this.switchDevices.push(device);
          break;
        case 'panic':
          this.panicDevices.push(device);
          break;
        case 'lockDoor':
          this.lockDoorDevices.push(device);
          break;
        case 'outerDoor':
          this.outerDoorDevices.push(device);
          break;
        case 'valve':
          this.valveDevices.push(device);
          break;
        default:
          this.restDevices.push(device);
      }
    });
  }

  public closeWindow(devices = null) {
    this.modalCtrl.dismiss(devices);
  }

  public changeDevicesRoom() {
    let selectedLocation = JSON.parse(localStorage.getItem('selectedLocation'));
    let dismissData = [];
    if (this.selectedRoom == "favorites") {
      let devices = [];
      this.devices.forEach(device => {
        if (device.checked) {
          devices.push(device.id);
        }
      });
      utils.addDevicesListToShortcuts(selectedLocation.locationId, devices);
    }
    if (this.selectedRoom.includes("room_")) {
      this.devices.forEach(device => {
        if (device.checked) {
          device.roomId = this.selectedRoom;
          dataBase.updateDeviceRoomId(selectedLocation.locationKey, device);
        }
      });
    }
    if (this.selectedRoom == "notifications") {
      this.devices.forEach(device => {
        if(device.checked) {
          dismissData.push(device.id);
        }
      });
    }
    if (this.selectedRoom == "hiddenDevices") {
      let hiddenDevices = [];
      this.devices.forEach(device => {
          device.visible = !device.checked;
          dataBase.updateDeviceVisibility(selectedLocation.locationId, device)
          if (!device.visible) {
            hiddenDevices.push(device.id);
          }
      });
        localStorage.setItem(`hiddenDevices_${selectedLocation.locationKey}`, JSON.stringify(hiddenDevices));
    }
    return this.closeWindow(dismissData);
  }

  private refreshUI(): void {
    this.zone.run(() => {

    });
  }


}
