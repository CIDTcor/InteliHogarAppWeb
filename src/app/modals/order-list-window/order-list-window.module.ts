import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderListWindowPageRoutingModule } from './order-list-window-routing.module';

import { OrderListWindowPage } from './order-list-window.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderListWindowPageRoutingModule
  ],
  declarations: [OrderListWindowPage]
})
export class OrderListWindowPageModule {}
