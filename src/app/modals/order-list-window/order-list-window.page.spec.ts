import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OrderListWindowPage } from './order-list-window.page';

describe('OrderListWindowPage', () => {
  let component: OrderListWindowPage;
  let fixture: ComponentFixture<OrderListWindowPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderListWindowPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OrderListWindowPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
