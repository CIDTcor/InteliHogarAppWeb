import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderListWindowPage } from './order-list-window.page';

const routes: Routes = [
  {
    path: '',
    component: OrderListWindowPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderListWindowPageRoutingModule {}
