import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { dataBase } from '../../../assets/Util/FirebaseCommunication';

@Component({
  selector: 'app-order-list-window',
  templateUrl: './order-list-window.page.html',
  styleUrls: ['./order-list-window.page.scss'],
})
export class OrderListWindowPage implements OnInit {
  private selectedLocation;
  rooms;
  location;

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController) { 
    }

  ngOnInit() {
    this.setRooms(this.rooms);
    this.selectedLocation = location;
  }

  
  private setRooms(rooms) {
    rooms.sort((a, b) => {
      return a.roomOrder - b.roomOrder;
    });
    this.rooms = rooms;
  }

  public orderRoom(action, room) {
    // let index = this.getIndex(room.roomId);
    // let oldOrder;
    // let newOrder;
    // if (action == 'up') {
    //   oldOrder = this.rooms[index].roomOrder;
    //   newOrder = this.rooms[index - 1].roomOrder;
    //   this.rooms[index].roomOrder = newOrder;
    //   this.rooms[index - 1].roomOrder = oldOrder;
    //   dataBase.updateRoomOrder(this.selectedLocation.locationId, this.rooms[index]);
    //   dataBase.updateRoomOrder(this.selectedLocation.locationId, this.rooms[index - 1]);
    // } else if (action == 'down') {
    //   oldOrder = this.rooms[index].roomOrder;
    //   newOrder = this.rooms[index + 1].roomOrder;
    //   this.rooms[index].roomOrder = newOrder
    //   this.rooms[index + 1].roomOrder = oldOrder
    //   dataBase.updateRoomOrder(this.selectedLocation.locationId, this.rooms[index]);
    //   dataBase.updateRoomOrder(this.selectedLocation.locationId, this.rooms[index + 1]);
    // }
    // this.rooms.sort((a, b) => {
    //   return a.roomOrder - b.roomOrder;
    // });
  }

  private getIndex(roomId) {
    let index = 0;
    for (let i = 0; i <= this.rooms.length; i++) {
      if (this.rooms[i].roomId == roomId) {
        index = i;
        break;
      }
    }
    return index;
  }

  public closeWindow(){
    this.modalCtrl.dismiss(this.rooms);
  }

}
