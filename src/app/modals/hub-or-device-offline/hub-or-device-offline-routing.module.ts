import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HubOrDeviceOfflinePage } from './hub-or-device-offline.page';

const routes: Routes = [
  {
    path: '',
    component: HubOrDeviceOfflinePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HubOrDeviceOfflinePageRoutingModule {}
