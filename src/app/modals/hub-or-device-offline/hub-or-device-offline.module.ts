import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HubOrDeviceOfflinePageRoutingModule } from './hub-or-device-offline-routing.module';

import { HubOrDeviceOfflinePage } from './hub-or-device-offline.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HubOrDeviceOfflinePageRoutingModule
  ],
  declarations: [HubOrDeviceOfflinePage]
})
export class HubOrDeviceOfflinePageModule {}
