import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HubOrDeviceOfflinePage } from './hub-or-device-offline.page';

describe('HubOrDeviceOfflinePage', () => {
  let component: HubOrDeviceOfflinePage;
  let fixture: ComponentFixture<HubOrDeviceOfflinePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HubOrDeviceOfflinePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HubOrDeviceOfflinePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
