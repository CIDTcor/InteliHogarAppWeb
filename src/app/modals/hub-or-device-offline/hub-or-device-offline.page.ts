import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';

import { AuthService } from '../../services/auth.service';


@Component({
  selector: 'app-hub-or-device-offline',
  templateUrl: './hub-or-device-offline.page.html',
  styleUrls: ['./hub-or-device-offline.page.scss'],
})
export class HubOrDeviceOfflinePage implements OnInit {

  from
  constructor(
    private navCtrl: NavController,
    private authService: AuthService,
    public modalCtrl: ModalController
  ) { }

  ngOnInit() {

  }

  public actionsTriggered(action) {
    switch (action) {
      case 'locations':
        this.navCtrl.navigateRoot('locations');
        break;
      case 'signOut':
        this.signOut();
        break;
    }
    this.modalCtrl.dismiss();
  }

  private signOut() {
    this.navCtrl.navigateRoot('loading');
    let savedData = JSON.parse(localStorage.getItem("savedData"));
    let subscriptionPlan = JSON.parse(localStorage.getItem("subscriptionPlan"));
    let sessionUser = JSON.parse(localStorage.getItem("sessionUser"));
    localStorage.clear();
    if (savedData) {
      localStorage.setItem("savedData", JSON.stringify(savedData));
    }
    if (subscriptionPlan) {
      localStorage.setItem("subscriptionPlan", JSON.stringify(subscriptionPlan));
    }
    if (sessionUser) {
      localStorage.setItem("sessionUser", JSON.stringify(sessionUser));
    }
    // dataBase.logOut(isDone => {
    //   if (isDone) {
    //     alert.dismiss();
    //     this.navCtrl.navigateRoot('login');
    //   }
    // });
    this.authService.logout();
  }

}
