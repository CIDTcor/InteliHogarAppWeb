import { Component, OnInit } from '@angular/core';
import { NavController, ModalController, LoadingController, ToastController } from '@ionic/angular';
import { translateThermostatModes } from '../../../assets/Util/deviceUtil';
import { dataBase } from '../../../assets/Util/FirebaseCommunication';

declare function renderRoundSlider(deviceId, sliderType, value, status);
declare function sendCommand(type, id, value, isDone);

@Component({
  selector: 'app-thermostat-window',
  templateUrl: './thermostat-window.page.html',
  styleUrls: ['./thermostat-window.page.scss'],
})
export class ThermostatWindowPage implements OnInit {
  public thermostat;
  private loader;
  data;

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController) {
  }

  ngOnInit() {
    this.thermostat = this.data;
    this.getThermostatState();
  }

    private getThermostatState() {
    dataBase.getSpecificDeviceState(this.thermostat, state => {
      this.updateUI(state);
      this.thermostat.value = state.value;
      this.thermostat.status = state.status;
      this.thermostat.temperature = state.value.temperature + "°C";
      let _sliderType = (this.thermostat.status == "off" || this.thermostat.status == null) ? "none" : "default";
      renderRoundSlider(this.thermostat.id, _sliderType, parseInt(this.thermostat.value.thermostatSetpoint) + 1, this.thermostat.status);
      // this.thermostat.status = translateThermostatModes(state.status);
    });
  }

  public sendCommand(type, id, value) {
    this.showLoader();
    sendCommand(type, id, value, isDone => {
      this.loader.dismiss();
      if (!isDone) {
        this.showToast('Conexión lenta, comando no enviado');
      }
    });
  }

  private showLoader() {
    this.loader = this.loadingCtrl.create({
      cssClass: 'comfort-loader'
    });
    this.loader.present();
  }

  public async showToast(msg) {
    let toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    await toast.present();
  }

  public closeWindow() {
    this.modalCtrl.dismiss();
  }

  private updateUI(state) {
    let viewMode = document.getElementById("thermoMode");
    let viewTemperature = document.getElementById("thermoTemp");

    viewMode.innerHTML = translateThermostatModes(state.status);
    viewTemperature.innerHTML = state.value.temperature + "°C";
  }


}
