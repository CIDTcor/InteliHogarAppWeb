import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ThermostatWindowPageRoutingModule } from './thermostat-window-routing.module';

import { ThermostatWindowPage } from './thermostat-window.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ThermostatWindowPageRoutingModule
  ],
  declarations: [ThermostatWindowPage]
})
export class ThermostatWindowPageModule {}
