import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ThermostatWindowPage } from './thermostat-window.page';

const routes: Routes = [
  {
    path: '',
    component: ThermostatWindowPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ThermostatWindowPageRoutingModule {}
