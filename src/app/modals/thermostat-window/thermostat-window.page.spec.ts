import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ThermostatWindowPage } from './thermostat-window.page';

describe('ThermostatWindowPage', () => {
  let component: ThermostatWindowPage;
  let fixture: ComponentFixture<ThermostatWindowPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThermostatWindowPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ThermostatWindowPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
