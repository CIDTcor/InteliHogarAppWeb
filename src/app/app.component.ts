import { Component } from '@angular/core';

import { Platform, ModalController, AlertController, MenuController, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { LoadingPage } from './pages/loading/loading.page';
import { HelpPage } from './pages/help/help.page';
import { ConfigurationsPage } from './pages/configurations/configurations.page';
import { LocationsPage } from './pages/locations/locations.page';
import { LoginPage } from './pages/login/login.page';
import { HometabsPage } from './pages/hometabs/hometabs.page';
import { dataBase } from '../assets/Util/FirebaseCommunication';
import { FirebaseX } from "@ionic-native/firebase-x/ngx";
import { Events } from '../assets/Util/events';
import { BackButtonEvent } from '@ionic/core';
import { Router } from '@angular/router';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { MobileAccessibility } from '@ionic-native/mobile-accessibility/ngx';

import { Network } from '@ionic-native/network/ngx';
import { HubOrDeviceOfflinePage } from './modals/hub-or-device-offline/hub-or-device-offline.page';


// import { CreateEditRoomPage } from './pages/tabsModals/create-edit-room/create-edit-room.page'
// import { AuthService } from '../app/services/auth.service';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  rootPage: any = LoadingPage;
  public helpPage = HelpPage;
  public currentUser;
  pages: Array<{ title: string, component: any, icon: any }>;
  private disconnectSubscription;
  private connectSubscription;
  private offlineModal;
  private map = {};
  private arr = [];
  private body;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public modalCtrl: ModalController,
    public menuCtrl: MenuController,
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    private firebase: FirebaseX,
    private events: Events,
    private router: Router,
    private inAppBrowser: InAppBrowser,
    private network: Network,
    private mobileAccessibility: MobileAccessibility
    // private createEditRoomPage: CreateEditRoomPage
    // private authService: AuthService
  ) {
    this.initializeApp();
    this.getDataFromLocalStorage();
    this.listenToKeyboardEvent();
    // this.setPages();
    
  }

  //#region Initialize App
  public initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      // this.splashScreen.hide();
      //this.pushSetup();
      this.firebaseXPushSetup();

      (<any>Window).approve = function (data: any) {
        alert('Approve called');
      };

      (<any>Window).reject = function (data: any) {
        alert('Reject called');
      };

      this.checkNetworkConnection();

      var channel = {
        name: "alarm_channel",
        id: "alarmChannel",
        sound: "alarm"
      };

      this.firebase.grantPermission().then(resp => {
      });
      this.firebase.createChannel(channel).then(channelResult => {
      }
      );

      this.firebase.onMessageReceived().subscribe(data => {
        if (data.type == 'alarm') {
          let locationId = data.locationId;
          var deviceId = data.deviceId;
          var _this = this;
          if (JSON.parse(localStorage.getItem('selectedLocation')).locationId == locationId && window.location.pathname.includes('hometabs')) {
            this.events.publish('alarmActivated', deviceId);
            this.navCtrl.navigateRoot('hometabs/security');
          } else {
            dataBase.getLocationInfo(locationId, info => {
              //this.locationPage.selectLocation(info);
              setTimeout(function () {
                _this.navCtrl.navigateRoot('hometabs/security');
              }, 3000);
              setTimeout(function () {
                _this.events.publish('alarmActivated', deviceId);
              }, 3000);
            });
          }
        }
      }
      );

      if (this.mobileAccessibility) {
        this.mobileAccessibility.usePreferredTextZoom(false);
      }

    });



  }

  private getDataFromLocalStorage() {
    this.currentUser = JSON.parse(localStorage.getItem('sessionUser'));
    this.currentUser = this.currentUser ? this.currentUser : { userName: 'Defaut' };
    this.events.subscribe('user:updated', user => {
      this.currentUser.userImage = user.userImage;
      this.currentUser.userName = user.userName;
    });
  }

  public listenToKeyboardEvent() {
    window.addEventListener('keyboardWillShow', ev => {
      this.events.publish('keyboard:updated', true);
    });
    window.addEventListener('keyboardWillHide', ev => {
      this.events.publish('keyboard:updated', false);
    });
    document.addEventListener('ionBackButton', (ev: BackButtonEvent) => {
      ev.detail.register(10, () => {
        if (this.router['routerState'].snapshot.url != "/login") {
          this.navCtrl.back();
        }
      });
    });
  }

  private setPages() {
    this.pages = [
      { title: 'Localizaciones', component: LocationsPage, icon: 'md-locate' },
      { title: 'Configuraciones', component: ConfigurationsPage, icon: 'md-settings' }
    ];
  }

  private firebaseXPushSetup() {
    if (!(this.platform.is('mobileweb') || this.platform.is('desktop'))) {
      this.firebase.getToken().then(token => {
        this.updateDeviceToken(token);
      });
    }
  }

  private updateDeviceToken(newToken) {
    let oldToken = localStorage.getItem("token");
    if (oldToken && oldToken != newToken) {
      localStorage.setItem("token", newToken);
      localStorage.setItem("oldToken", oldToken);
    } else {
      localStorage.setItem("token", newToken);
    }
  }

  // public async logout() {
  //   this.menuCtrl.close();
  //   let alert = await this.alertCtrl.create({
  //     header: '¿Seguro?',
  //     subHeader: 'Si continuas perderás todos tus accesos directos y notificaciones. ¿Estás seguro?',
  //     buttons: ['Cancelar', {
  //       text: 'Continuar',
  //       handler: () => {
  //         // this.nav.setRoot(LoadingPage);
  //         this.navCtrl.navigateRoot('loading')
  //         dataBase.deleteToken(JSON.parse(localStorage.getItem('selectedLocation')).locationId, isDone => {
  //           if (isDone) {
  //             let savedData = JSON.parse(localStorage.getItem("savedData"));
  //             localStorage.clear();
  //             if (savedData) {
  //               localStorage.setItem("savedData", JSON.stringify(savedData));
  //             }
  //             this.authService.logOut(isDone => {
  //               if (isDone) {
  //                 // this.nav.setRoot(LoginPage);
  //                 this.navCtrl.navigateRoot('login')
  //               }
  //             });
  //           }
  //         });
  //       }
  //     }]
  //   });
  //   await alert.present();
  // }
  //#endregion

  //#region navigation
  public openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    // this.nav.push(page.component);
    this.navCtrl.navigateForward(page)
  }
  public openStore() {
    let URL = `https://www.intelihogar.com/#!/tienda`;
    //window.open(URL, "_system");
    this.inAppBrowser.create(URL, '_system');
  }

  private openWindow(): void {
    let user = JSON.parse(localStorage.getItem("sessionUser"));
    let locationId = JSON.parse(localStorage.getItem("selectedLocation")).locationId
    let URL = `https://www.intelihogar.com/backend/?userid=${user.userMail}&locationid=${locationId}&origin=movil`;
    //window.open(URL);
    this.inAppBrowser.create(URL, '_system');
  }

  public openRoutingPage(page) {
    localStorage.removeItem('locationId');
    switch (page) {
      case 'locations':
        this.menuCtrl.close();
        this.navCtrl.navigateBack(page);
        break;
      case 'configurations':
      case 'help':
        this.menuCtrl.close();
        this.navCtrl.navigateForward(page);
        break;
      case 'store':
        let URL = `https://www.intelihogar.com/tienda`;
        //window.open(URL, "_system");
        this.inAppBrowser.create(URL, '_system');
        break;
    }
  }

  private checkNetworkConnection() {
    this.disconnectSubscription = this.network.onDisconnect().subscribe(() => {
      // setHeaderColorTitle('firebrick', 'No tiene conexión', 'aliceblue');
      this.launchOfflineModal('device');
    });

    this.connectSubscription = this.network.onConnect().subscribe(async () => {
      // setHeaderColorTitle('#006600', 'Ya tiene conexión', 'aliceblue');
      // setTimeout(() => {
      // setHeaderColorTitle('#f8f8f8', this.selectedLocation.locationName, '#424242');
      if (this.modalCtrl.getTop()) {
        let modal = await this.modalCtrl.getTop();
        if (modal) {
          this.modalCtrl.dismiss();
        }
      }
      // }, 2000);
    });

  }

  private async launchOfflineModal(from) {
    this.offlineModal = await this.modalCtrl.create({
      component: HubOrDeviceOfflinePage,
      componentProps: { from: from },
      backdropDismiss: false,
      showBackdrop: true,
      cssClass: 'editLocationModal'
    })
    await this.offlineModal.present();
    this.offlineModal.onDidDismiss().then(data => {

    });
  }


  //#endregion


}
